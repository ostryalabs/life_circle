class Breadcrumb < Struct.new(:breadcrumbs, :request_param)
  
  def push_element
    breadcrumbs.push(BreadcrumbElement.new(:name => "Home", :url => "/", :klass => ""))
    MenuBar.hirachichal_menu_bar(request_param).each do |menu_bar|
      is_key_mateched = ((menu_bar["name"].downcase == request_param) ? "active" : "")
      breadcrumbs.push(BreadcrumbElement.new(:name => menu_bar["name"], :url => "#{menu_bar["url"]}?bread_crumb_param=#{menu_bar['name']}", :klass => (is_key_mateched ? "active" : "")))
    end
  end

  def change_class(key)
    breadcrumbs.each do|breadcrumb|
      if breadcrumb.name == key
        breadcrumb.klass = "active"
      else
        breadcrumb.klass = ""
      end
    end
  end

  private
  class BreadcrumbElement
    include Virtus.model
    attribute :name, :String
    attribute :url, :String
    attribute :klass, :String
  end

  def new_list
    @breadcrumbs = []
  end

end
