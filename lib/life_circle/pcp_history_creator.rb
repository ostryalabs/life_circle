module LifeCircle
  class PcpHistoryCreator
    def initialize(enrollment_id, current_user)
      p "Initialize"
      p enrollment_id
      p current_user
      @enrollment_id = enrollment_id
      #@enrollment = Enrollment.find(@enrollment_id)
      #@enquiry = @enrollment.enquiry
      @current_user = current_user
    end

    def save_patient_abilities_history(params)
      open_transaction do
        @params = params
        enrollment_history = save_enrollment_history(@params[:description])
        if enrollment_history.save
          patient_ability_history = PatientSelfcareAbilityHistory.new()
          patient_ability_history.enrollment_history_id =enrollment_history.id
          patient_ability_history.patient_selfcare_ability_master_id = @params[:id]
          patient_ability_history.independent = @params[:independent]
          status = patient_ability_history.save
          unless status
            p "save_patient_abilities"
            raise ActiveRecord::Rollback
          end
        end        
      end
      true
    end

    def save_adaptive_devices_history
      open_transaction do
      end
    end

    def save_drug_management_history
      open_transaction do
      end
    end

    def save_patient_routine_history
      open_transaction do
      end
    end

    def save_vitals_tracked_history
      open_transaction do
      end
    end

    def save_patient_care_assitance_history
      open_transaction do
      end
    end

    def save_patient_condition_history
      open_transaction do
      end
    end

    def save_enrollment_history(params)
      open_transaction do
        p "=============="
        p params
        p @enrollment_id
        @enrollment_history = EnrollmentHistory.new()
        @enrollment_history.enrollment_id = @enrollment_id
        @enrollment_history.note = params
        @enrollment_history.created_by = @current_user.id
        @enrollment_history
      end
    end
    
    private
    
    def open_transaction
      ActiveRecord::Base.transaction do
        yield if block_given?
      end
    end
  end
end
