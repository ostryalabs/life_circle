module LifeCircle
  class CareGiverChargeReviseFormObject
    include Virtus
    extend ActiveModel::Naming
    include ActiveModel::Conversion
    include ActiveModel::Validations
    attribute :period_from, Date
    attribute :period_to, Date
    attribute :no_of_days, Integer
    attribute :enrollment_id, Integer
    attribute :enquiry_care_givers, [EnquiryCareGiver]
    attribute :generation_date, Date
  end
end
