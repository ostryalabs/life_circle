module LifeCircle
  class EnrollmentCreator
    attr_accessor :enrollment
    def initialize(enquiry, params, current_user)
      @enquiry = enquiry
      @params = params
      @current_user = current_user
    end

    def create
      ActiveRecord::Base.transaction do
        @enrollment = Enrollment.new(enrollment_params(@params))
        @enrollment.enquiry = @enquiry
        if @enrollment.safety_measures.length > 0
          return false if @params[:enrollment_safety_violation].nil? || @params[:enrollment_safety_violation][:comments].nil?
          @enrollment_safety = EnrollmentSafetyViolation.new do |safety|
            safety.comments = @params[:enrollment_safety_violation][:comments]
            safety.enrollment = @enrollment
            safety.enrolled_by = @current_user
          end
        end
        guardian = nil
        if @params[:patient][:guardian][:id].present?
          guardian = Guardian.find(@params[:patient][:guardian][:id])
          guardian.assign_attributes(guardian_params(@params[:patient][:guardian]))
          guardian.save
        else
          guardian = Guardian.create(guardian_params(@params[:patient][:guardian]))
        end
        if @params[:patient][:id].present?
          patient = Patient.find(@params[:patient][:id])
        else
          user = new_user
          user.save
          patient = Patient.new
          patient.user = user
        end
        @enquiry.patient = patient
        @enquiry.patient.assign_attributes(patient_params(@params[:patient]))
        @enquiry.patient.guardian = guardian
        @enquiry.patient.save
        @enquiry.save
        @enquiry.requested_time_slots.map do |ts|
          ts.patient_id = @enquiry.patient.id
          ts
        end.map(&:save)
        @enrollment.patient = @enquiry.patient
        @enrollment.report_prepared_by = @current_user
        @enrollment.enquiry = @enquiry
        @enrollment.application_no = generate_appication_no
        @enrollment.status = 'ACTIVE'
        @enrollment.save
        @enrollment_safety.save if @enrollment_safety.present?
        @enquiry.mark_as_enrolled
        @enrollment.patient_disabilities.destroy_all
        @enrollment.patient_disabilities = new_patient_disabilities(@params, @enrollment)
        @enrollment.patient_disabilities.map(&:save!)
      end
      true
    end
   
 
    private
    
    def patient_params(params)
      params.permit(:first_name, :last_name, :dob, :age ,:gender, :mobile_no, :land_line_no, :alternate_contact_no, :guardian_id ).merge!(:address_attributes => params.require(:address).permit(:line1, :line2, :city, :area, :state, :country, :pin_code , :city_master_id))
    end

    def guardian_params(params)
      params = params.permit(:first_name, :last_name, :mobile_no, :relation, :address_same_as_patient, :alternate_contact_no, :age, :gender)
      unless params[:address_same_as_patient].present? 
        params.merge!(:address_attributes => params.require(:address).permit(:line1, :line2, :city, :area, :state, :country, :pin_code , :city_master_id))
      end
      params
    end

    def enrollment_params(params)
      params.permit(:patient_bmi, :patient_home_setting, :cg_work_alone_with_patient, :suffering_from_infection_disease, :safe_and_well_connected_locality, :provides_personal_protective_equipment, :family_member_helps, :email_for_official_comminication, :primary_contact , :super_care_giver_id , :reason_for_coming_id)
    end

    def new_patient_disabilities(params, enrollment)
      disability_masters = []
      params[:disabilities].each do |p|
       if p[:assigned].present?
         disability_masters.push(PatientDisability.new({:patient_id => enrollment.patient.id, :disability_master_id => p[:id], :enrollment_id => enrollment.id}))
       end
      end
      disability_masters
    end

    def generate_appication_no
      str = "LC#{Date.today.strftime("%d%m%y")}"
      count = sprintf('%02d', (Enrollment.enrolled_on(Date.today).count+1))
      "#{str}#{count}"
    end
    
    def new_user
      User.new(:user_name => @enquiry.email, :email => @enquiry.email , :mobile_no => @enquiry.mobile_no, :password =>User.generate_password_on_admin_creation, :role => User::PATIENT)
    end
    
  end
end
