module LifeCircle
  class EnquiryParamParser
    def self.prepare_search_params_from_enquiry(enquiry)
      search_params = {}
      search_params[:languages] = enquiry.enquiry_languages.map(&:language_master_id)
      search_params[:requested_services] = enquiry.requested_services.map(&:service_master_id)
      search_params[:service_required_from] = enquiry.service_required_from.to_s
      search_params[:service_required_to] = enquiry.service_required_to.to_s
      search_params[:gender] = enquiry.care_giver_gender
      search_params[:time_slots] = enquiry.requested_time_slots.map{|ts| {"time_from" => ts.decorated_time_from, "time_to" => ts.decorated_time_to}}
      
      search_params[:patient_address] =  enquiry.patient.try(:address_str)
      search_params[:open_term] = enquiry.open_term?
      search_params[:affordable_price] = split_affordable_price(enquiry.affordable_price) if enquiry.affordable_price.present?
      search_params[:caregiver_stay] = enquiry.caregiver_stay
      search_params
    end

    def self.split_affordable_price(price)
      split = price.split("<")
      if split.count > 1
        return {:l_value => split[1], :operator => "<"}
      end
      split = price.split(">")
      if split.count > 1
        return {:h_value => split[1], :operator => ">"}
      end
      split = price.split("-")
      if split.count > 1
        return {:l_value => split[0], :h_value => split[1], :operator => "to"}
      end
      return {:h_value => price, :operator => ">"}
    end
  end
end
