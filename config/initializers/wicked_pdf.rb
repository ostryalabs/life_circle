if Rails.env.production?
  WickedPdf.config = {
    exe_path: File.join("~/apps/life_circle/shared", "bundle/ruby/2.2.0/bin/wkhtmltopdf")
  }
end
