require 'test_helper'

class ReasonForComingsControllerTest < ActionController::TestCase
  setup do
    @reason_for_coming = reason_for_comings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:reason_for_comings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create reason_for_coming" do
    assert_difference('ReasonForComing.count') do
      post :create, reason_for_coming: { name: @reason_for_coming.name }
    end

    assert_redirected_to reason_for_coming_path(assigns(:reason_for_coming))
  end

  test "should show reason_for_coming" do
    get :show, id: @reason_for_coming
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @reason_for_coming
    assert_response :success
  end

  test "should update reason_for_coming" do
    patch :update, id: @reason_for_coming, reason_for_coming: { name: @reason_for_coming.name }
    assert_redirected_to reason_for_coming_path(assigns(:reason_for_coming))
  end

  test "should destroy reason_for_coming" do
    assert_difference('ReasonForComing.count', -1) do
      delete :destroy, id: @reason_for_coming
    end

    assert_redirected_to reason_for_comings_path
  end
end
