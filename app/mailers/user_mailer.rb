class UserMailer < ApplicationMailer
  def welcome_email(user , password)
    @user = user
    @password = password
    mail(to: @user.email , subject: 'Life Circle Login')
  end
  
  def ticket_to_admin(template , raised_by , role , category , date)
    @template = template
    @raised_by = raised_by
    @role = role
    @category = category
    @date = date
    mail(to: 'navya@ostryalabs.com' , subject: 'New Ticket Raised')
  end
  
  def ticket_to_user(email , template , raised_by , role , category , date)
    @template = template
    @raised_by = raised_by
    @role = role
    @category = category
    @date = date
    mail(to: email , subject: 'New Ticket Raised')
  end
  
  def cg_login(email , user_name , password)
    @email = email
    @user_name = user_name
    @password = password
    mail(to: email , subject: 'New CG Login')
  end

  def hello
    mail(to: 'gopika@ostryalabs.com', subject: 'I emailed from SES')
  end
end
