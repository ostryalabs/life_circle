class PaymentMasterMailer < ApplicationMailer

  def invoice(payment_master, cc_email)
    @payment_master = payment_master
    date = payment_master.invoice_generation_date
    @month_str = "#{date.strftime('%b')} #{date.strftime('%Y')}"
    pdf_str = WickedPdf.new.pdf_from_string(render_to_string(:pdf => "invoice", :template => '/payment_masters/show.pdf.haml'))
    attachments["Invoice_#{date.strftime('%b')}_#{date.strftime('%Y')}.pdf"] =  {
      mime_type: 'application/pdf',
      content: pdf_str
    }
    mail(to: @payment_master.enrollment.enquiry.email, subject: "Invoice #{@month_str}", cc: cc_email)
  end

end
