class DrugMastersController < ApplicationController
  autocomplete :drug_master, :name, :full => true
  load_resource
  

  def index
    @drug = DrugMaster.select(:id,:name,:route_master_id, :form_master_id, :unit_master_id).all.order("id")
    @d  = DrugMaster.all.map do |x|
      { :id => x.id , :name => x.name , :route_name => x.route_name , :form_name => x.form_name , :unit_name => x.unit_name}
    end
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @d
      end
    end
  end
  def create
    @drug_master = DrugMaster.new(drug_master_params)
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @drug_master.save}
      end
    end

  end

  def update
    respond_to do |format|
      format.json do
        render json: {status: @drug_master.update(drug_master_params)}
      end
    end
  end

  def destroy
    respond_to do |format|
      format.json do
        render json: {status: @drug_master.destroy}
      end
    end
  end

  private
  
  def drug_master_params
    params.require(:drug_master).permit(:name, :route_master_id, :form_master_id, :unit_master_id)
  end
  
end
 
