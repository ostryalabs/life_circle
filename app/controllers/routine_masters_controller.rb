class RoutineMastersController < ApplicationController
   load_resource
  
  def index
    @routine = RoutineMaster.select(:id, :name).all.order("id")
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @routine
      end
    end
  end
  def create
    @routine_master = RoutineMaster.new(routine_master_params)
    @routine_master.created_by = current_user.id
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @routine_master.save}
      end
    end

  end

  def update
    respond_to do |format|
      format.json do
        render json: {status: @routine_master.update(routine_master_params)}
      end
    end
  end

  def destroy
    respond_to do |format|
      format.json do
        render json: {status: @routine_master.destroy}
      end
    end
  end

  private
  
  def routine_master_params
    params.require(:routine_master).permit(:name)
  end
end
