class VitalTrendsController < ApplicationController
  load_resource
  before_action :load_enrollment
  
  def index
    @vitals = @enrollment.vital_trends.select(:id, :key_vitals_master_id, :upper_limit, :lower_limit, :enrollment_id)
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @vitals
      end
    end
  end
  def create
    #@key_vitals = KeyVitalsMaster.where(:id => params[:key_vitals_master_id])
    @vital_trend = VitalTrend.new(vital_trend_params)
    @vital_trend.created_by = current_user.id
    #p abc
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @vital_trend.save}
      end
    end

  end

  def update
    @vital_trend = VitalTrend.find(params[:id])
    #p vital_trend_params
    respond_to do |format|
      format.json do
        render json: {status: @vital_trend.update(vital_trend_params)}
      end
    end
  end

  def destroy
    if @vital_trend.destroy
      @status = true
    else
      @status = false
    end
    respond_to do |format|
      format.json do
        render json: {status: @status}
      end
    end
  end

  private

  def load_enrollment
    @enrollment_id = params[:enrollment_id]
    @enrollment = Enrollment.find(params[:enrollment_id])
  end
  
  def vital_trend_params
    params.require(:vital_trend).permit(:key_vitals_master_id, :value, :date , :upper_limit, :lower_limit, :note, :enrollment_id)
  end
end
