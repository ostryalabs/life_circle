class CareGiverDocumentsController < ApplicationController
 before_action :load_care_giver_master
  load_resource :only => [:approve]
  before_action :authenticate_user!

  def document_for_upload
    @care_giver_documents = @care_giver_master.care_giver_documents
    respond_to do |format|
      format.html{}
      format.json do
        if @care_giver_documents.empty?
          @care_giver_documents = 2.times.map{|i| CareGiverDocument.new{|cgr| cgr.care_giver_master_id = @care_giver_master.id}}
        end
        render :json => {:documents => @care_giver_documents}
      end
    end
  end

  def create
    respond_to do |format|
      format.html{}
      format.json do
        @care_giver_document = params[:id].present? ? CareGiverDocument.find(params[:id]) : CareGiverDocument.new             
        @care_giver_document.assign_attributes(care_giver_document_params(params))
        @care_giver_document.date_of_creation = Date.today
        @care_giver_document.care_giver_master = @care_giver_master
        render :json => {:status => @care_giver_document.save}
      end
    end
  end

  def save_document
    respond_to do |format|
      @care_giver_documents = []
      params[:care_giver_documents].each do |local_params|
        if local_params[:id].present?
          care_giver_document = CareGiverDocument.find(local_params[:id])
          care_giver_document.assign_attributes(care_giver_document_params(local_params))
          care_giver_document.care_giver_master =  @care_giver_master
          @care_giver_documents.push(care_giver_document)
        else
          care_giver_document = CareGiverDocument.new(care_giver_document_params(local_params))
          care_giver_document.care_giver_master =  @care_giver_master
          @care_giver_documents.push(care_giver_document)
        end
      end
      format.html{}
      format.json do
        if @care_giver_documents.map(&:valid?).all?
          status = @care_giver_documents.map(&:save).all?
          @care_giver_master.check_children_approvals_and_update_self_approval
        else
          status = false
        end
          render :json => {:status => status}
      end
    end
  end
  
  def approve
    respond_to do |format|
      format.html do
      end
      format.json do
        status = @care_giver_document.update({:approved => true, :approved_by => current_user.id})
        @care_giver_master.check_children_approvals_and_update_self_approval
        render :json => {:status => status}
      end
    end
  end
  

  private

  def care_giver_document_params(local_params)
    local_params.permit(:document_name, :date_of_creation, :attachment)
  end
  
  def load_care_giver_master
    @care_giver_master = CareGiverMaster.find(params[:care_giver_master_id])
  end

end
