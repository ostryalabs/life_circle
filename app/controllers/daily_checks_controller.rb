class DailyChecksController < ApplicationController
  load_resource
  before_action :load_role
  def index
    p "+++++++++_____________+++++++++++++=========="
    p @rolee
    if(@check_id.present?)
      p "dc"
      if(@rolee == 'Patient')
        p 'a'
        @daily_checks = DailyCheck.where(:patient_id => params[:check_id]).order("entered_at")
      elsif(@rolee == 'Caregiver')
        p 'b'
        @daily_checks = DailyCheck.where(:care_giver_master_id => params[:check_id]).order("entered_at")
      end
    else
      p "pa"
      if(@rolee == 'Patient')
        @patients = Patient.all
      elsif(@rolee == 'Caregiver')
        @caregivers = CareGiverMaster.all
        #@daily_checks = DailyCheck.where(:care_giver_master_id => params[:check_id]).last
      end
    end
                                        
    respond_to do |format|
      format.html do
      end
      format.json do
        #@daily_checks = DailyCheck.all.order("id")
        if(@check_id.present?)
          p "dcc"
          data = @daily_checks.map do |c|        
            if c.patient_id
              {:id => c.id, :description => c.description, :patient_id => c.patient_id, :entered_by => c.entered_by, :tag => c.tag , :name => c.patient_name, :formated_entered_at => c.formated_entered_at}
            else
              {:id => c.id, :description => c.description, :care_giver_master_id => c.care_giver_master_id, :entered_by => c.entered_by, :tag => c.tag , :name => c.care_giver_master_name, :formated_entered_at => c.formated_entered_at}          
            end
          end
          render :json => {:daily_check => data}
        else
          p "paa"
          if(@rolee == 'Patient')
            data = @patients.map do |p|
              @daily_check = DailyCheck.where(:patient_id => p.id).last
              if(@daily_check.present?)
                p  "penterd"
                p @daily_check.entered_at
                {:id => p.id, :name => p.name, :gender => p.gender, :dob => p.dob, :mobile => p.mobile_no, :date => @daily_check.entered_at.strftime("%d/%m/%Y-%H:%M") }
              else
                {:id => p.id, :name => p.name, :gender => p.gender, :dob => p.dob, :mobile => p.mobile_no, :date => p.created_at.strftime("%d/%m/%Y-%H:%M") }
              end
            end
            render :json => {:daily_check => data}
          elsif(@rolee == 'Caregiver')
            data = @caregivers.map do |c|
              @daily_check = DailyCheck.where(:care_giver_master_id => c.id).last
                if(@daily_check.present?)
                 p 'cap'
                 {:id => c.id, :name => c.name, :gender => c.gender, :dob => c.dob, :mobile => c.telephone_no, :date => @daily_check.entered_at.strftime("%d/%m/%Y-%H:%M") }
                else
                  p 'dap'
                  {:id => c.id, :name => c.name, :gender => c.gender, :dob => c.dob, :mobile => c.telephone_no, :date => c.created_at.strftime("%d/%m/%Y-%H:%M") }
                end
              end
              render :json => {:daily_check => data}
          end
          
        end
      
      end
    end
  end
  def create
    p "============"
    @daily_check = DailyCheck.new(daily_check_params)
    @daily_check.entered_at = DateTime.now
    @daily_check.entered_by = current_user.id
    #@daily_check.created_by = current_user.id
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @daily_check.save}
      end
    end

  end

  def update
    @daily_check = DailyCheck.find(params[:id])
    respond_to do |format|
      format.json do
        render json: {status: @daily_check.update(daily_check_params)}
      end
    end
  end

  def destroy
    @daily_check = DailyCheck.find(params[:id])
    respond_to do |format|
      format.json do
        render json: {status: @daily_check.destroy}
      end
    end
  end

  private
  def load_role
    @rolee = params[:role]
    @check_id = params[:check_id]
    p "______"
    p @rolee
  end
  def daily_check_params
    params.require(:daily_check).permit(:description, :patient_id, :care_giver_master_id, :tag)
  end
end
