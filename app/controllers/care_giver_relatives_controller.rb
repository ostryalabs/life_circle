class CareGiverRelativesController < ApplicationController
  before_action :load_care_giver_master
  load_resource :only => [:approve]
  before_action :authenticate_user!
  
  def index
    respond_to do |format|
      
      @care_giver_relatives = @care_giver_master.care_giver_relatives
      format.html{}
      format.json do
        if @care_giver_relatives.empty?
          @care_giver_relatives = 2.times.map{|i| CareGiverRelative.new{|cgr| cgr.care_giver_master_id = @care_giver_master.id}}
        end
        render :json => {:relatives => @care_giver_relatives}
      end
    end
  end

  def create
    respond_to do |format|
      @care_giver_relatives = []
      params[:care_giver_relatives].each do |local_params|
        if local_params[:name].present? and (not local_params[:id].present?)
          @care_giver_relatives.push(CareGiverRelative.new(care_giver_relative_params(local_params).merge!(:approved => false, :approved_by => current_user.id)))
        elsif local_params[:id].present?
          care_giver_relative = CareGiverRelative.find(local_params[:id])
          care_giver_relative.assign_attributes(care_giver_relative_params(local_params))
          @care_giver_relatives.push(care_giver_relative)
        end
      end
      format.html{}
      format.json do
        if @care_giver_relatives.map(&:valid?).all?
          status = @care_giver_relatives.map(&:save).all?
          @care_giver_master.check_children_approvals_and_update_self_approval
        else
          status = false
        end
          render :json => {:status => status}
      end
    end
  end

  def approve
    respond_to do |format|
      format.html do
      end
      format.json do
        status = @care_giver_relative.update({:approved => true, :approved_by => current_user.id})
        if status
          @care_giver_master.check_children_approvals_and_update_self_approval
        end
        render :json => {:status => status}
      end
    end
  end
  
  private
  
  def care_giver_relative_params(local_params)
    local_params.permit(:name, :age, :occupation, :contact_no, :relation, :address, :emergency_contact, :care_giver_master_id)
  end
  
  def load_care_giver_master
    @care_giver_master = CareGiverMaster.find(params[:care_giver_master_id])
  end

end
