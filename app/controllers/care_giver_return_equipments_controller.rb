class CareGiverReturnEquipmentsController < ApplicationController
  def index
    @care_giver_id = params[:care_giver_master_id]
    @care_giver = CareGiverMaster.find(params[:care_giver_master_id])
    @care_giver_payments = CareGiverDailyAttendance.where(:care_giver_master_id => params[:care_giver_master_id]).group_by { |m| m.date.beginning_of_month }
    p '111111111111'
    p @care_giver_payments
    @engagements = EnquiryCareGiver.where(:care_giver_master_id => params[:care_giver_master_id])
    if @care_giver.care_giver_return_equipments.empty?
      @care_giver_equipments = CareGiverEquipment.all.map do |equ|
        equipment = CareGiverReturnEquipment.new()
        equipment.care_giver_equipment_id = equ.id
        equipment.care_giver_master_id = @care_giver_id
        equipment
      end    
      
    else
      @care_giver_equipments = CareGiverEquipment.all.map do |equ|
        exists = @care_giver.care_giver_return_equipments.where(:care_giver_equipment_id => equ.id).first
        #p @enrollment.patient_adaptive_equipments.count
        #p "check exists"
        p exists
        if exists.present?
          exists.assigned = true
          exists
        else
          p "check__________________________"
          equipment = CareGiverReturnEquipment.new()
          equipment.care_giver_equipment_id = equ.id
          equipment.care_giver_master_id = @care_giver_id
          p equipment
          equipment
        end
      end
    end
    
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @care_giver_equipments
      end
    end
    
  end

  def save_equipments
    p "check equipments"
    p params[:equipments]
    @care_giver_equipments = []
    p 'params[:equipments]'
    p params[:equipments]
    @equipments = params[:equipments].select {|x| x[:assigned].present? && x[:assigned] == true}
    @equipments.each do |y|
      equipment = CareGiverReturnEquipment.new()
      equipment.care_giver_equipment_id = y[:care_giver_equipment_id]
      equipment.care_giver_master_id = y[:care_giver_master_id]
      @care_giver_equipments.push(equipment)
    end
    @care_giver = CareGiverMaster.find(params[:care_giver_master_id])
    @care_giver.care_giver_return_equipments.destroy_all
    @care_giver_equipments.map(&:save)
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @care_giver_equipments
      end
    end
  end 
end


