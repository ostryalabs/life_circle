class NewCityRequestsController < ApplicationController
  
  
  def acknowledge
    @new_city_request = NewCityRequest.find(params[:id])
  end
  
  
  def new
    respond_to do |format|
      format.html do 
        @new_city_request = NewCityRequest.new
      end
      format.json do
      end
    end

  end

  def create
    @new_city_request = NewCityRequest.new(city_request_params)
    respond_to do |format|
      format.html do 
        if @new_city_request.save
          redirect_to acknowledge_new_city_request_path(@new_city_request)
        end
      end
      format.json do
        render :json => {:status => @new_city_request.save}
      end
    end
  end

  private

  def city_request_params
    params.require(:new_city_request).permit(:city, :area, :email, :mobile_no , :cg_type, :name)
  end

  

end
