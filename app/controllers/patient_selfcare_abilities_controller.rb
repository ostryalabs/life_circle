class PatientSelfcareAbilitiesController < ApplicationController
  before_action :load_enrollment 

  def index
    @abilities = @enrollment.patient_selfcare_abilities
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @abilities
      end
    end
  end
  
  def create    
    @patient_ability = PatientSelfcareAbility.new(patient_selfcare_ability_params)
    #p @patient_ability
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @patient_ability.save}
      end
    end

  end

  def update
    @patient_ability = PatientSelfcareAbility.find(params[:id])
    respond_to do |format|
      format.json do
        render json: {status: @patient_ability.update(patient_selfcare_ability_params)}  
      end
    end
  end
 
  def destroy
    @patient_ability = PatientSelfcareAbility.find(params[:id])
    respond_to do |format|
      format.json do
       render json: {status: @patient_ability.destroy}
      end
    end
  end

  private

  def load_enrollment
    @enrollment_id = params[:enrollment_id]
    @enrollment = Enrollment.find(params[:enrollment_id])
  end

  def patient_selfcare_ability_params
    params.require(:patient_selfcare_ability).permit(:patient_selfcare_ability_master_id, :independent, :enrollment_id, :description)
  end
  
  # def index
  #   @enrollment_id = params[:enrollment_id]
  #   @enrollment = Enrollment.find(params[:enrollment_id])
  #   if @enrollment.patient_selfcare_abilities.empty?
  #     @patient_abilities = PatientSelfcareAbilityMaster.all.map do |equ|
      
  #       patient_ability = PatientSelfcareAbility.new()
  #       patient_ability.patient_selfcare_ability_master_id = equ.id
  #       patient_ability.enrollment_id = @enrollment_id
  #       patient_ability.created_by = current_user.id
  #       patient_ability
  #     end    
  #     staus_list = @patient_abilities.map(&:save)
  #   else
  #     @patient_abilities= @enrollment.patient_selfcare_abilities.order(:id);
  #   end
  #   p "check abilities"
  #   p @patient_abilities
  #   respond_to do |format|
  #     format.html do
  #     end
  #     format.json do
  #       render :json => @patient_abilities
  #     end
  #   end
  # end

  # def new
  #   @enrollment_id = params[:enrollment_id]
  # end
  
  # def update_patient_abilities
  #   respond_to do |format|
  #     format.json do
  #       # p "update the abili"
  #       # p params[:ability]
  #       # p "update_patient_abilities"
  #       # p @enrollment_id
  #       # p current_user.id
  #       @patient_params = params[:ability]
  #       abilities_history_creator = LifeCircle::PcpHistoryCreator.new(@enrollment_id, current_user)
  #       if abilities_history_creator.save_patient_abilities_history(@patient_params)         
  # 	  @patient_ability = PatientSelfcareAbility.where(:id => @patient_params[:id]);  	 
  # 	  @patient_ability[0][:independent] = @patient_params[:independent]
  # 	  @patient_ability[0][:description] = @patient_params[:description] 
  #         @patient_ability[0].save
  #       end
       
  #     end     
          
  #   end
  #   render :json => {:status => true, :patient_ability =>@patient_ability}
  # end

  
  
end


 
