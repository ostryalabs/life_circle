class PatientDisabilitiesController < ApplicationController
  def index
    @enrollment_id = params[:enrollment_id]
    @enrollment = Enrollment.find(params[:enrollment_id])
    if @enrollment.patient_disabilities.empty?
      @patient_disabilities = DisabilityMaster.all.map do |equ|
        disability = PatientDisability.new()
        disability.disability_master_id = equ.id
        disability.enrollment_id = @enrollment_id
        #disability.created_by = current_user.id
        disability
      end    
     
    else
      @patient_disabilities = DisabilityMaster.all.map do |equ|
        exists = @enrollment.patient_disabilities.where(:disability_master_id => equ.id).first
        #p @enrollment.patient_adaptive_disabilitys.count
        #p "check exists"
        p exists
        if exists.present?
          exists.assigned = true
          exists
        else
        disability = PatientDisability.new()
        disability.disability_master_id = equ.id
        disability.enrollment_id = @enrollment_id
        disability.patient_id = @enrollment.patient.id
        disability.assigned = false
        disability
        end
      end
    end

    respond_to do |format|
      format.html do
      end
      format.json do
      data = @patient_disabilities.map do |c|
      {:disability_master_id => c.disability_master_id,:disability_name => c.disability_name,:assigned => c.assigned}
      end
        render :json => data
      end
    end
    
  end

  def save_patient_disabilities
    p "check disabilitys"
    p params[:disabilities]
    @enrollment = Enrollment.find(params[:enrollment_id])
    @patient_disabilities = []
    @disabilities = params[:disabilities].select {|x| x[:assigned].present? && x[:assigned] == true}
    @disabilities.each do |y|
      disability = PatientDisability.new()
      disability.disability_master_id = y[:disability_master_id]
      disability.enrollment_id = params[:enrollment_id]
      disability.patient_id = @enrollment.patient.id
      @patient_disabilities.push(disability)
    end
    
    @enrollment.patient_disabilities.destroy_all
    @patient_disabilities.map(&:save)
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @patient_disabilities
      end
    end
  end 
end
