class TicketConversationsController < ApplicationController
   load_resource
  before_action :load_parent
  before_action :authenticate_user!

  def index
    @ticket_conversations = @parent.ticket_conversations.order("entered_at desc")
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:conversations => @ticket_conversations, :parent_tag => @parent.current_tag}
      end
    end
  end
  
  def create
    @ticket_conversation = TicketConversation.new(ticket_conversation_params)
    @ticket_conversation.entered_at = DateTime.now
    @ticket_conversation.entered_by = current_user.id
    ActiveRecord::Base.transaction do
      status = @ticket_conversation.save
     
      if status and @ticket_conversation.tag.present?
        @parent.update_attributes({:current_tag => @ticket_conversation.tag})
        session[:return_to] = :back
        redirect_to session.delete(:return_to)       
      end
    end
    respond_to do |format|
      format.html do
      end
      format.json do
        #render json: {status: status}
      end
    end
  end

  def update
    respond_to do |format|
      format.json do
        render json: {status: @ticket_conversation.update(ticket_conversation_params)}
      end
    end
  end

  private

  def load_parent
    @ticket_id = params[:ticket_id]
    @name = params[:name]
    @role = params[:role]
    @parent = Ticket.find(params[:ticket_id])   
  end

  def ticket_conversation_params
    params.require(:ticket_conversation).permit(:ticket_id, :description, :tag)
  end
end
