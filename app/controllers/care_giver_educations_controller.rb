class CareGiverEducationsController < ApplicationController
  before_action :load_care_giver_master
  load_resource :only => [:approve]
  before_action :authenticate_user!

  def education_for_registration
    @care_giver_educations = @care_giver_master.care_giver_educations
    respond_to do |format|
      format.html{}
      format.json do
        if @care_giver_educations.empty?
          @care_giver_educations = 2.times.map{|i| CareGiverEducation.new{|cgr| cgr.care_giver_master_id = @care_giver_master.id}}
        end
        render :json => {:degrees => @care_giver_educations}
      end
    end
  end
  
  def create
    respond_to do |format|
      format.html{}
      format.json do
        @care_giver_education = params[:id].present? ? CareGiverEducation.find(params[:id]) : CareGiverEducation.new
        @care_giver_education.assign_attributes(care_giver_education_params(params).merge!(:approved => false, :approved_by => current_user.id))
        @care_giver_education.care_giver_master = @care_giver_master
        render :json => {:status => @care_giver_education.save}
      end
    end
  end

  def save_education
    respond_to do |format|
      @care_giver_educations = []
      params[:care_giver_educations].each do |local_params|
        if local_params[:id].present?
          care_giver_education = CareGiverEducation.find(local_params[:id])
          care_giver_education.assign_attributes(care_giver_education_params(local_params).merge(:approved => false, :approved_by => current_user.id))
          care_giver_education.care_giver_master =  @care_giver_master
          @care_giver_educations.push(care_giver_education)
        else
          care_giver_education = CareGiverEducation.new(care_giver_education_params(local_params))
          care_giver_education.care_giver_master =  @care_giver_master
          @care_giver_educations.push(care_giver_education)
        end
      end
      format.html{}
      format.json do
        if @care_giver_educations.map(&:valid?).all?
          status = @care_giver_educations.map(&:save).all?
          @care_giver_master.check_children_approvals_and_update_self_approval
        else
          status = false
        end
          render :json => {:status => status}
      end
    end
  end
  
  def approve
    respond_to do |format|
      format.html do
      end
      format.json do
        status = @care_giver_education.update({:approved => true, :approved_by => current_user.id})
        @care_giver_master.check_children_approvals_and_update_self_approval
        render :json => {:status => status}
      end
    end
  end
  
  
  def cg_dob
    respond_to do |format|
      format.html do
      end
      format.json do
        status = @care_giver_master.dob
        render :json => {:status => status}
      end
    end
  end

  private

  def care_giver_education_params(local_params)
    local_params.permit(:institute, :period_from, :period_to, :exam_passed, :marks_percentage, :highest_qualification, :professional_nursing_qualification, :attachment )
  end
  
  def load_care_giver_master
    @care_giver_master = CareGiverMaster.find(params[:care_giver_master_id])
  end


  
end
