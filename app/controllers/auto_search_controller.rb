class AutoSearchController < ApplicationController
  before_action :authenticate_user!
  autocomplete :drug_master, :name, :full => true, :extra_data => [:route_master_id, :form_master_id, :unit_master_id,:route_name]
  def autocomplete_user_details
    users = User.having_no_active_enrollments(User.where("(lower(user_name) ILIKE '%#{params[:term]}%') or (lower(mobile_no) ILIKE '%#{params[:term]}%') or (lower(email) ILIKE '%#{params[:term]}%')").where(:role => "patient"))
    render :json => users.map{ |user| {:id => user.id, :label => "#{user.mobile_no}-#{user.email}"}}
  end
  
  def autocomplete_drug_master_names
    drugs = DrugMaster.where("lower(name) ILIKE '%#{params[:term]}%'")
    render :json => drugs.map{ |drug| {:id => drug.id, :label => "#{drug.name}",:form =>"#{drug.form_name}", :unit =>"#{drug.unit_name}", :route => "#{drug.route_name}" }}
  end
  
  def autocomplete_caregiver_master_names
    caregivers = CareGiverMaster.where("lower(first_name) ILIKE '%#{params[:term]}%'")
    render :json => caregivers.map{ |caregiver| {:id => caregiver.id, :label => "#{caregiver.first_name}, #{caregiver.last_name}, #{caregiver.father_name}" }}
  end 
  
  def autocomplete_patient_names
    patients = Patient.where("lower(first_name) ILIKE '%#{params[:term]}%'")
    render :json => patients.map{ |patient| {:id => patient.id, :label => "#{patient.first_name}, #{patient.dob}, #{patient.mobile_no}" }}
  end
end
