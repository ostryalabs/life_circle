class JobRunsController < ApplicationController
  load_resource :only => [:show, :mail_all_payments]
  #authorize_resource

  def index
    page = params[:page].present? ? params[:page] : 1
    @job_runs = JobRun.matching_code(JobRun::INVOICE_GENERATION) #.paginate(:page => page)
  end

  def show
    respond_to do |format|
      format.html {}
      format.json do 
        page = params[:page].present? ? params[:page] : 1
        @payment_masters = @job_run.payment_masters.paginate(:page => page)
        render :json => JsonPagination.pagination_entries(@payment_masters).merge!(:payment_masters => @payment_masters)
      end
    end
    
  end

  def schedule_monthly_invoice
    job_run = JobRun.matching_code(JobRun::INVOICE_GENERATION).in_the_current_month(Date.today).first
    unless job_run.present? and (job_run.scheduled? or job_run.finished?)
      invoice_job = LifeCircle::MonthlyInvoiceGenerationJob.new(current_user, Date.today)
      Delayed::Job.enqueue invoice_job
      result_link = "<a href=\"/job_runs/#{invoice_job.job_run_id}\">here</a>"
      flash[:success] = I18n.t :success, :scope => [:job, :schedule], job: JobRun::INVOICE_GENERATION.titleize, job_link: result_link
    else
      result_link = "<a href=\"/job_runs/#{job_run.id}\">here</a>"
      if job_run.scheduled?
        flash[:alert] = I18n.t :already_scheduled, :scope => [:job, :schedule], job: JobRun::INVOICE_GENERATION.titleize, job_link: result_link
      elsif job_run.finished?
        flash[:alert] = I18n.t :already_finished, :scope => [:job, :schedule], job: JobRun::INVOICE_GENERATION.titleize, job_link: result_link
      end
    end
    redirect_to job_runs_path
  end

  def mail_selected_payments
    respond_to do |format|
      format.json do 
        payment_ids = ((params[:payment_master_ids].is_a? String) ? [params[:payment_master_ids]] : params[:payment_master_ids])
        payment_ids.each do |id| 
          pyament_master =  PaymentMaster.find(id) 
          PaymentMasterMailer.invoice(pyament_master, ApplicationDefault.default_cc_email).deliver_later
          pyament_master.mark_as_mail_sent
        end
        render :json => {:staus => true}
      end
    end

  end

  def mail_all_payments
    respond_to do |format|
      format.json do 
        @job_run.payment_masters.mail_not_sent.each do |pyament_master| 
          PaymentMasterMailer.invoice(pyament_master, ApplicationDefault.default_cc_email).deliver_later
          pyament_master.mark_as_mail_sent
        end
        render :json => {:status => true}
      end
    end

  end

  def nullify_selected_payments
    respond_to do |format|
      format.json do 
        status_list = []
        ActiveRecord::Base.transaction do
          payment_ids = ((params[:payment_master_ids].is_a? String) ? [params[:payment_master_ids]] : params[:payment_master_ids])
          status_list = payment_ids.map do |id| 
            pyament_master =  PaymentMaster.find(id) 
            pyament_master.mark_as_nullified
          end
        end
        render :json => {:status => status_list.all?}
      end
    end

  end


end
