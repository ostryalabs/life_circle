class RequestedServicesController < ApplicationController

   def index
    @enrollment_id = params[:enrollment_id]
    @enrollment = Enrollment.find(params[:enrollment_id])
    if @enrollment.enquiry.requested_services.empty?
      @requested_services = ServiceMaster.all.map do |equ|
        service = RequestedService.new()
        service.service_master_id = equ.id
        service.enquiry_id = @enrollment.enquiry_id
        service.patient_id = @enrollment.patient.id
        service
      end    
     
    else
      @requested_services = ServiceMaster.all.map do |equ|
        exists = @enrollment.enquiry.requested_services.where(:service_master_id => equ.id).first
        #p @enrollment.patient_adaptive_services.count
        #p "check exists"
        p exists
        if exists.present?
          exists.assigned = true
          exists
        else
        service = RequestedService.new()
        service.service_master_id = equ.id
        service.enquiry_id = @enrollment.enquiry_id
        service.patient_id = @enrollment.patient.id
        service.assigned = false
        service
        end
      end
    end

    respond_to do |format|
      format.html do
      end
      format.json do
      data = @requested_services.map do |c|
      {:service_master_id => c.service_master_id, :service_name => c.service_name ,:assigned => c.assigned
      }
      end
        render :json => data
      end
    end
    
  end
  
  def enrolled
  @enrollment_id = params[:enrollment_id]
  @enrollment = Enrollment.find(params[:enrollment_id])
  @requested_services = @enrollment.enquiry.requested_services
  respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @requested_services
      end
    end
  end
  
  def save_requested_services
    p "check services"
    p params[:services]
    @enrollment = Enrollment.find(params[:enrollment_id])
    @enquiry_caregiver = EnquiryCareGiver.find_by_id(@enrollment.enquiry_id)
    @caregiver_services = CareGiverService.where(:care_giver_master_id => @enquiry_caregiver.care_giver_master_id)    
    p @caregiver_services
    # p @enrollment.enquiry
    @r = []
    @s =[]
    @not_saved =[]
    @requested_services = []
    @services = params[:services].select {|x| x[:assigned].present? && x[:assigned] == true}
    # p @caregiver_services
    # p @services
    # @services.each do |x|
    #   @caregiver_services.each do |z|
    #     if x[:service_master_id] != z.service_master_id
    #       p 'samee'
    #       bre
    #     end
    #   end
    # end
    
    # a= @services.select(:id).map(&:id)
    p "#######################"
    p @services
    @caregiver_services.each do |r|
      @s.push(r[:service_master_id])
    end
    @services.each do |c|
      @r.push(c[:service_master_id])
    end
    p @r
    p @s
    @not_in = @r-@s
    @services.each do |c|
      @not_in.each do |d|
        if(d == c[:service_master_id])
        @not_saved.push(c[:service_name])
        end
      end
    end
      
    # @services.each do |y|
    #   if @services.include?(@caregiver_services.each)
    #     @not_saved.push(y[:service_name])           
    #   else
    #     service = RequestedService.new()
    #     service.service_master_id = y[:service_master_id]
    #     service.enquiry_id = @enrollment.enquiry_id
    #     service.patient_id = @enrollment.patient.id
    #     @requested_services.push(service)     
    #   end
      
    # end
    
    @services.each do |y|
      @caregiver_services.each do |z|
       p "======"
       p y[:service_master_id] 
       p z[:service_master_id]
       if y[:service_master_id] == z[:service_master_id]
         service = RequestedService.new()
         service.service_master_id = y[:service_master_id]
         service.enquiry_id = @enrollment.enquiry_id
         service.patient_id = @enrollment.patient.id
         @requested_services.push(service)          
          next
        end
      end      
    end
    @notsaved = @not_saved.uniq
    @enrollment.enquiry.requested_services.destroy_all
    @requested_services.map(&:save)
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:requested_services => @requested_services, :not_saved => @notsaved.join(",")}
      end
    end 
  end 
end
