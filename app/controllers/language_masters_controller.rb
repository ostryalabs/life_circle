class LanguageMastersController < ApplicationController
  load_resource
  def index
    @language_masters = LanguageMaster.select(:id, :language).all.order("language")
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @language_masters
      end
    end
  end
  

  def create
    @language_master = LanguageMaster.new(language_master_params)
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @language_master.save}
      end
    end

  end

  def update
    respond_to do |format|
      format.json do
        render json: {status: @language_master.update(language_master_params)}
      end
    end
  end

  def destroy
    respond_to do |format|
      format.json do
        render json: {status: @language_master.destroy}
      end
    end
  end

  private
  
  def language_master_params
    params.require(:language_master).permit(:language, :description)
  end
end
