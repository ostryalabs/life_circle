class CareGiverChargesController < ApplicationController
  load_resource
  before_action :load_care_giver_master
  before_action :authenticate_user!
  
  def new_or_existing_charges
    if @care_giver_master.care_giver_charge.present? 
      @care_giver_charge =  @care_giver_master.care_giver_charge  
    else
      @care_giver_charge = CareGiverCharge.new(:care_giver_master_id => @care_giver_master.id, :base_charge => @care_giver_master.expected_sal_per_day)
    end
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:care_giver_charge => @care_giver_charge }
      end
    end

  end

  def save_charges
    if params[:care_giver_charge][:id].present?
      @care_giver_charge =  CareGiverCharge.find(params[:care_giver_charge][:id])
    else
      @care_giver_charge = CareGiverCharge.new
    end
    @care_giver_charge.assign_attributes(care_giver_charge_params.merge(:approved => false, :approved_by => current_user.id))
    @care_giver_charge.modified_by = current_user.id
    status = @care_giver_charge.save
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:status => status }
      end
    end
  end


  def index
    @care_giver_charges = CareGiverCharge.all
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @care_giver_charges
      end
    end
  end
  
  def create
    @care_giver_charge = CareGiverCharge.new(care_giver_charge_params.merge(:approved => false, :approved_by => current_user.id))
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @care_giver_charge.save}
      end
    end
  end

  def update
    respond_to do |format|
      format.json do
        render json: {status: @care_giver_charge.update(care_giver_charge_params.merge(:approved => false, :approved_by => current_user.id))}
      end
    end
  end

  def destroy
    respond_to do |format|
      format.json do
        render json: {status: @care_giver_charge.destroy}
      end
    end
  end

  def approve
    respond_to do |format|
      format.html do
      end
      format.json do
        status = @care_giver_charge.update({:approved => true, :approved_by => current_user.id})
        render :json => {:status => status}
      end
    end
  end


  private
  
  def care_giver_charge_params
    params.require(:care_giver_charge).permit(:base_charge, :margin, :margin_type, :care_giver_master_id)
  end

  def load_care_giver_master
    @care_giver_master = CareGiverMaster.find(params[:care_giver_master_id])
  end

end
