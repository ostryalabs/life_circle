class DrugQuantityMastersController < ApplicationController
  before_action :load_enrollment 
  load_resource
  def index
    #@drug_quantity =Enrollment.find(@enrollment_id)
    @drug_quantit = @enrollment.drug_quantity_masters.select(:id, :quantity, :sos, :review_date, :refill_date, :dose, :drug_master_id, :enrollment_id)
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @drug_quantit
      end
    end
  end
  def create
    p """==="""
    p params[:slots].length
    @slots = params[:slots]      
    @slots.each do |x|
      p x[:time1]
      p x[:time2]
      p x[:time3]
    end
    @drug_quantity = DrugQuantityMaster.new(drug_quantity_master_params)
    @drug_quantity.created_by = current_user.id
    if params[:check]
      @drug_quantity.review_date = Date.today + 100.years
    end
   respond_to do |format|
      format.html do
      end
      format.json do
        if @drug_quantity.save
          @status =true
          @slots = params[:slots]         
           @slots.each do |x|
             x.each do | k , v|
               if v != ""
               @time_of_administration = TimeOfAdministration.new()
               @time_of_administration.drug_quantity_master_id = @drug_quantity.id
               @time_of_administration.time = v
               @time_of_administration.save
               @status =true
               end
             end           
           end
          
        end
        render json: {:status => @status, :drug_quantity => @drug_quantity}
      end
    end

  end

  def update
    @drug_quantity = DrugQuantityMaster.find(params[:id])
    respond_to do |format|
      format.json do
        render json: {status: @drug_quantity.update(drug_quantity_master_params)}
      end
    end
  end

  def destroy
    @drug_quantity = DrugQuantityMaster.find(params[:id])
    @drug_quantity.time_of_administrations.destroy_all
    respond_to do |format|
      format.json do
        if @drug_quantity.destroy
          @status = true
        else 
          @status = false
        end
        render json: {status: @status}
      end
    end
  end

  private
  def load_enrollment
    @enrollment_id = params[:enrollment_id]
    @enrollment = Enrollment.find(params[:enrollment_id])
    @created_by = current_user.id
    
  end
  
  def drug_quantity_master_params
    params.require(:drug_quantity_master).permit(:quantity, :sos, :review_date, :refill_date, :dose, :drug_master_id, :enrollment_id)
  end
  
end
