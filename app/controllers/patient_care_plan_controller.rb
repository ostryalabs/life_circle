class PatientCarePlanController < ApplicationController
  
  before_action :load_enrollment
  
  
  def index
  end
  
   private

  def load_enrollment
    @enrollment_id = params[:enrollment_id]
    @enrollment = Enrollment.find(params[:enrollment_id])
  end
end
