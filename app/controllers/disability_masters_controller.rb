class DisabilityMastersController < ApplicationController
  load_resource
  def index
    @disabilities = DisabilityMaster.select(:id, :disability).order("disability")
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @disabilities
      end
    end
  end
  

  def create
    @disability_master = DisabilityMaster.new(disability_master_params)
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @disability_master.save}
      end
    end

  end

  def update
    respond_to do |format|
      format.json do
        render json: {status: @disability_master.update(disability_master_params)}
      end
    end
  end

  def destroy
    respond_to do |format|
      format.json do
        render json: {status: @disability_master.destroy}
      end
    end
  end

  private
  
  def disability_master_params
    params.require(:disability_master).permit(:disability, :description)
  end
end
