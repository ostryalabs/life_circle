class PatientRateMastersController < ApplicationController
  load_resource
  
  def index
    @patient = PatientRateMaster.all.order("id")
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @patient
      end
    end
  end
  def create
    @patient_rate_master = PatientRateMaster.new(patient_rate_master_params)
    #@patient_rate_master.created_by = current_user.id
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @patient_rate_master.save}
      end
    end

  end

  def update
    respond_to do |format|
      format.json do
        render json: {status: @patient_rate_master.update(patient_rate_master_params)}
      end
    end
  end

  def destroy
    respond_to do |format|
      format.json do
        render json: {status: @patient_rate_master.destroy}
      end
    end
  end

  private
  
  def patient_rate_master_params
    params.require(:patient_rate_master).permit(:from, :to, :percent)
  end
end
