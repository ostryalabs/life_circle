class EnquiryEnollmentsController < ApplicationController

  protected
  def search_params
    enquiry_params = params[:enquiry]
    search_params = {}
    search_params[:languages] = enquiry_params[:languages].select{|lan| (lan[:assigned].present? and lan[:assigned] == true)}.map{|l| l[:id]}
    search_params[:requested_services] = enquiry_params[:services].select{|ser| (ser[:assigned].present? and ser[:assigned] == true)}.map{|s| s[:id]}
    search_params[:service_required_from] = enquiry_params[:service_required_from]
    search_params[:service_required_to] = enquiry_params[:service_required_to]
    search_params[:gender] = (enquiry_params[:gender].present? ? enquiry_params[:gender] : enquiry_params[:care_giver_gender])
    if enquiry_params[:requested_time_slots].present?
      search_params[:time_slots] = enquiry_params[:requested_time_slots].select{|ts| (ts[:time_from].present?)}
    end
    
    search_params[:travel_distance] = enquiry_params[:travel_distance]
    if enquiry_params[:patient_address].present?
      search_params[:patient_address] =  enquiry_params[:patient_address].values.join(",")
    else
      search_params[:patient_address] = enquiry_params[:place_details]
    end
    search_params[:open_term] = enquiry_params[:open_term]
    search_params[:affordable_price] = enquiry_params[:affordable_price]
    search_params[:caregiver_stay] = enquiry_params[:caregiver_stay]
    
    search_params
  end
  
  
  def required_enquiry_params
    enquiry_params = params.require(:enquiry).permit(:mentioned_registration_type, :care_giver_gender, :require_heavy_patient_lifting, :no_of_care_givers, :caregiver_stay, :area_master_id, :service_required_to, :service_required_from, :open_term, :travel_distance, :appointment_time, :mobile_no, :email, :requested_user_name, :missed_call_request_id, :form_filled_location, :address_attributes => [:line1, :line2, :city, :area, :state, :country, :pin_code])
    enquiry_params[:affordable_price] = params[:enquiry][:affordable_price][:key] if params[:enquiry][:affordable_price].present?
    enquiry_params
  end



end
