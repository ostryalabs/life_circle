class SessionsController  < Devise::SessionsController
  #protect_from_forgery with: :null_session
  respond_to :json
  #allow_params_authentication! :exept => [:create]
  def create
    super do |user|   
    end
  end

  def get_current_user
    respond_to do |format|
      format.json do
        render :json => {:current_user => current_user}
      end
    end
  end
end
