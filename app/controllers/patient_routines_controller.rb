class PatientRoutinesController < ApplicationController
  load_resource
  before_action :load_enrollment

  def index
    @routine = @enrollment.patient_routines.order("time")
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => [@routine]
      end
    end
  end
  
  def create    
    @patient_routine = PatientRoutine.new(patient_routine_params)
    @patient_routine.created_by = current_user.id
    #p @patient_routine
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @patient_routine.save}
      end
    end

  end

  def update_patient_routine  
    respond_to do |format|
      format.json do
        @routine = params[:patient_routine]
        @patient_routine = PatientRoutine.where(:id => @routine[:id])
        @patient_routine[0].routine_master_id = @routine[:routine_master_id]
        @patient_routine[0].time = @routine[:time]
        @patient_routine[0].save
        #render json: {status: @patient_routine)} 
      end
    end
     render :json => {:status => true, :patient_routine =>@patient_routine}  
     
  end
 
  def destroy
    respond_to do |format|
      format.json do
        # p 'sdgmlmgdslddl'
     	# p params[:id]
        # new = PatientRoutine.find(params[:id])
        # new.delete
        # render :json => true
        if @patient_routine.destroy
          status = true
        else
          status = false
        end
       render json: {status: status}
      end
    end
  end


  private

  def load_enrollment
    @enrollment_id = params[:enrollment_id]
    @enrollment = Enrollment.find(params[:enrollment_id])
  end

  def patient_routine_params
    params.require(:patient_routine).permit(:routine_master_id, :time, :enrollment_id, :created_by)
  end
end
