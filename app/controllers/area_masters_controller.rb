class AreaMastersController < ApplicationController
  load_resource
  before_action :load_city_master

  def index
    @area_masters = @city_master.area_masters.order("name")
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @area_masters
      end
    end
  end
  
  def create
    @area_master = AreaMaster.new(area_master_params)
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @area_master.save}
      end
    end

  end

  def update
    respond_to do |format|
      format.json do
        render json: {status: @area_master.update(area_master_params)}
      end
    end
  end

  def destroy
    respond_to do |format|
      format.json do
        render json: {status: @area_master.destroy}
      end
    end
  end


  private

  def load_city_master
    @city_master = CityMaster.find(params[:city_master_id])
  end

  def area_master_params
    params.require(:area_master).permit(:name, :city_master_id)
  end
  

end
