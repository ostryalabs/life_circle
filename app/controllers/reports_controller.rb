class ReportsController < ApplicationController
  def index
  end
  
  def age_report
    
    @p = PendingBillAfterPayment.where(:status =>  "PENDING").map do |x|
      @n = Enrollment.where(:id => x.enrollment_id)
      @b = PaymentBill.where(:payment_master_id => x.payment_master_id)
      @p = PaymentMaster.where(:id =>x.payment_master_id )
      if @b.present? 
        amount = @b[0][:amount_paid]
        if @p[0][:total_amount_in_rupees]
          @balance = @p[0][:total_amount_in_rupees] - amount
        end
      end
      p '222222222222222222222'
      p @p
      if @balance
        date = (Date.today - @p[0][:invoice_generation_date].to_date).to_i
      else 
        date = 0
      end
      p '11111111111111111111111111111'
      p date
      {
        :status => x.status , :date => @p[0][:invoice_generation_date] ,  :invoice_no => @p[0][:invoice_no] , :name => @n[0].patient.name , :amount => @p[0][:total_amount_in_rupees] , :balance => @balance , :age => date  
        
      }
    end
    render :json => @p
  end

  def credit
    @report =  params[:report]
    if @report[:status] = 'all'
      @c = CreditNote.where(:credit_note_date => @report[:from_date]..@report[:to_date])
    else
      @c = CreditNote.where(:credit_note_date => @report[:from_date]..@report[:to_date]).where(:status => @report[:report_type])
    end
    @p = @c.map do |x|
      n = Enrollment.where(:id => x.payment_master.enrollment_id)
      {
        :status => x.status , :date => x.credit_note_date , :name => n[0].patient.name ,  :amount => x.value_of_credit_note
      }
    end
    render :json => @p
  end


  def payment_report
    @report =  params[:report]
    @p = PaymentBill.where(:payment_date => @report[:from_date]..@report[:to_date]).map do |x|
      n = Enrollment.where(:id => x.payment_master.enrollment_id)
      {
        :name => n[0].patient.name ,  :date => x.payment_date , :mode=> x.payment_mode , :amount => x.amount_paid , :description => x.description_1 , :invoice_no => x.payment_master.invoice_no
      }
    end
    render :json => @p
  end

  def invoice_details
    @report =  params[:report]
    @p = PaymentMaster.where(:invoice_generation_date => @report[:from_date]..@report[:to_date]).map do |x|
      @n = Enrollment.where(:id => x.enrollment_id)
      @b = PaymentBill.where(:payment_master_id => x.id)
      if @b.present? 
        amount = @b[0][:amount_paid]
        if x.total_amount_in_rupees
          @balance = x.total_amount_in_rupees - amount
        else
          @balance = 0
        end
      end
      {
        :status => x.status , :invoice_date => x.invoice_generation_date , :due_date => x.due_date , :invoice_no => x.invoice_no , :name => @n[0].patient.name , :amount => x.total_amount_in_rupees , :balance => @balance  
        
      }
    end
    render :json => @p
    
  end
  

  def balance_report
    @report =  params[:report]
    @p = PaymentMaster.where(:invoice_generation_date => @report[:from_date]..@report[:to_date]).group_by(&:enrollment_id)
    @json = []
    p = @p.keys.sort()
    @json = []
    p.each do |z|
      @array = []
      @p[z].each do |y|
        @c = CreditNote.where(:payment_master_id => y.id).where(:status => 'PENDING').sum(:value_of_credit_note)
        @array << y.total_amount_in_rupees
      end
      @x = @array.inject{|sum , x| sum + x}
      if @x
        balance = @x - @c
      else
        balance = @x
      end
      n = Enrollment.where(:id => z)
      
      @json << {:name => n[0].patient.name, :amount => @x , :credits => @c , :balance => balance  }
    end
    render :json => @json
  end

  


  def get_report
    @report =  params[:report]
    @p = PaymentMaster.where(:invoice_generation_date => @report[:from_date]..@report[:to_date])
    @p.each do |x|
      p '12121212'
      p x.enrollment.patient.name
      p x.total_amount
    end
    @p = PaymentMaster.where(:invoice_generation_date => @report[:from_date]..@report[:to_date]).group_by(&:enrollment_id)
    @json = []
    p = @p.keys.sort()
    @json = []
    p.each do |z|
      @array = []
      @p[z].each do |y|
        @array << y.amount_in_rupees
      end
      @x = @array.inject{|sum , x| sum + x}
      
      n = Enrollment.where(:id => z)
      
      @json << {:name => n[0].patient.name, :amount => @x , :count => @array.count  }
    end
    render :json => @json
  end

  
  
end
