class PatientsController < ApplicationController
  before_action :authenticate_user!

  def service
  end
  
  def list_care_givers
  end

  def index
    respond_to do |format|
      format.html do
        if current_user.role == 'l_c_agent'
          @patients = Patient.all
        end
        if current_user.role == 'super_care_giver'
          @enrollments =  Enrollment.where(:super_care_giver_id => current_user.id)
          @enrollments.each do |y|
            p 'navya'
            p @patients
            @patients = Patient.where(:id => y.patient_id)
          end
        end
        if current_user.role == 'care_giver'
          user = CareGiverMaster.where(:user_id => current_user.id)
          @enq = EnquiryCareGiver.where(:care_giver_master_id => user[0][:id])
          
          if @enq.present?
            @enq.each do |x|
              @enrollments =  Enrollment.where(:enquiry_id => x[:enquiry_id])
            end
            @enrollments.each do |y|
              p 'navya'
              p @patients
              @patients = Patient.where(:id => y.patient_id)
            end
          end
        end
        
        if current_user.role == 'patient'
          @patients = Patient.where(:user_id => current_user.id)
        end
        
      end
      format.json do
      end
    end
  end
  

  def show
    @patient = Patient.find(params[:id])
    @enrollment = Enrollment.find_by_patient_id(params[:id])
   
  end

  def get_patient_details_with_user_id
    @patient = Patient.where(:user_id => params[:user_id]).first
    respond_to do |format|
      format.html do
      end
      format.json do
        patient_details = {:user_name => @patient.user.user_name, :email => @patient.user.email, :mobile_no => @patient.user.mobile_no, :patient_name => @patient.name, :patient_id => @patient.id }
        render :json => @patient, :include => {:guardian => {:include => :address}, :address => {:except => [:created_at, :updated_at]}}
      end
    end
  end

  def create_patient_from_user_params
    respond_to do |format|
      format.html do
      end
      format.json do
        status = false
        ActiveRecord::Base.transaction do
          @user = User.new(:user_name => params[:user][:email], :email => params[:user][:email] , :mobile_no => params[:user][:mobile_no], :password =>User.generate_password_on_admin_creation, :role => User::PATIENT)
          status = @user.save
          if status 
            @patient = Patient.new(:first_name => params[:user][:patient_name], :user_id => @user.id, :mobile_no => params[:user][:mobile_no])
            @patient.save
          end
        end
        if status
          patient_details = {:user_name => @patient.user.user_name, :email => @patient.user.email, :mobile_no => @patient.user.mobile_no, :patient_name => @patient.name, :patient_id => @patient.id }
          render :json => {:status => true, :patient => patient_details}
        else
          render :json => {:status => false, :error_msg => @user.errors.messages.map{|k, v| "#{k} #{v.join(' , ')}"}.join("<br/>")}
        end
      end
    end

  end
  
  private
  

end
