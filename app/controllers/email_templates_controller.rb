class EmailTemplatesController < ApplicationController
  def index
    @emails = EmailTemplate.all
  end
  
  def new
    @email = EmailTemplate.new
  end

  def edit
    @email = EmailTemplate.find(params[:id])
    @template = TemplateText.find_by_id(@email.template_text_id)
  end


  def update
    @email = EmailTemplate.find(params[:id])
    respond_to do |format|
      if @email.update(email_params)
        if params[:sms].present?
          format.html { redirect_to show_sms_email_template_path, notice: 'Template was successfully updated.' }
        else
          format.html { redirect_to @email, notice: 'Template was successfully updated.' }
          
        end
        format.json { head :no_content }
      else
        format.html { render action: 'new' }
        format.json { render json: @email.errors, status: :unprocessable_entity }
      end
    end
  end

  
  def create
    @email = EmailTemplate.new(email_params)
    if @email.save
      redirect_to email_templates_path
    else
      render 'new'
    end
  end

  def show
    @email = EmailTemplate.find(params[:id])
    @template = TemplateText.find_by_id(@email.template_text_id)
  end
  
  def edit_sms
    @email = EmailTemplate.find(params[:id])
    @template = TemplateText.find_by_id(@email.template_text_id)
  end

  def show_sms
    @email = EmailTemplate.find(params[:id])
    @template = TemplateText.find_by_id(@email.template_text_id)
  end

  def email_params
    params.require(:email_template).permit(:name, :text1 , :text2 , :template_text_id)
  end

end
