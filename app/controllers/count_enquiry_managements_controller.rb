class CountEnquiryManagementsController < ApplicationController
  def index
    @enquiries = Enquiry.includes(:patient).where.not(status: "EMPTY_RESULTS").count
    @missed_calls = MissedCallRequest.all.count
    @new_city = NewCityRequest.all.count
    @no_cg = Enquiry.includes(:patient).where(status: "EMPTY_RESULTS").count
    @appointments = Enquiry.where(:appointment_status => "Open").count
    @high = Enquiry.where(:current_tag => "Potential-High").count
    @low =Enquiry.where(:current_tag => "Potential-Low").count
    @non_prospect = Enquiry.where(:current_tag => "Non-prospect").count
    @audit = Enquiry.where(:current_tag => "To Audit").count
    @closed = Enquiry.where(:current_tag => "Closed").count
    @enrolled = Enquiry.includes(:patient).enrolled.count
    @enrollments = Enrollment.where(:status =>'ACTIVE')
    @all = []
    @enrollments.each do |x|
      if PatientAdaptiveEquipment.where(:enrollment_id => x.id).exists? and PatientSelfcareAbility.where(:enrollment_id => x.id).exists? and PatientRoutine.where(:enrollment_id => x.id).exists? and DrugQuantityMaster.where(:enrollment_id => x.id).exists? and VitalTrend.where(:enrollment_id => x.id).exists? and CareToBeProvidedPlan.where(:enrollment_id => x.id).exists?
      else
        @all << x
      end
    end
    @incomplete_pcp = @all.count
    @closed =  Enrollment.where(:status => 'CLOSED').count
    @job_applicant_list = CareGiverEnquiry.all.where(:status => 'Open').count
    @short_list = CareGiverEnquiry.where(:status => 'Recruit').count
    @not_approved = CareGiverEnquiry.where(:status => 'Documents Collected').count
    @hold_list = CareGiverEnquiry.where(:status => 'Hold').count
    @cg_new_city = CgNewCityRequest.all.count
    @cg_closed = CareGiverEnquiry.where(:status => 'Rejected').count
    @patients_new_tickets = Ticket.where(:role => 'Patient',:current_tag => 'New').count
    @patients_open_tickets =  Ticket.where(:role => 'Patient',:current_tag => 'Open').count
    @patients_waiting = Ticket.where(:role => 'Patient',:current_tag => 'Waiting for Inputs').count
    @patients_waiting_for_closed = Ticket.where(:role => 'Patient',:current_tag => 'Waiting for Closed').count
    @ticket_closed = Ticket.where(:role => 'Patient',:current_tag => 'Closed').count
    @cg_new_tickets = Ticket.where(:role => 'Caregiver',:current_tag => 'New').count
    @cg_open_tickets =  Ticket.where(:role => 'Caregiver',:current_tag => 'Open').count
    @cg_waiting = Ticket.where(:role => 'Caregiver',:current_tag => 'Waiting for Inputs').count
    @cg_waiting_for_closed = Ticket.where(:role => 'Caregiver',:current_tag => 'Waiting for Closed').count
    @cg_ticket_closed = Ticket.where(:role => 'Caregiver',:current_tag => 'Closed').count
    
  end

  def all_cities
    respond_to do |format|
      format.json do
        @care = CityMaster.all
      end
      render :json => CityMaster.all
    end
  end
  

  def all_jsons 
    respond_to do |format|
      format.json do
        p 'navya'
        @enquiries = Enquiry.includes(:patient).where.not(status: "EMPTY_RESULTS").count
        @missed_calls = MissedCallRequest.all.count
        @new_city = NewCityRequest.all.count
        @no_cg = Enquiry.includes(:patient).where(status: "EMPTY_RESULTS").count
        @appointments = Enquiry.where(:appointment_status => "Open").count
        @high = Enquiry.where(:current_tag => "Potential-High").count
        p @high
        @low =Enquiry.where(:current_tag => "Potential-Low").count
        @non_prospect = Enquiry.where(:current_tag => "Non-prospect").count
        @audit = Enquiry.where(:current_tag => "To Audit").count
        @closed = Enquiry.where(:current_tag => "Closed").count
        @enrolled = Enquiry.includes(:patient).enrolled.count
        @enrollments = Enrollment.where(:status =>'ACTIVE')
        @all = []
        @enrollments.each do |x|
          if PatientAdaptiveEquipment.where(:enrollment_id => x.id).exists? and PatientSelfcareAbility.where(:enrollment_id => x.id).exists? and PatientRoutine.where(:enrollment_id => x.id).exists? and DrugQuantityMaster.where(:enrollment_id => x.id).exists? and VitalTrend.where(:enrollment_id => x.id).exists? and CareToBeProvidedPlan.where(:enrollment_id => x.id).exists?
          else
            @all << x
          end
        end
        @incomplete_pcp = @all.count
        @enrollment_closed =  Enrollment.where(:status => 'CLOSED').count
        @job_applicant_list = CareGiverEnquiry.all.where(:status => 'Open').count
        @short_list = CareGiverEnquiry.where(:status => 'Recruit').count
        @not_approved = CareGiverEnquiry.where(:status => 'Documents Collected').count
        @hold_list = CareGiverEnquiry.where(:status => 'Hold').count
        @cg_new_city = CgNewCityRequest.all.count
        @cg_closed = CareGiverEnquiry.where(:status => 'Rejected').count
        @patients_new_tickets = Ticket.where(:role => 'Patient',:current_tag => 'New').count
        @patients_open_tickets =  Ticket.where(:role => 'Patient',:current_tag => 'Open').count
        @patients_waiting = Ticket.where(:role => 'Patient',:current_tag => 'Waiting for Inputs').count
        @patients_waiting_for_closed = Ticket.where(:role => 'Patient',:current_tag => 'Waiting for Closed').count
        @ticket_closed = Ticket.where(:role => 'Patient',:current_tag => 'Closed').count
        @cg_new_tickets = Ticket.where(:role => 'Caregiver',:current_tag => 'New').count
        @cg_open_tickets =  Ticket.where(:role => 'Caregiver',:current_tag => 'Open').count
        @cg_waiting = Ticket.where(:role => 'Caregiver',:current_tag => 'Waiting for Inputs').count
        @cg_waiting_for_closed = Ticket.where(:role => 'Caregiver',:current_tag => 'Waiting for Closed').count
        @cg_ticket_closed = Ticket.where(:role => 'Caregiver',:current_tag => 'Closed').count
        p 'navya'
        render :json => {:enquiries => @enquiries , :missed_calls => @missed_calls , :new_city => @new_city , :no_cg => @no_cg , :appointments => @appointments , :high =>  @high , :low => @low , :audit => @audit , :closed => @closed , :job_applicant_list => @job_applicant_list , :short_list => @short_list , :not_approved => @not_approved , :hold_list => @hold_list , :cg_new_city => @cg_new_city , :cg_closed => @cg_closed , :enrolled => @enrolled , :incomplete_pcp => @incomplete_pcp , :enrollment_closed => @enrollment_closed , :patients_new_tickets => @patients_new_tickets , :patients_open_tickets => @patients_open_tickets , :patients_waiting => @patients_waiting , :patients_waiting_for_closed => @patients_waiting_for_closed , :ticket_closed => @ticket_closed , :cg_new_tickets => @cg_new_tickets , :cg_open_tickets => @cg_open_tickets , :cg_waiting => @cg_waiting , :cg_waiting_for_closed => @cg_waiting_for_closed , :cg_ticket_closed => @cg_ticket_closed }
      end
    end
  end
  
end
