class CareGiverAdditionalDetailsController < ApplicationController
  before_action :load_care_giver_master
  load_resource :except => [:physical_description]
  before_action :authenticate_user!

  def physical_description
    @care_giver_additional_detail = @care_giver_master.care_giver_additional_detail.present? ? @care_giver_master.care_giver_additional_detail : CareGiverAdditionalDetail.new
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {care_giver_details: @care_giver_additional_detail.slice(:id,:height_in_cms, :hair, :eyes_color, :complexion, :identification_tatoo, :any_deoformity, :physical_description_approved)}
      end
    end
  end

  def health_and_lefe_style
    @care_giver_additional_detail = @care_giver_master.care_giver_additional_detail
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {care_giver_details: @care_giver_additional_detail.slice(:id,:blood_group, :known_allergy, :current_health_problem, :past_health_problem, :smoking_habit, :chewing_tobacco, :consume_liquor, :convicted_any_court_of_law, :civil_or_criminal_proceeding_in_court_of_law, :health_approved)}
      end
    end
  end

  def show
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {care_giver_details: @care_giver_additional_details}
      end
    end
  end


  def save_additional_details
    @care_giver_additional_details = params[:care_giver_additional_detail][:id].present? ? CareGiverAdditionalDetail.find(params[:care_giver_additional_detail][:id]) : CareGiverAdditionalDetail.new
    @care_giver_additional_details.assign_attributes(additional_detail_params)
    @care_giver_additional_details.care_giver_master = @care_giver_master
    status = @care_giver_additional_details.save
    @care_giver_master.check_children_approvals_and_update_self_approval
    respond_to do |format|
      format.html do
      end
      format.json do
        
        render :json => {:status => status}
      end
    end
  end


  def update_physical_description
    
    @care_giver_additional_details.assign_attributes(physical_description_params)
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:status => @care_giver_additional_details.save}
      end
    end
  end

  def update_health_and_life_cycle
    @care_giver_additional_details.assign_attributes(health_and_life_cycle_params.merge(:approved => false, :approved_by => current_user.id))
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:status => @care_giver_additional_details.save}
      end
    end
  end


  def update_undertakings
    @care_giver_additional_details.assign_attributes(undertaking_params)
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:status => @care_giver_additional_details.save}
      end
    end
  end

  def approve_physical_description
    respond_to do |format|
      format.html do
      end
      format.json do
        status = @care_giver_additional_detail.update({:physical_description_approved => true, :physical_description_approved_by => current_user.id})
        @care_giver_master.check_children_approvals_and_update_self_approval
        render :json => {:status => status}
      end
    end
  end
  
  def approve_helth_and_life
    respond_to do |format|
      format.html do
      end
      format.json do
        status = @care_giver_additional_detail.update({:health_approved => true, :health_approved_by => current_user.id})
        @care_giver_master.check_children_approvals_and_update_self_approval
        render :json => {:status => status}
      end
    end
  end
  
  private
  
  def load_care_giver_master
    @care_giver_master = CareGiverMaster.find(params[:care_giver_master_id])
  end

  def additional_detail_params
    params.require(:care_giver_additional_detail).permit(:height_in_cms, :hair, :eyes_color, :complexion, :identification_tatoo, :any_deoformity, :blood_group, :known_allergy, :current_health_problem, :past_health_problem, :smoking_habit, :chewing_tobacco, :consume_liquor, :convicted_any_court_of_law, :civil_or_criminal_proceeding_in_court_of_law, :physical_description_approved, :health_approved)
  end
  
end
