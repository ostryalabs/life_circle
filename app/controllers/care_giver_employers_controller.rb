class CareGiverEmployersController < ApplicationController
  before_action :load_care_giver_master
  load_resource :only => [:approve]
  before_action :authenticate_user!

  def employer_for_registration
    @care_giver_employers = @care_giver_master.care_giver_employers
    respond_to do |format|
      format.html{}
      format.json do
        if @care_giver_employers.empty?
          @care_giver_employers = 2.times.map{|i| CareGiverEmployer.new{|cgr| cgr.care_giver_master_id = @care_giver_master.id}}
        end
        render :json => {:employers => @care_giver_employers, :reference_1 => @care_giver_master.reference_1,:reference_2 => @care_giver_master.reference_2}
      end
    end
  end

  def create
    respond_to do |format|
      format.html{}
      format.json do
        @care_giver_employer = params[:id].present? ? CareGiverEmployer.find(params[:id]) : CareGiverEmployer.new
        @care_giver_employer.assign_attributes(care_giver_employer_params(params))
        @care_giver_employer.care_giver_master = @care_giver_master
        status = @care_giver_employer.save
        if status
          status = @care_giver_master.update_attributes(care_giver_reference_params)
        end
        render :json => {:status => status}
      end
    end
  end


  def save_fresher
    respond_to do |format|
      format.json do
        care_giver_employer = CareGiverEmployer.new()
        care_giver_employer.fresher  = params[:fresher]
        care_giver_employer.approved = false
        care_giver_employer.care_giver_master_id = params[:care_giver_master_id]
        render :json => {:status => care_giver_employer.save}
      end
    end
    
  end

  def save_employer
    respond_to do |format|
      @care_giver_employers = []
      params[:care_giver_employers].each do |local_params|
        if local_params[:id].present?
          care_giver_employer = CareGiverEmployer.find(local_params[:id])
          care_giver_employer.assign_attributes(care_giver_employer_params(local_params).merge(:approved => false, :approved_by => current_user.id ))
          care_giver_employer.care_giver_master =  @care_giver_master
          @care_giver_employers.push(care_giver_employer)
        else
          care_giver_employer = CareGiverEmployer.new(care_giver_employer_params(local_params))
          care_giver_employer.care_giver_master =  @care_giver_master
          @care_giver_employers.push(care_giver_employer)
        end
      end
      format.html{}
      format.json do
        if @care_giver_employers.map(&:valid?).all?
          status = @care_giver_employers.map(&:save).all?
          if status
            status = @care_giver_master.update_attributes(care_giver_reference_params)
          end
        else
          status = false
        end
          render :json => {:status => status}
      end
    end
  end
  
  def approve
    respond_to do |format|
      format.html do
      end
      format.json do
        status = @care_giver_employer.update({:approved => true, :approved_by => current_user.id})
        render :json => {:status => status}
      end
    end
  end
  

  private

  def care_giver_reference_params
    {reference_1: params[:reference_1], reference_2: params[:reference_2]}
  end
  
  def care_giver_employer_params(local_params)
    local_params.permit(:employer, :period_from, :period_to, :attachment, :nature_of_work, :contact_no , :fresher)
  end
  
  def load_care_giver_master
    @care_giver_master = CareGiverMaster.find(params[:care_giver_master_id])
  end
  
end
