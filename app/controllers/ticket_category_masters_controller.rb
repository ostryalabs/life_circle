class TicketCategoryMastersController < ApplicationController
  load_resource
  
  def index
    @tkt_category = TicketCategoryMaster.all.order("id")
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @tkt_category
      end
    end
  end
  def create
  p "============"
    @ticket_category_master = TicketCategoryMaster.new(ticket_category_master_params)
    #@ticket_category_master.created_by = current_user.id
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @ticket_category_master.save}
      end
    end

  end

  def all_categories
    respond_to do |format|
      format.html do
      end
      format.json do
        @patient_category = TicketCategoryMaster.where(:category_type => "Patient")
        @cg_category = TicketCategoryMaster.where(:category_type => "Caregiver")
        render :json => {:patient => @patient_category , :care_giver => @cg_category}
      end
    end
  end
  
  def update
    @ticket_category_master = TicketCategoryMaster.find(params[:id])
    respond_to do |format|
      format.json do
        render json: {status: @ticket_category_master.update(ticket_category_master_params)}
      end
    end
  end

  def destroy
    @ticket_category_master = TicketCategoryMaster.find(params[:id])
    respond_to do |format|
      format.json do
        render json: {status: @ticket_category_master.destroy}
      end
    end
  end

  private
  
  def ticket_category_master_params
    params.require(:ticket_category_master).permit(:name, :description, :category_type)
  end
end
