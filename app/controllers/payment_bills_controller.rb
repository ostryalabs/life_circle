class PaymentBillsController < ApplicationController
  before_action :load_enrollment
  before_action :load_payment_master, :except => [:enrollment_bills]
  before_action :set_payment_bill, only: [:show, :edit, :update, :destroy]


  def enrollment_bills
    @payment_bills = @enrollment.payment_masters.map(&:payment_bills).flatten
  end
  
  # GET /payment_bills
  # GET /payment_bills.json
  def index
    @payment_bills = @payment_master.payment_bills.all
  end

  # GET /payment_bills/1
  # GET /payment_bills/1.json
  def show
  end

  # GET /payment_bills/new
  def new
    @payment_bill = PaymentBill.new
    @payment_bill.payment_master_id = @payment_master.id
    if(@payment_master.payment_bills.present?)
      @payment_bill.total_amount_to_pay = @payment_master.total_pending_payment
    else
      @payment_bill.total_amount_to_pay = @payment_master.total_amount
    end
    @payment_bill.payment_date = Date.today
  end

  # GET /payment_bills/1/edit
  def edit
  end

  # POST /payment_bills
  # POST /payment_bills.json
  def create
    @payment_bill = PaymentBill.new(payment_bill_params)
    respond_to do |format|
      if @payment_bill.bill_pre_process_and_save
        format.html { redirect_to enrollment_payment_master_path(@enrollment, @payment_master), notice: 'Payment bill was successfully created.' }
        format.json { render :show, status: :created, location: @payment_bill }
      else
        format.html { render :new }
        format.json { render json: @payment_bill.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /payment_bills/1
  # PATCH/PUT /payment_bills/1.json
  def update
    respond_to do |format|
      if @payment_bill.update(payment_bill_params)
        format.html { redirect_to @payment_bill, notice: 'Payment bill was successfully updated.' }
        format.json { render :show, status: :ok, location: @payment_bill }
      else
        format.html { render :edit }
        format.json { render json: @payment_bill.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payment_bills/1
  # DELETE /payment_bills/1.json
  def destroy
    @payment_bill.destroy
    respond_to do |format|
      format.html { redirect_to payment_bills_url, notice: 'Payment bill was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment_bill
      @payment_bill = PaymentBill.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payment_bill_params
      params.require(:payment_bill).permit(:payment_master_id, :amount_paid, :payment_mode, :description_1, :description_2, :payment_done_by, :payment_date, :total_amount_to_pay)
    end
    
    def load_payment_master
      @payment_master = PaymentMaster.find(params[:payment_master_id])
    end

    def load_enrollment
      @enrollment = Enrollment.find(params[:enrollment_id])
    end

end
