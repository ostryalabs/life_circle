class Address < ActiveRecord::Base
  #validates :line1, :presence => true
  
  geocoded_by :full_address   # can also be an IP address
  after_validation :geocode          # auto-fetch coordinates
  belongs_to :city_master
  
  attr_accessor :area, :city
  
  def full
    address =[]
    address << self.line1
    return address[0]
  end
  
  
  def address_list
    address =[]
    address << self.line1
    address << self.line2
    address << self.try(:city_master).try(:name)
    address << self.state
    address << self.country
    address << self.pin_code
  end

  def address_json
    str = self.line1.to_s
    str << ','
    str << self.line2.to_s 
    str << ','
    str << self.city.to_s 
    str << ','
    str << self.state.to_s 
    str << ','
    str << self.country.to_s 
    str << ','
    str << self.pin_code.to_s
    return str
  end
  
 

  
  def full_address
    address_list.join(", ")
  end

  def full_address_html
    address_list.join("<br/>")
  end



  
end
