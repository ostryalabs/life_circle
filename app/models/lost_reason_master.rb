class LostReasonMaster < ActiveRecord::Base
  validates :reason, :presence => true
end
