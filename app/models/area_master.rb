class AreaMaster < ActiveRecord::Base
  validates :name, :presence => true
  geocoded_by :name
  after_validation :geocode 
  
  belongs_to :city_master
end
