class EnquiryHistory < ActiveRecord::Base

  scope :having_migration_date_on_or_after, lambda{|date| where("migration_date >= ?", date)}
  has_many :enquiry_care_giver_histories
  has_many :requested_time_slots, :class_name => "RequestedTimeSlotHistory"
  
  def total_charge
    enquiry_care_giver_histories.inject(0){|sum, cg| sum+ (cg.caregiver_charge.to_i+cg.transport_charge.to_i)}
  end

end
