class ApplicationDefault < ActiveRecord::Base

  after_save :reload_enrollment_fee, :if => Proc.new { |default| default.description.present? and  (default.description.downcase == "enrollment fee" or default.description.downcase == "enrollment_fee")}
  
  def reload_enrollment_fee
    self.class.load_enrollmet_fee
  end

  def self.load_enrollmet_fee
    @@enrollment_fee = self.where("lower(description) = 'enrollment_fee' OR lower(description) = 'enrollment fee'").first.value
  end

  def self.enrollment_fee
    @@enrollment_fee ||= load_enrollmet_fee
  end

  def self.default_cc_email
    self.where(:description => "default_cc_email").first.value
  end

  def self.invoice_due_date(date)
    invoice_due_row = self.where(:description => "invoice_due_date").first 
    (invoice_due_row.present? ? (date + invoice_due_row.value.to_i) : date)
  end

  def self.cg_price_ranges
    lower_limit = self.where(:description => "Budget_Lower_Limit").first.try(:value).try(:to_i)
    higher_limit = self.where(:description => "Budget_Upper_Limit").first.try(:value).try(:to_i)
    ranges = []
    if lower_limit.present? and higher_limit.present?  and (lower_limit < higher_limit)
      while( lower_limit <= higher_limit) do
        ranges.push({operator: "<", l_value: lower_limit, key: "<#{lower_limit}"})
        lower_limit += 100
      end
    end
    ranges
  end

end
