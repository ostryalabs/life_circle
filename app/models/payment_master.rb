class PaymentMaster < ActiveRecord::Base
  default_scope { where("nullified = false OR nullified = null") } 
  validates :invoice_no, :presence => true
  belongs_to :enrollment
  belongs_to :job_run
  has_many :credit_notes
  has_many :payment_bills
  

  PREV_MONTH_DEBIT_COMMENTS_MSG = "Amount exceded as caregiver replaced higher skill caregiver on client request "
  PREV_MONTH_AMOUNT_SETTLE_COMMENTS = "Amount Settlement "

  PAYMENT_MODES = [["Cash", "CASH"], ["Cheque", "CHEQUE"], ["OnlineTransfer", "ONLINE_TRANSFER"], ["Online Payment", "ONLINE_PAYMENT"]]
  DISCOUNT_TYPES = [["Absolute", "ABSOLUTE"], ["Percentage", "PERCENTAGE"]]
  attr_accessor :caregiver_charge, :enrollment_fee, :no_of_days, :discount_every_time, :prev_payment_credit_notes, :discount_every_time
  
  scope :not_failed, lambda{ where("status != 'FAILED'")}
  scope :in_the_month_of, lambda{|date| where("? BETWEEN period_from AND period_to", date)}
  scope :having_enrollment, lambda{|id| where(:enrollment_id => id)}
  scope :latest_payment, lambda{ order("period_to DESC").first}
  scope :generated_on, lambda{|date| where(:invoice_generation_date => date)}
  scope :mail_not_sent, lambda{ where("mail_sent = false")}

  def patient_name
    enrollment.patient.name
  end

  def patient_address
    enrollment.patient_with_address_html
  end
  
  def completed?
    status == "COMPLETED"
  end

  def no_of_days
    return @no_of_days if @no_of_days.present?
    if period_from.present? and period_to.present?
      (period_from..period_to).count
    end
  end

  def credit_note_amount
    self.credit_notes.issued.inject(0){ |sum, credit_note| sum+credit_note.value_of_credit_note }
  end

  def enrollment_fee
    if self.enrollment.payment_masters.first.try(:id).to_i == self.id
      enrollment.enrollment_fee
    else
      0
    end
  end
  
  def total_amount
     (amount_in_rupees.to_i + enrollment_fee.to_i  - payment_discount.to_i - credit_note_amount.to_i)
  end

  alias_method :total_amount_in_rupees, :total_amount
 
  def payment_discount
    if discount_type == "PERCENTAGE"
      (amount_in_rupees * (discount.to_f/100)).round
    else
      discount.to_i
    end
  end

  def first_payment?
    enrollment.payment_masters.count == 1
  end

  def enquiry_care_givers
    enrollment.enquiry.enquiry_care_givers
  end

  def amount_after_discount
    (amount_in_rupees.to_i - discount_amount.to_i)
  end

  def discount_amount
    payment_discount
  end

  def discount_in_percentage
    if enrollment.discount_type == "PERCENTAGE"
      enrollment.discount
    else
      convert_absolute_discount_into_persentage
    end
  end

  def convert_absolute_discount_into_persentage
    ((100*enrollment.discount.to_f) / amount_in_rupees)
  end


  def mark_as_paid
    self.update_attributes({:status => "PAID"})
  end

  def mark_as_mail_sent
    self.update_attributes({:mail_sent => true})
  end

  def mark_as_nullified
    self.update_attributes({:nullified => true})
  end


  def mark_as_partly_paid
    self.update_attributes({:status => "PARTLY_PAID"})
  end

  def total_pending_payment
    (total_amount - total_amount_paid)
  end

  def total_amount_paid
    payment_bills.completed.inject(0){|sum, pb| sum+pb.amount_paid.to_i}
  end

  def credit_note_issue_reason
    if enrollment.closed? or enrollment.close_pending?
      "Services availing only for #{attendance_dates.count} days from #{attendance_dates.first.try(:login_time).try(:to_date)} to #{attendance_dates.last.try(:login_time).try(:to_date)} hence balance  days amount returning"
    else
      "CG went on leave for #{absent_dates.count} days on #{absent_dates.join(', ')}. Client no accepted for replacement."
    end
  end

  def notice_period_amount_adjusted?
    ((enrollment.closed? or enrollment.close_pending?) ? "YES" : "NO")
  end
  
  def daily_service_rate
    enrollment.enquiry.total_charge
  end

  def absent_dates
    @absent_date ||= (
                      attendace_dates = attendance_dates.map{ |d| d.login_time.to_date };
                      (period_from..period_to).select{|d| (not attendace_dates.include?d)}
                      )
  end
  
  def attendance_dates
    @attendance_dates ||= enrollment.care_giver_daily_attendances.exists_in_range(period_from, period_to)
  end

  def as_json(options = {})
    options[:methods] = (options[:methods].present? ? options[:methods] : [] )
    options[:methods] += [:no_of_days, :credit_note_amount, :total_amount, :enrollment_fee]
     options[:methods] += [:patient_name, :patient_address]
    options[:include] = [:credit_notes]
    super(options)
  end
  
end
