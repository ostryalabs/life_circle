class EnquiryReferenceMaster < ActiveRecord::Base
  validates :reference, :presence => true
end
