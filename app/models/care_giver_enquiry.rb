class CareGiverEnquiry < ActiveRecord::Base
  has_many :enquiry_conversations, as: :conversation_master
  has_one :care_giver_master
end
