class DailyCheck < ActiveRecord::Base
  belongs_to :patient
  belongs_to :care_giver_master
  
  def patient_name
   patient.first_name
  end
  
  def care_giver_master_name
   care_giver_master.try(:first_name)
  end
  
  def formated_entered_at
    entered_at.strftime("%d/%m/%Y %H:%M") 
  end
  
  def as_json(options={})
    options[:methods] = [:formated_entered_at, :patient_name, :care_giver_master_name]
    super(options)
  end
end
