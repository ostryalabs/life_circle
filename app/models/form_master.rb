class FormMaster < ActiveRecord::Base
  validates :name, :presence => true
  has_many :drug_masters
end
