class Guardian < ActiveRecord::Base
  has_one :patient
  belongs_to :address
  accepts_nested_attributes_for :address
  
  def name
    "#{first_name} #{last_name}"
  end

  def address_str
    if address_same_as_patient
      patient.address_str
    else
      address.try(:full_address)
    end
  end

  def as_json(options = {})
    super(options)
  end

  
end
