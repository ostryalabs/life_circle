class CareGiverEmployer < ActiveRecord::Base
  mount_uploader :attachment, DocumentUploader
  belongs_to :care_giver_master

end
