class RoleMenuMapping < ActiveRecord::Base
  belongs_to :menu_bar
  belongs_to :role
  
  attr_accessor :checked_value
  
  def name
    menu_bar.name
  end

  def parent_id
    menu_bar.parent_id
  end

  def as_json(options = {})
    super(:methods => [:checked_value ,:name , :parent_id ])
  end
    
end
