class DrugQuantityMaster < ActiveRecord::Base
  belongs_to :enrollment
  belongs_to :drug_master
  has_many :time_of_administrations

  def drug_name
    drug_master.name
  end
  
  def times_str
    time_of_administrations_list.join(", ") 
  end

  def time_of_administrations_list
    time_of_administrations.map{|s| s.time.strftime("%H:%M")}
  end
    
  def as_json(options = {})   
    super(:methods => [:drug_name, :times_str])   
  end
end
