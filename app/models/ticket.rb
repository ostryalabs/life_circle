class Ticket < ActiveRecord::Base
  belongs_to :ticket_category_master
  belongs_to :patient
  belongs_to :care_giver_master
  has_many :ticket_conversations
   
  def category_name
    ticket_category_master.name
  end
   
  def patient_name
    patient.first_name
  end
  
  def user_name
    if care_giver_master_id.present?
      care_giver_master.try(:first_name)
    else
      patient.first_name
    end
  end

  def care_giver_master_name
   care_giver_master.try(:first_name)
  end
   
   def as_json(options = {})   
    super(:methods => [:category_name])   
   end
end
