class PatientSelfcareAbilityMaster < ActiveRecord::Base
   validates :selfcare_ability, :presence => true
end
