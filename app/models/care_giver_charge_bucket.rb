class CareGiverChargeBucket < ActiveRecord::Base
  validates :charge_percentage, :presence => true
  validates :hrs_from, :presence => true

  scope :having_given_time_duration, lambda{|hrs| where("(CASE WHEN hrs_to != null THEN (#{hrs} >= hrs_from and  #{hrs} < hrs_to) ELSE (#{hrs} >= hrs_from) END)")}

  def self.charge_percentage_for_time(time_in_hours)
    self.having_given_time_duration(time_in_hours).last.try(:charge_percentage)
  end

  def self.calculated_charge_on_given_duration(work_hours, charge)
    return charge if work_hours.nil?
    charge_percentage = self.charge_percentage_for_time(work_hours).to_f
    ((charge_percentage/100) * charge).round
    
  end

end
