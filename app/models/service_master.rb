class ServiceMaster < ActiveRecord::Base
  validates :service, :presence => true
  has_many :sub_service_masters
  has_many :pcp_services
end
