class PatientSelfcareAbility < ActiveRecord::Base

  belongs_to :enrollment
  belongs_to :patient_selfcare_ability_master
  
   def ability_name
      patient_selfcare_ability_master.selfcare_ability
   end
  
  def as_json(options = {})   
      super(:methods => [:ability_name])   
  end

  
end
