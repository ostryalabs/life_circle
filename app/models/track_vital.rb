class TrackVital < ActiveRecord::Base
  belongs_to :enrollment
  belongs_to :vital_trend
  
  def vital_name
    vital_trend.vital_name
  end
  
  def vital_unit
      vital_trend.vital_unit
  end
  
  def upper_limit
    vital_trend.upper_limit
  end
  
  def lower_limit
      vital_trend.lower_limit
  end
  def as_json(options = {})   
      super(:methods => [:vital_name, :vital_unit, :upper_limit, :lower_limit])   
  end
  
end
