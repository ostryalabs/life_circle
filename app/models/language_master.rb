class LanguageMaster < ActiveRecord::Base
  has_many :care_giver_languages, :dependent => :destroy
end
