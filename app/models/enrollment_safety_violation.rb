class EnrollmentSafetyViolation < ActiveRecord::Base
  validates :comments, :presence => true
  belongs_to :enrollment

end
