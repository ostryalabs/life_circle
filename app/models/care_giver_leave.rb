class CareGiverLeave < ActiveRecord::Base
  validates :from_date, :presence => true
  validates :to_date, :presence => true
  validates :description, :presence => true
  belongs_to :care_giver_master

  scope :leave_on_date, lambda{|date| where("from_date <= ? and to_date >= ?", date, date)}
  scope :approved, lambda{where(:status => "APPROVED")}

  def care_giver_name
    care_giver_master.name
  end

  def care_giver_img_url
    care_giver_master.profile_pic.try(:thumb).try(:url)
  end
  
  def care_giver_enquiry_id
    care_giver_master.care_giver_enquiry_id
  end
  
  def care_giver_status
    care_giver_master.status
  end
  
  def approve
    self.update(:status => "APPROVED")
  end

  def reject
    self.update(:status => "REJECTED")
  end

  def as_json(options = {})
    super(options)
  end
  
end
