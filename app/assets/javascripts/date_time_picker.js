$(document).ready(function(){
    $('.dobdatepicker').datetimepicker({

	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
        maxDate: new Date()
	// minDate:'-1970/01/02', // yesterday is minimum date
	// maxDate:'+1970/01/02' // and tommorow is maximum date calendar
    });
    
    $('.datepicker').datetimepicker({

	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d'
	// minDate:'-1970/01/02', // yesterday is minimum date
	// maxDate:'+1970/01/02' // and tommorow is maximum date calendar
    });

    $('.datepickerNoPast').datetimepicker({
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	minDate:'-1970/01/02' // yesterday is minimum date
	// maxDate:'+1970/01/02' // and tommorow is maximum date calendar
    });

    $('.timepicker').datetimepicker({
        datepicker:false,
        format:'H:i',
        step: 30

    });

    $("input[rel='dynamicTimePicker']").on("focus", function(){
        $(this).datetimepicker({
            datepicker:false,
            format:'H:i',
            step: 30
        });
    })
});

var getTimepicker= function(obj){
    $(obj).datetimepicker({
        datepicker:false,
        format:'H:i',
        step: 30,
        onSelect: function (timeText, inst) {
            $(obj).val(timeText)
        }
    });
}

$(function() {
    $( "#dobdatepicker" ).datepicker({  maxDate: new Date });
});
var appointmentTimepicker= function(obj){
    $(obj).datetimepicker({
        datepicker:false,
        format:'H:i',
        step: 30,
        minTime: '08:00',
        maxTime: '20:00',
        onSelect: function (timeText, inst) {
            $(obj).val(timeText)
        }
    });
}
