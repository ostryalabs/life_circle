(function(angular, app) {
    "use strict";
    app.directive("myDatetimepicker", ['$timeout', function($timeout){
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function(scope, elem, attrs, ngModel){
                // timeout internals are called once directive rendering is complete
                $timeout(function(){                    
                    $(elem).datetimepicker({
                        timepicker:false,
                        format:'d/m/Y',
                        maxDate:'+1970/01/02',
                        onSelect: function (dateText, inst) {
                            ngModel.$setViewValue(dateText);
                            ngModel.$render();
                        }
                    });
                });
            }
        };
    }]);
})(angular, lifeCircleApp);
