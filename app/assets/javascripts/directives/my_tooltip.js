(function(angular, app) {
    "use strict";
    app.directive('myTooltip', function () {
        return {
            restrict: 'A',
            replace: false,
            scope: {
                tooltipPlacement: '=?',
                tooltip: '='
            },
            compile: function compile( tElement, tAttributes ) {
                return function postLink( scope, element, attributes, controller ) {
                    if (scope.tooltip !== '') {
                        element.attr('data-toggle', 'tooltip');
                        element.attr('data-placement', scope.tooltipPlacement || 'top');
                        element.attr('title', scope.tooltip);
                        element.tooltip();  
                    }
                    
                    scope.$watch('tooltip', function(newVal) {
                        if (!element.attr('data-toggle')) {
                            element.attr('data-toggle', 'tooltip');
                            element.attr('data-placement', scope.tooltipPlacement || 'top');
                            element.attr('title', scope.tooltip);
                            element.tooltip();  
                        }
                        element.attr('title', newVal);
                        element.attr('data-original-title', newVal);
                    });
                };
            }
        };
    });
})(angular, lifeCircleApp);
