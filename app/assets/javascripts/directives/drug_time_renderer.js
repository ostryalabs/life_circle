(function(angular, app) {
    "use strict";
    app.directive("drugTimeRenderer", function() {
	return {
	    restrict: 'E',
	    scope: {                          
                slots: '=',
	    },
	    controller: ["$scope", function($scope){
                if(typeof $scope.slots != 'undefined' && $scope.slots.length > 0){
                  //alert("not defined")
                }else{
                    //alert("defined")
                    $scope.slots = [{time1: "", time2: "", time3: ""}]
                    //alert(JSON.stringify($scope.slots))
                }
                $scope.addMoreSlot = function(){
                    //alert("addmore")
                    var newSlot = {}
                    angular.forEach($scope.slots[0], function(val, key){
                        newSlot[key] = ""
                        //alert(key)
                    });
                    $scope.slots.push(newSlot)
                    //alert(JSON.stringify($scope.slots))

                }

                $scope.removeTimeSlot = function(index){
                    if(index > 0){
                        $scope.slots.splice(index);
                    }
                }              
	    }],
	    template: "<div class='col-md-12' ng_repeat='slot in slots'><section class='col col-5'>"+
                "<label class='input'><input type='text' placeholder='fromTime' ng-model='slot.time1' onFocus = 'getTimepicker(this)'/></label></section>"+
                "<section class='col col-5'><label class='input'><input type='text' placeholder='toTime' ng-model='slot.time2' onFocus = 'getTimepicker(this)'/></label>"+
                "<label class='input'><input type='text' placeholder='toTime' ng-model='slot.time3' onFocus = 'getTimepicker(this)'/></label>"+
                "</section><section class='col col1' ng-show='$index > 0'><a href='#' title='close' ng-click ='removeTimeSlot($index)'><i class='fa fa-close fa-lg'></i></a></section></div>"+
                "<div class='col-md-4'><a href='#' ng-click='addMoreSlot()' class='button'>add More</a></div>"
	}
    });
})(angular, lifeCircleApp);
