var placeSearch, autocomplete;
var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
};


function care_giver_initialize() {
    
    console.log("initialize.....")
    // Create the autocomplete object, restricting the search
    // to geographical location types.
    //var boundingBox = [15.922682168891521, 76.96352606706787, 18.81731783110848, 79.99647393293213]
    var southWest = new google.maps.LatLng(17.22526821688915, 78.32835260670679);
    var northEast = new google.maps.LatLng(17.51473178311085, 78.63164739329322);
    var hyderabadBounds = new google.maps.LatLngBounds( southWest, northEast );
    var options = {
        bounds: hyderabadBounds,
        types: ['geocode'],
        componentRestrictions: {
            country: 'IN'
        }
        
    }
    autocomplete = new google.maps.places.Autocomplete((document.getElementById('autocomplete')), options);
   
    // When the user selects an address from the dropdown,
    // populate the address fields in the form.
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        fillInAddress();
    });
}

// [START region_fillform]
function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();
    // for (var component in componentForm) {
    //     document.getElementById(component).value = '';
    //     document.getElementById(component).disabled = false;
    // }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    
    var placeDetails = {}
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        placeDetails[addressType] = place.address_components[i].long_name
        console.log(addressType + "" + place.address_components[i].long_name)
        if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            //document.getElementById(addressType).value = val;
        }
    }
    $("#place_details_object").val(JSON.stringify(placeDetails))
}

// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    // if (navigator.geolocation) {
    //     navigator.geolocation.getCurrentPosition(function(position) {
    //         var geolocation = new google.maps.LatLng(
    //             position.coords.latitude, position.coords.longitude);
    //         var circle = new google.maps.Circle({
    //             center: geolocation,
    //             radius: position.coords.accuracy
    //         });
    //         autocomplete.setBounds(circle.getBounds());
    //     });
    // }
}

