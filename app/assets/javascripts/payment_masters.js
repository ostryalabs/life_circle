$(document).ready(function(){
    $("#invoice_to_date").on("blur", function(){
        var from_date = $("#invoice_from_date").val()
        var to_date = $("#invoice_to_date").val()
        if(typeof from_date != 'undefined' && typeof to_date != 'undefined'){
            from  = from_date.split("/");
            f = new Date(from[2], from[1] - 1, from[0]);
            to  = to_date.split("/");
            t = new Date(to[2], to[1] - 1, to[0]);
            var one_day=1000*60*60*24;
            difference_ms = Math.round((t-f)/one_day);
            $("#invoice_no_of_days").val(difference_ms)
        }
    });
    
    
});

function reflectPeriodToChange(index){
    $('#recalculateButton'+index).show(); 
    $('#sendInvoiceButton'+index).hide();
}
