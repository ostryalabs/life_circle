$(document).ready(function() {
    $("#registration_type").change(function(){
	if($(this).val() === 'CALL_ME'){
	    $("#refered_mobile_no").show()
	}else{
	    $("#refered_mobile_no").hide()
	}
    });
});

function init_close_enquiry(enquiryId, patient_name){
    $("#closing_enquiry_id").val(enquiryId);
    $("#closing_comments").val("");
    $("#close-enquiry-pop-up").modal("show")
}


function closeEnquiry(){
    var closeEnquiryId = $("#closing_enquiry_id").val()
    var closingComments = $("#closing_comments").val()
    var url = "/enquiries/"+closeEnquiryId+"/close.json"
    
    $.ajax({
        method: "PUT",
        data: {"closing_comments": closingComments},
        url: url,
        success: function( data ) {
            location.reload();
        },
        error: function(ermsg){
            console.log(ermsg)
        }
    });
}

