(function(angular, app) {
    "usestrict";
    app.controller('VitalTrendsController',["$scope", "resources", "$window",function($scope, resources, $window) {
        $scope.success = false
        $scope.warning = false
        $scope.alertMessage = ""
        $scope.toogel = false
        $scope.loadVitalTrends = function(enrollment_id){
            //alert("load")
            $scope.enrollment_id = enrollment_id
            $scope.key_vitals = resources.KeyvitalsMaster.query();            
            $scope.vitaltrends = resources.Vitaltrend.query({enrollment_id:enrollment_id});
            //console.log($scope.vitaltrends.vitals)
            //$scope.vitaltrends.$promise.then(function(data) {
                //$scope.vitaltrends = data.vitals;
                //console.log(data.vitals)
            //});
        }

        $scope.popLimits =  function(id){
            for(i = 0; i < $scope.key_vitals.length; i++ )
            {
                if($scope.key_vitals[i].id == id)
                {
                    
                    $scope.vitaltrend.lower_limit = $scope.key_vitals[i].lower_limit
                    $scope.vitaltrend.upper_limit = $scope.key_vitals[i].upper_limit
                    //alert($scope.key_vitals[i].lower_limit)
                }
            }
        }
        $scope.toogle = function(){
          $scope.toogel = ($scope.toogel == false) ? true : false
        }
        $scope.newVitalTrend = function(){
            //alert("new")       
            $scope.vitaltrend = new resources.Vitaltrend({enrollment_id: $scope.enrollment_id})
            $scope.vitaltrend.isNew = true 
        }
        
        $scope.close = function(){
             $scope.vitaltrend.isNew = false;
        }

        $scope.closeupdate = function(vitaltrend){
            vitaltrend.edit = false;
        }
        
        $scope.edit = function(vitaltrend){
            vitaltrend.edit = true;
        }

        $scope.update = function(vitaltrend){
            vitaltrend.$update()
                .then(function(value) {
                    vitaltrend.edit = false;
                    $scope.success = true
		    $scope.alertMessage = "Update Successfully!"
                    $scope.loadVitalTrends($scope.enrollment_id);
                }, function(reason) {
                   $scope.warning = true
		   $scope.alertMessage = "Error!"
                    // handle failure
                });
        }

        $scope.save = function(){
            //alert($scope.vitaltrend)
            $scope.vitaltrend.$save()
                .then(function(value) {
                    $scope.success = true
		    $scope.alertMessage = "Saved Successfully!"
                    $scope.loadVitalTrends($scope.enrollment_id);
                }, function(reason) {
                    // handle failure
                   $scope.warning = true
		   $scope.alertMessage = "Error!"
                });
        }
        
        $scope.destroy = function(vitaltrend){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                vitaltrend.$delete()
                    .then(function(responce){
                        $scope.vitaltrends.splice($scope.vitaltrends.indexOf(vitaltrend), 1)
                        $scope.success = true
		        $scope.alertMessage = "Delete Successfull!"
                    }, function(reason) {
                        $scope.warning = true
		        $scope.alertMessage = "Error!"
                    // handle failure
                });
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
