(function(angular, app) {
    "use strict";
    app.controller('EnquiryInitController',["$scope", "$window", "localStorageService" ,  function($scope, $window, localStorageService ) {

        $scope.goToEnquiry = function(){
            localStorageService.clearAll()
            $window.location.href = "/enquiry"
        }
	$scope.goTo = function(){
            localStorageService.clearAll()
            $window.location.href = "/service_request"
            //$state.go("service_request")
	}
    }]);  
})(angular, lifeCircleApp);

