(function(angular, app) {
    "use strict";
    app.controller('PatientActivityController',["$scope", "resources", "lifeCircleService", "$window",function($scope, resources, lifeCircleService, $window) {
        $scope.success = false
        $scope.warning = false
        $scope.alertMessage = ""
        $scope.loadPatientRoutines = function(enrollment_id){
            $scope.enrollment_id = enrollment_id
            $scope.routine_masters = resources.Routine.query();
            lifeCircleService.getPatientRoutines($scope.enrollment_id)
                .then(function(value) {
                    $scope.patient_routines = value.data[0]
                    //console.log($scope.patient_routines)
                });
            //console.log($scope.routine_masters)
            // $scope.patient_routines = resources.PatientRoutine.query({enrollment_id:enrollment_id});
            // $scope.patient_routines.$promise.then(function(data) {
            //     $scope.patient_routines = data[0];
            //     console.log(data[0])
            // });
        }

        $scope.newPatientRoutine = function(){               
           $scope.patient_routine = new resources.PatientRoutine({enrollment_id: $scope.enrollment_id})
            $scope.patient_routine.isNew = true 
            //alert($scope.enrollment_id)
            
        }
        
        $scope.close = function(){
            $scope.patient_routine.isNew = false;
        }
        
        $scope.closeupdate = function(patient_routine){
            patient_routine.edit = false;
        }
       
        $scope.edit = function(patient_routine){
            patient_routine.edit = true;
        }

        $scope.update = function(patient_routine){
            lifeCircleService.updatePatientRoutine($scope.enrollment_id, patient_routine)
                .then(function(value) {
                    patient_routine.edit = false;
                    $scope.success = true
		    $scope.alertMessage = "Update Successfully"
                    $scope.loadPatientRoutines($scope.enrollment_id);
                }, function(reason) {
                    // handle failure
                    $scope.warning = true
		   $scope.alertMessage = "Error!"
                });
        }

        $scope.save = function(){
            $scope.patient_routine.$save()
                .then(function(value) {
                    $scope.success = true
		    $scope.alertMessage = "Saved Successfully"
                    $scope.loadPatientRoutines($scope.enrollment_id);  
                    //alert($scope.enrollment_id)             
                }, function(reason) {
                    // handle failure
                    $scope.warning = true
		   $scope.alertMessage = "Error!"
                });
        }
        
        $scope.destroy = function(patient_routine){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                lifeCircleService.destroyPatientRoutine($scope.enrollment_id, patient_routine)
                    .then(function(responce){
                        $scope.patient_routines.splice($scope.patient_routines.indexOf(patient_routine), 1)
                        $scope.success = true
		        $scope.alertMessage = "Delete Successfull"
                    }, function(reason) {
                        $scope.warning = true
		        $scope.alertMessage = "Error!"
                    // handle failure
                });
            };
        }
      
        
    }]);  
    
})(angular, lifeCircleApp);
