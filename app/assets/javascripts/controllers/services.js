(function(angular, app) {
    "use strict";
    app.controller('ServiceController',["$scope", "resources", "$window",function($scope, resources, $window) {
        
        $scope.loadServices = function(){
            $scope.services = resources.Service.query();
        }
        
        $scope.newService = function(){
            $scope.service = new resources.Service({service: null})
            $scope.service.isNew = true
        }

        $scope.edit = function(service){
            service.edit = true;
        }

        $scope.update = function(service){
            service.$update()
                .then(function(value) {
                    service.edit = false;
                    $scope.loadServices();
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.service.$save()
                .then(function(value) {
                    $scope.loadServices();
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(service){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                service.$delete()
                    .then(function(responce){
                        $scope.services.splice($scope.services.indexOf(service), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);


