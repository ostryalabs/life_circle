(function(angular, app) {
    "usestrict";
    app.controller('TrackvitalsController',["$scope", "$filter", "resources", "$window",function($scope, $filter,  resources, $window) {
        $scope.success = false
        $scope.warning = false
        $scope.alertMessage = ""
        $scope.show = false
        $scope.loadTrackvitals = function(enrollment_id){
            //alert("load")
            $scope.enrollment_id = enrollment_id
            $scope.vital_trends = resources.Vitaltrend.query({enrollment_id:enrollment_id});            
            $scope.trackvitals = resources.Trackvital.query({enrollment_id:enrollment_id});
            //console.log($scope.trackvitals.vitals)
            //$scope.trackvitals.$promise.then(function(data) {
                //$scope.trackvitals = data.vitals;
                //console.log(data.vitals)
            //});
        }

        $scope.popLimits =  function(id){
            for(i = 0; i < $scope.vital_trends.length; i++ )
            {
                if($scope.vital_trends[i].id == id)
                {
                    if($scope.vital_trends[i].vital_name == 'BP')
                    $scope.show = true
                    else
                    $scope.show = false
                }
            }
        }
        
        $scope.newTrackvital = function(){
            //alert("new")       
            $scope.trackvital = new resources.Trackvital({enrollment_id: $scope.enrollment_id})
            $scope.trackvital.isNew = true
            date = new Date();
            $scope.trackvital.entered_at = $filter('date')(date, "dd/MM/yyyy");
            //$scope.trackvital.entered_at = (new Date).toLocaleFormat("%m %e, %Y");
        }

        $scope.filtertype = function(){
            return function (item) {
                if (item.vital_name != 'BP')
                {
                    return true;
                }
                return false;
            };
        }
        
        $scope.close = function(){
             $scope.trackvital.isNew = false;
        }

        $scope.closeupdate = function(trackvital){
            trackvital.edit = false;
        }
        
        $scope.edit = function(trackvital){
            trackvital.edit = true;
        }

        $scope.update = function(trackvital){
            trackvital.$update()
                .then(function(value) {
                    trackvital.edit = false;
                    $scope.success = true
		    $scope.alertMessage = "Update Successfully!"
                    $scope.loadTrackvitals($scope.enrollment_id);
                }, function(reason) {
                    $scope.warning = true
		   $scope.alertMessage = "Error!"
                    // handle failure
                });
        }

        $scope.save = function(){
            //alert($scope.trackvital)
            $scope.trackvital.$save()
                .then(function(value) {
                    $scope.success = true
		    $scope.alertMessage = "Saved Successfully!"
                    $scope.loadTrackvitals($scope.enrollment_id);
                }, function(reason) {
                    // handle failure
                     $scope.warning = true
		   $scope.alertMessage = "Error!"
                });
        }
        
        $scope.destroy = function(trackvital){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                trackvital.$delete()
                    .then(function(responce){
                        $scope.trackvitals.splice($scope.trackvitals.indexOf(trackvital), 1)
                        $scope.success = true
		        $scope.alertMessage = "Delete Successfull!"
                    }, function(reason) {
                        $scope.warning = true
		        $scope.alertMessage = "Error!"
                    // handle failure
                });
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
