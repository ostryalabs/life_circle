(function(angular, app) {
    "use strict";
    app.controller('DailycheckController', ["$scope", "resources", "$window", "$http", function($scope, resources, $window, $http) {
        
        $scope.check_tags = [{value: 'Genral', key: "Genral"}, { value: 'Feedback', key:"Feedback"}, {value:'Appreciation', key:"Appreciation"}]
        
        $scope.labelClasses = {"Genral": 'label-primary',  "Feedback": 'label-info', "Appreciation": 'label-success'}
                
        $scope.loadDailychecks = function(role,check_id){
            $scope.role = role
            $scope.check_id = check_id
            resources.Dailycheck.query({check_id: $scope.check_id, role: $scope.role}).$promise.then(function(result){
                console.log(result)
                    $scope.dailychecks = result.daily_check
            })
            $scope.tkt_categs = resources.Ticketcateg.query();
            $scope.newDailycheck();
        }
        
        $scope.addTag = function(){
            
            $scope.dailycheck.tag = $scope.tag
        }

        $scope.newDailycheck = function(){
            $scope.dailycheck = new resources.Dailycheck({ description: ""})
            //$scope.dailycheck.cg_status = $scope.cg_status
            //$scope.dailycheck.conversation_for = $scope.conversation_for
        }

        $scope.edit = function(dailycheck){
            dailycheck.edit = true;
        }

        $scope.update = function(dailycheck){
            dailycheck.$update()
                .then(function(value) {
                    dailycheck.edit = false;
                    $scope.loadDailychecks($scope.role,$scope.check_id);
                }, function(reason) {
                    
                    // handle failure
                });
        }
        
        $scope.filtertype = function(role){
            return function (item) {
                if(role == 'Patient'){
                    if (item.category_type == 'Patient')
                    {
                        return true;
                    }
                }
                else if (role == 'Caregiver')
                {
                    if (item.category_type == 'Caregiver')
                    {
                        return true;
                    } 
                }
                return false;
            };
        }
        
        $scope.makeTicket = function(){
            $scope.maketick = true
            $("#ticket-pop-up").modal("show")
        }
        
        $scope.save = function(){
            if($scope.role== "Patient"){
                $scope.dailycheck.patient_id = $scope.check_id
                $scope.patient_id = null
                //$scope.dailycheck.role = "Patient"                 
                //alert($("#id").val())
            }
            else if($scope.role == "Caregiver"){
                $scope.dailycheck.care_giver_master_id = $scope.check_id
                $scope.caregiver_id = null
                // $scope.dailycheck.role = "Caregiver"
                //alert("caregiver")
            }                    
            $scope.dailycheck.$save()
                .then(function(value) {
                    $scope.loadDailychecks($scope.role,$scope.check_id);
                }, function(reason) {
                    // handle failure
                });
        }        
    }]);  
    
})(angular, lifeCircleApp);
