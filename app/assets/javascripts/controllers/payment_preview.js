(function(angular, app) {
    "use strict";
    app.controller('PaymentPreviewController',["$scope", "lifeCircleService", "$window",function($scope, lifeCircleService, $window){
        $scope.payment_modes = [{key: "CASH", value: "Cash"}, {key: "CHEQUE", value: "Cheque"}, {key: "ONLINE_TRANSFER", value: "OnlineTransfer"}, {key: "ONLINE_PAYMENT", value: "Online Payment"}];
        $scope.discount_options = [{key:"Absolute", value: "ABSOLUTE"}, {key:"Percentage", value:"PERCENTAGE"}]

        $scope.periodToFocused = function(payment_master){
            $scope.previoudPeriodTo = payment_master.period_to
            console.log($scope.previoudPeriodTo)
        }

        $scope.periodToBlured = function(payment_master, index){
            if($scope.previoudPeriodTo != payment_master.period_to){
                $("#recurringButton"+index).show()
            }
        }

        
        
        $scope.loadPaymentDetails = function(enrollmentId, generationDate){
            $scope.enrollmentId = enrollmentId
            $scope.generationDate = generationDate
            getPaymentForPreview()
        }

        var getPaymentForPreview = function(){
            lifeCircleService.getNewPaymentForReview($scope.enrollmentId, $scope.generationDate)
                .then(function(responce){
                    $scope.payment_masters =  responce.data.payment_masters
                    $('#recalculateButton').hide(); 
                    $('#sendInvoiceButton').show();
                    
                });
        }

        $scope.issue_credit_note = function(credit_note){
            lifeCircleService.issueCreditNote($scope.enrollmentId, credit_note.payment_master_id, credit_note.id)
                .then(function(responce){
                    getPaymentForPreview()
                });
        }

        $scope.revoke_credit_note = function(credit_note){
            lifeCircleService.revokeCreditNote($scope.enrollmentId, credit_note.payment_master_id, credit_note.id)
                .then(function(responce){
                    getPaymentForPreview()
                });
        }

        $scope.mark_as_pending_credit_note = function(credit_note){
            lifeCircleService.markAsPendingCreditNote($scope.enrollmentId, credit_note.payment_master_id, credit_note.id)
                .then(function(responce){
                    getPaymentForPreview()
                });
        }

        $scope.sendInvoice = function(payment_master){
            var discount_params = {discount: payment_master.discount, discount_type: payment_master.discount_type, discount_every_time: payment_master.discount_every_time}
            lifeCircleService.updatePaymentDiscountDetails($scope.enrollmentId, payment_master.id, discount_params)
                .then(function(responce){
                    $window.location.href = "/enrollments/"+$scope.enrollmentId+"/payment_masters/"+payment_master.id
                });
        }

        $scope.recalculate = function(payment){
            delete payment.credit_notes
            console.log(payment)
            lifeCircleService.recalculate($scope.enrollmentId, payment, $scope.generationDate)
                .then(function(responce){
                    getPaymentForPreview()
                    //alert("Success...Provide Payment Invoice show page")
                });
        }

        $scope.broadcast_discount_change = function(payment_master){
            if(payment_master.discount_type == "PERCENTAGE"){
                payment_master.total_amount = (payment_master.amount_in_rupees + payment_master.enrollment_fee - (payment_master.amount_in_rupees * (payment_master.discount/100)))}
            else{
                payment_master.total_amount = (payment_master.amount_in_rupees + payment_master.enrollment_fee - payment_master.discount)
            }
        }


    }]);  
})(angular, lifeCircleApp);


