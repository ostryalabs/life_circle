(function(angular, app) {
    "usestrict";
    app.controller('FormController',["$scope", "resources", "$window" , "lifeCircleService" ,function($scope, resources, $window , lifeCircleService) {
        
        $scope.loadForms = function(){
            $scope.forms = resources.Formmaster.query();
            //alert(JSON.stringify($scope.forms))
        }
        
        $scope.newForm = function(){
            $scope.form = new resources.Formmaster({form: null})
            $scope.form.isNew = true
        }

        $scope.edit = function(form){
            form.edit = true;
        }
        
        $scope.update = function(form){
            form.$update()
                .then(function(value) {
                    form.edit = false;
                    $scope.loadForms();
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.form.$save()
                .then(function(value) {
                    $scope.loadForms();
                    $window.location.reload();
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(form){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                form.$delete()
                    .then(function(responce){
                        $scope.forms.splice($scope.forms.indexOf(form), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
