(function(angular, app) {
    "usestrict";
    app.controller('TimeofadminController',["$scope", "resources", "$window",function($scope, resources, $window) {
        
        $scope.loadTimeofadmins = function(){
            $scope.timeofadmins = resources.Timeofadmin.query();
        }
        
        $scope.newTimeofadmin = function(){
            $scope.timeofadmin = new resources.Timeofadmin({time: null})
            $scope.timeofadmin.isNew = true
        }

        $scope.edit = function(timeofadmin){
            timeofadmin.edit = true;
        }

        $scope.update = function(timeofadmin){
            timeofadmin.$update()
                .then(function(value) {
                    timeofadmin.edit = false;
                    $scope.loadTimeofadmins();
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.timeofadmin.$save()
                .then(function(value) {
                    $scope.loadTimeofadmins();
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(timeofadmin){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                timeofadmin.$delete()
                    .then(function(responce){
                        $scope.timeofadmins.splice($scope.timeofadmins.indexOf(timeofadmin), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
