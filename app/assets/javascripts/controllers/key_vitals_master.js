(function(angular, app) {
    "usestrict";
    app.controller('KeyVitalsController',["$scope", "resources", "$window",function($scope, resources, $window) {
        
        $scope.loadkeyvitalsMasters = function(){
            //alert("load")
            $scope.keyvitalsmasters = resources.KeyvitalsMaster.query();
        }
        
        $scope.newkeyvitalsMaster = function(){
            //alert("new")       
            $scope.keyvitalsmaster = new resources.KeyvitalsMaster({keyvitalsmaster: null})
            $scope.keyvitalsmaster.isNew = true 
        }

        $scope.edit = function(keyvitalsmaster){
            keyvitalsmaster.edit = true;
        }

        $scope.update = function(keyvitalsmaster){
            keyvitalsmaster.$update()
                .then(function(value) {
                    keyvitalsmaster.edit = false;
                    $scope.loadkeyvitalsMasters();
                    
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.keyvitalsmaster.$save()
                .then(function(value) {
                    $scope.loadkeyvitalsMasters();
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(keyvitalsmaster){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                keyvitalsmaster.$delete()
                    .then(function(responce){
                        $scope.keyvitalsmasters.splice($scope.keyvitalsmasters.indexOf(keyvitalsmaster), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
