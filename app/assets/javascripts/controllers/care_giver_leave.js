(function(angular, app) {
    "use strict";
    app.controller('CareGiverLeaveController',["$scope", "resources", "$http", "$window",function($scope, resources, $http, $window) {
        

        $scope.loadAllCGLeaves = function(){
            $scope.care_giver_leaves = resources.CareGiverLeave.allCGLeaves();
        }

        $scope.loadCareGiverLeaves = function(careGiverMasterId){
            $scope.careGiverMasterId = careGiverMasterId
            $scope.care_giver_leaves = resources.CareGiverLeave.query({care_giver_master_id: careGiverMasterId});
            //$scope.care_giver_leave = new resources.CareGiverLeave({from_date: null, to_date: null, care_giver_master_id: $scope.careGiverMasterId, description: null})
        }
        
        $scope.newCareGiverLeave = function(){
            $scope.care_giver_leave = new resources.CareGiverLeave({from_date: null, to_date: null, care_giver_master_id: $scope.careGiverMasterId, description: null})
            $scope.care_giver_leave.isNew = true
            
        }

        $scope.close = function(){
            $scope.care_giver_leave.isNew = false;
        }
        
        $scope.edit = function(care_giver_leave){
            care_giver_leave.edit = true;
        }

        $scope.update = function(care_giver_leave){
            care_giver_leave.$update()
                .then(function(value) {
                    care_giver_leave.edit = false;
                    $scope.loadCareGiverLeaves($scope.careGiverMasterId);
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.approve = function(care_giver_leave){
            care_giver_leave.$approve()
                .then(function(value) {
                    $scope.loadAllCGLeaves()
                }, function(reason) {
                    // handle failure
                });
        }


        $scope.reject = function(care_giver_leave){
            care_giver_leave.$reject()
                .then(function(value) {
                    $scope.loadAllCGLeaves()
                }, function(reason) {
                    // handle failure
                });
        }


        $scope.newSave = function(cgleave){
            $scope.care_giver_leave = cgleave
            $scope.care_giver_leave.care_giver_master_id = $('#id').val()
            $scope.careGiverMasterId = $('#id').val()
            var url =  "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_leaves.json"
            $http.post(url, {care_giver_leave: $scope.care_giver_leave })
                .then(function(response){
                    //console.log(response)
                    $scope.care_giver_leave.isNew = false;
                    $scope.loadAllCGLeaves()
                },function(result){
                    alert(result)
                })
            // $scope.care_giver_leave.care_giver_master_id = $("#id").val();
            // $scope.care_giver_leave.$save()
            //     .then(function(value) {
            //         $scope.loadCareGiverLeaves($scope.careGiverMasterId);
            //     }, function(reason) {
            //         // handle failure
            //     });
        }

        
        $scope.save = function(){
            alert('came')
            if($('#id').val() != null){
                $scope.careGiverMasterId = $('#id').val()                
            }
            alert("saort")
            $scope.care_giver_leave.$save()
                .then(function(value) {
                    $scope.loadCareGiverLeaves($scope.careGiverMasterId);
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(care_giver_leave){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                care_giver_leave.$delete()
                    .then(function(responce){
                        $scope.care_giver_leaves.splice($scope.care_giver_leaves.indexOf(care_giver_leave), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);


