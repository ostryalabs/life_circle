(function(angular, app) {
    "use strict";
    app.controller('loginController',["$scope", "$location", "$http", "$window", function($scope, $location, $http, $window) {
        $scope.user = {login: "", password: ""}
        $scope.login = function(){
            console.log($scope.$parent.enquiry)
            $http.post("/users/sign_in.json", {user: $scope.user }).
                success(function(data, status, headers, config) {
                    $scope.$parent.bookCareGiver()
                    $scope.error = false
                }).
                error(function(data, status, headers, config) {
                    $scope.error = true
                    $scope.error_msg = data.error
                });
        }
    }]);  
})(angular, lifeCircleApp);

