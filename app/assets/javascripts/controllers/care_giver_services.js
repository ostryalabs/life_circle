(function(angular, app) {
    "use strict";
    app.controller('CareGiverServicesController',["$scope", "lifeCircleService", "$http", "params", "$state", function($scope, lifeCircleService, $http, params, $state){
        $scope.careGiverMasterId = params.careGiverMasterId
        var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_services/services_for_registration.json";
        $http.get(url)
            .then(function(response){
                $scope.services = response.data.care_giver_services
                var assign_count = 0
                var approve_count = 0
                angular.forEach($scope.services, function(service){
                    if(service.assigned == true){
                        assign_count += 1
                    }
                    if(service.approved == true || service.approved == 't'){
                        approve_count += 1
                    }
                })
                if(assign_count != 0 && assign_count == approve_count){
                    $scope.approved = true
                }else{
                    $scope.approved = false
                }
            })
        
        $scope.save = function(){
            var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_services/save_services.json";
            $http.post(url, {care_giver_services: $scope.services})
                .then(function(response){
                    $state.go('careGiverAvailability', {careGiverMasterId: $scope.careGiverMasterId})
                })
            
        }

        $scope.approve = function(){
            var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_services/approve.json/";
            $http.put(url)
                .then(function(response){
                    $scope.approved = response.data.status
                });
        }


    }]);  
    
})(angular, lifeCircleApp);


