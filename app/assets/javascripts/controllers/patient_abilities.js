(function(angular, app) {
    "use strict";
    app.controller('PatientabilitiesController',["$scope", "resources", "$window",function($scope, resources, $window) {
	$scope.success = false
        $scope.warning = false
        $scope.alertMessage = ""
        $scope.loadselfcares = function(enrollment_id){
            $scope.selfcare_masters = resources.Selfcare.query();
            $scope.loadPatientabilities(enrollment_id)
        }
        
        $scope.loadPatientabilities = function(enrollment_id){
            $scope.enrollment_id = enrollment_id
            
            //console.log($scope.routine_masters)
            $scope.patient_abilities = resources.Patientabilities.query({enrollment_id:enrollment_id});
            $scope.patient_abilities.$promise.then(function(data) {
                $scope.patient_abilities = data;
                angular.forEach($scope.patient_abilities, function(a){
                    angular.forEach($scope.selfcare_masters, function(b){
                        if(a.patient_selfcare_ability_master_id == b.id){
                            $scope.selfcare_masters.splice($scope.selfcare_masters.indexOf(b), 1)
                        }
                            
                    })
                })
                console.log($scope.selfcare_masters)
            });
        }

        $scope.filtertype = function(){
            return function (item) {
                if (item.attr == true)
                {
                    return false;
                }
                else
                return true;
            };
        }
        $scope.newPatientabilities = function(){               
            $scope.patient_ability = new resources.Patientabilities({enrollment_id: $scope.enrollment_id})
            $scope.patient_ability.isNew = true 
            //alert($scope.enrollment_id)
            
        }
       
        $scope.edit = function(patient_ability){
            patient_ability.edit = true;
        }

        $scope.update = function(patient_ability){
            //alert($scope.enrollment_id)
            patient_ability.$update()
                .then(function(value) {
                    //alert()
                    patient_ability.edit = false;
                    $scope.success = true
		    $scope.alertMessage = "Update Successfully"
                    //alert()
                    $scope.loadPatientabilities($scope.enrollment_id);
                }, function(reason) {
                   $scope.warning = true
		   $scope.alertMessage = "Error!"
                    //alert(reason)
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.patient_ability.$save()
                .then(function(value) {
                    //alert()
                    $scope.success = true
		    $scope.alertMessage = "Saved Successfully"
                    $scope.loadPatientabilities($scope.enrollment_id);  
                    //alert($scope.enrollment_id)             
                }, function(reason) {
                   $scope.warning = true
		   $scope.alertMessage = "Error!"
		   });
        }
        
        $scope.destroy = function(patient_ability){
            //alert()
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                patient_ability.$delete()
                    .then(function(responce){
                        $scope.patient_abilityies.splice($scope.patient_abilities.indexOf(patient_ability), 1)
                    })
            };
        }
      
        
    }]);  
    
})(angular, lifeCircleApp);
