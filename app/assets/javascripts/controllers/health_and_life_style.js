(function(angular, app) {
    "use strict";
    app.controller('HealthAndLifeStyleController',["$scope", "$http", "params", "$state", function($scope, $http, params, $state){
        $scope.careGiverMasterId = params.careGiverMasterId
        $scope.bloodGroups = [{name: "O+", value: "O+"},{name: "O-", value: "O-"}, {name: "A+", value: "A+"}, {name: "A-", value: "A-"}, {name: "B+", value: "B+"}, {name: "B-", value: "B-"}, {name: "AB+", value: "AB+"},{name: "AB-", value: "AB-"} ,{name: "Dont Know", value: "Dont Know"} ]
        var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_additional_details/health_and_lefe_style.json";
        $http.get(url)
            .then(function(response){
                $scope.health_and_life = response.data.care_giver_details
                console.log($scope.health_and_life)
            })
        
        $scope.save = function(){
            //$scope.health_and_life.health_approved = false
            var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_additional_details/save_additional_details.json";
            $http.post(url, {care_giver_additional_detail: $scope.health_and_life})
                .then(function(response){
                    console.log(response)
                    $scope.health_and_life = response.data.care_giver_details
                    $state.go('careGiverServices', {careGiverMasterId: $scope.careGiverMasterId})
                })


        }

        $scope.approve = function(helth_and_life){
            var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_additional_details/"+helth_and_life.id+"/approve_helth_and_life.json/";
            $http.put(url)
                .then(function(response){
                    helth_and_life.health_approved = response.data.status
                });
        }

    }]);  
    
})(angular, lifeCircleApp);

