(function(angular, app) {
    "use strict";
    app.controller('NewCityRequestController',["$scope", "lifeCircleService", function($scope, lifeCircleService) {

        var area = ""
        $scope.new_city_request_saved = false;
        for(var key in $scope.$parent.enquiry.address_attributes){
            area += ($scope.$parent.enquiry.address_attributes[key] + ", ")
        }
        area = area.substr(0, area.length-1)

        $scope.newCityRequest = {city: $scope.$parent.enquiry.address_attributes.city, area: area, email: null, mobile_no: null}
        
        $scope.submit_new_City_request = function(){
           
            lifeCircleService.submit_new_City_request($scope.newCityRequest)
                .then(function(responce){
                    $scope.new_city_request_saved = true
                    
                })
        }
    }]);  
    
})(angular, lifeCircleApp);




