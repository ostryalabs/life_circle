(function(angular, app) {
    "use strict";
    app.controller('missedCallRequestsController',["$scope", "lifeCircleService", function($scope, lifeCircleService) {
         $scope.getMissedCallRequests = function(page){
            lifeCircleService.missedCallRequests(page, $scope.page_status)
                .then(function(response){
                    injectValues(response)
                })
         }


         $scope.findMissedCallRequests = function(){
            lifeCircleService.missedCallRequestsWithFilter(1, $scope.page_status, $scope.mobile_no_filter)
                .then(function(response){
                    injectValues(response)
                })
         }

        var injectValues = function(response){
            $scope.missedCallRequests = response.data.missed_call_requests
            $scope.from_index = response.data.from_index
            $scope.to_index = response.data.to_index
            $scope.total_entries = response.data.total_entries;
            $scope.current_page = parseInt(response.data.current_page)
        }

    }]);  
    
})(angular, lifeCircleApp);
