(function(angular, app) {
    "use strict";
    app.controller('PatientRoutineController',["$scope", "resources", "$window",function($scope, resources, $window) {
	$scope.success = false
        $scope.warning = false
        $scope.alertMessage = ""
        $scope.loadPatientRoutines = function(enrollment_id){
            $scope.enrollment_id = enrollment_id
            $scope.routine_masters = resources.Routine.query();
            //console.log($scope.routine_masters)
            $scope.patient_routines = resources.PatientRoutine.query({enrollment_id:enrollment_id});
            $scope.patient_routines.$promise.then(function(data) {
                $scope.patient_routines = data[0];
                console.log(data[0])
            });
        }

        $scope.newPatientRoutine = function(){               
            $scope.patient_routine = new resources.PatientRoutine({enrollment_id: $scope.enrollment_id})
            $scope.patient_routine.isNew = true 
            //alert($scope.enrollment_id)
            
        }
       
        $scope.edit = function(patient_routine){
            patient_routine.edit = true;
        }

        $scope.update = function(patient_routine){
            //alert($scope.enrollment_id)
            patient_routine.$update()
                .then(function(value) {
                    //alert()
                    patient_routine.edit = false;
                    $scope.success = true
		    $scope.alertMessage = "Update Successfully"
                    //alert()
                    $scope.loadPatientRoutines($scope.enrollment_id);
                }, function(reason) {
               	   $scope.warning = true
		   $scope.alertMessage = "Error!"
                    //alert(reason)
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.patient_routine.$save()
                .then(function(value) {
                    //alert()
                    $scope.success = true
		    $scope.alertMessage = "Saved Successfully"
                    $scope.loadPatientRoutines($scope.enrollment_id);  
                    //alert($scope.enrollment_id)             
                }, function(reason) {
                   $scope.warning = true
		   $scope.alertMessage = "Error!"
                    // handle failure
                });
        }
        
        $scope.destroy = function(patient_routine){
            //alert()
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                patient_routine.$delete()
                    .then(function(responce){
                        $scope.patient_routines.splice($scope.patient_routines.indexOf(patient_routine), 1)
                        $scope.success = true
		        $scope.alertMessage = "Delete Successfull"
                    }, function(reason) {
                        $scope.warning = true
		        $scope.alertMessage = "Error!"
                    // handle failure
                });
            };
        }
      
        
    }]);  
    
})(angular, lifeCircleApp);
