(function(angular, app) {
    "use strict";
    app.controller('AdminPatientCreationController', ["$scope", "$http", "lifeCircleService", "$sce", function($scope, $http, lifeCircleService, $sce){
        $scope.registrars = [{key: 'Patient', value: "PATIENT"}, {key:'Relative', value:"RELATIVE"}, {key:'Admin', value:"ADMIN"}]
        $scope.user = {role: "patient", patient_name: "", email: "", mobile_no: "", user_name: "", registered_by: ""}
        $scope.existedUser = false
        $('#patientDetailId').bind('railsAutocomplete.select', function(event, data){
            $("#selectedUserId").val(data.item.id)
        });

        $scope.fetchPatientDetails = function(){
            var user_id = $("#selectedUserId").val()
            lifeCircleService.getPatientDetailsWithUserId(user_id)                
                .then(function(responce){
                    $scope.user = responce.data.patient
                    $scope.existedUser = true
                });
        }

        $scope.bookCG = function(){
            if(typeof $scope.user.patient_id != 'undefined' && $scope.user.patient_id != null ){
                $scope.$parent.enquiry.patient_id = $scope.user.patient_id
                $scope.$parent.bookCareGiver()
            }else{
                $scope.register()
            }
        }
        
        $scope.register = function(){
            var url = "/patients/create_patient_from_user_params.json"
            $http.post(url, {user: $scope.user}).
                success(function(response){
                    var data = response
                    if(data.status){
                        $scope.$parent.enquiry.patient_id = data.patient.patient_id
                        $scope.$parent.bookCareGiver()
                        $scope.error = false
                    }else{
                        $scope.error = true
                        $scope.error_msg = $sce.trustAsHtml(data.error_msg)
                    }
                }).
                error(function(data, status, headers, config) {
                    console.log(data)
                    $scope.error = true
                    var error_str = ""
                    angular.forEach(data.errors, function(value, key){
                        error_str += (key+": "+value+" <br/>")
                    });
                    $scope.error_msg = $sce.trustAsHtml(error_str)
                });
        }
        
    }]);  
    
})(angular, lifeCircleApp);
