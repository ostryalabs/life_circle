(function(angular, app) {
    "usestrict";
    app.controller('AdaptiveEquipmentController',["$scope", "resources", "$window",function($scope, resources, $window) {
        
        $scope.loadAdaptiveEquipments = function(){
            $scope.adaptiveEquipments = resources.AdaptiveEquipment.query();
        }
        
        $scope.newAdaptiveEquipment = function(){
            $scope.adaptiveEquipment = new resources.AdaptiveEquipment({adaptiveEquipment: null})
            $scope.adaptiveEquipment.isNew = true
        }

        $scope.edit = function(adaptiveEquipment){
            adaptiveEquipment.edit = true;
        }

        $scope.update = function(adaptiveEquipment){
            adaptiveEquipment.$update()
                .then(function(value) {
                    adaptiveEquipment.edit = false;
                    $scope.loadAdaptiveEquipments();
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.adaptiveEquipment.$save()
                .then(function(value) {
                    $scope.loadAdaptiveEquipments();
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(adaptiveEquipment){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                adaptiveEquipment.$delete()
                    .then(function(responce){
                        $scope.adaptiveEquipments.splice($scope.adaptiveEquipments.indexOf(adaptiveEquipment), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
