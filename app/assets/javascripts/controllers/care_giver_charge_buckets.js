(function(angular, app) {
    "use strict";
    app.controller('careGiverChargeBucketsController',["$scope", "lifeCircleService", "$window", function($scope, lifeCircleService, $window) {
        
        $scope.loadBuckets = function(){
            lifeCircleService.loadCareGiverChargeBuckets()
                .then(function(response){
                    $scope.charge_buckets = response.data

                })
        }

        $scope.saveBuckets = function(){
            if(validate()){
                lifeCircleService.saveCareGiverChargeBuckets($scope.charge_buckets)
                    .then(function(response){
                        $window.location = "/care_giver_charge_buckets"
                    })
            }else{
                alert("Invalid Ranges")
            }
        }

        $scope.addMore = function(){
            $scope.charge_buckets.push({hrs_from: null, hrs_to:null, charge_percentage: null})
        }

        var validate = function(){
            var minVal = null
            for(var i = 0 ; i < $scope.charge_buckets.length; i++){
                var value = $scope.charge_buckets[i];
                if(value.hrs_to != null && typeof value.hrs_from != 'undefined'&& value.hrs_from != null){
                    if(i==0){
                        minVal = value.hrs_from
                    }
                    if( (i != 0) && (value.hrs_from <= minVal || (value.hrs_from-minVal) > 1)){
                        console.log("value.hrs_from < minVal")
                        console.log(value.hrs_from)
                        console.log(minVal)
                        return false

                    }
                    minVal = value.hrs_from
                }
                if(value.hrs_to != null && typeof value.hrs_to != 'undefined'){
                    if((minVal >= value.hrs_to || (value.hrs_to - minVal) <= 0)){
                        console.log("minVal > value.hrs_to")
                        console.log(value.hrs_to)
                        console.log(minVal)

                        return false
                    }
                    minVal = value.hrs_to
                }
            };
            return true;
        }
        
    }]);  
    
})(angular, lifeCircleApp);

