(function(angular, app) {
    "usestrict";
    app.controller('TicketController',["$scope", "resources", "$http", "$window",function($scope, resources, $http, $window) {
        
        $scope.loadTickets = function(role, current_tag){
            $scope.role = role
            $scope.current_tag = current_tag
            $scope.tickets = resources.Ticket.query({role: role, current_tag: current_tag});
            $scope.loaddailycheck();             
        }
        
        $scope.loaddailycheck = function(){
            $scope.tkt_categs = resources.Ticketcateg.query();               
        }
        

        $scope.filtertype = function(role){
            return function (item) {
                if(role == 'patient'){
                    if (item.category_type == 'Patient')
                    {
                        return true;
                    }
                }
                else if (role == 'care_giver')
                {
                    if (item.category_type == 'Caregiver')
                    {
                        return true;
                    } 
                }
                return false;
            };
        }
        
        $scope.rolefilter = function(role){
            return function (item) {
                if(role == 'Patient'){
                    if (item.category_type == 'Patient')
                    {
                        return true;
                    }
                }
                else if (role == 'Caregiver')
                {
                    if (item.category_type == 'Caregiver')
                    {
                        return true;
                    } 
                }
                return false;
            };
        }
        
        $scope.newTicket = function(){
            $scope.ticket = new resources.Ticket({ticket: null})
            $scope.ticket.isNew = true
        }
        
        $scope.close = function(){
            $scope.ticket.isNew = false;
        }
        
         $scope.mclose = function(){
            $("#ticket-pop-up").modal("hide")
            $scope.ticket.ticket_category_master_id =''
        }

        $scope.edit = function(ticket){
            ticket.edit = true;
            ticket.status = 'Open'
            $scope.update(ticket)
        }

        $scope.update = function(ticket){
            ticket.$update()
                .then(function(value) {
                    $scope.ticket = value.ticket
                    //alert($scope.ticket.id)
                    ticket.edit = false;
                    $scope.loadTickets($scope.role, $scope.current_tag);
                    $window.location.href = '/ticket_conversations?ticket_id='+$scope.ticket.id;
                }, function(reason) {
                    
                    // handle failure
                });
        }
        
        $scope.msave = function(role,id){
            if(role == 'Patient')
       	    $scope.ticket.patient_id = id
       	    else
       	    $scope.ticket.caregiver_id = id
            $scope.ticket.role = role
            var url =  '/tickets.json?role='+role
            $http.post(url, {role: role, ticket : $scope.ticket})
                .then(function(response){
                    $("#ticket-pop-up").modal("hide")
                })          
        }

        $scope.save = function(c_role, role ){
            if(c_role == 'l_c_agent'){
                if(role == "Patient"){
                    $scope.ticket.patient_id = $("#id").val()
                    $scope.ticket.role = "Patient"                 
                    //alert($("#id").val())
                }
                else if(role == "Caregiver"){
                    $scope.ticket.care_giver_master_id = $("#id").val()
                    $scope.ticket.role = "Caregiver"
                    //alert("caregiver")
                }
                
            }
            $scope.ticket.$save()
                .then(function(value) {
                    $scope.loadTickets($scope.role, $scope.current_tag);
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(ticket){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                ticket.$delete()
                    .then(function(responce){
                        $scope.tickets.splice($scope.tickets.indexOf(ticket), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
