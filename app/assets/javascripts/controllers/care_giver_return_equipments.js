(function(angular, app) {
    "usestrict";
    app.controller('caregiverEquipmentsController',["$scope" ,"lifeCircleService", "resources", function($scope,lifeCircleService, resources) {
    
        $scope.equipments =[]
        $scope.loadEquipments = function(id){
            //alert(id)
            $scope.care_giver_id = id
            lifeCircleService.getEquipments($scope.care_giver_id)
		.then(function(result){
		    $scope.equipments = result.data
                    console.log($scope.equipments)
		});           
        }

        $scope.save = function(){
            alert($scope.care_giver_id)
            lifeCircleService.saveEquipments($scope.care_giver_id, $scope.equipments)
		.then(function(result){
		    $scope.equipments = result.data
		    //alert(JSON.stringify(result.data))
                    console.log($scope.equipments)
                    $scope.loadEquipments($scope.care_giver_id)
		});
        }

        $scope.update = function(equipment){
            //alert(JSON.stringify(abilities))
            lifeCircleService.updateEquipments($scope.care_giver_id, equipment)
		.then(function(result){
                    equipment.edit = false
                   // alert(result.data)
		    $scope.updateabilities = result.data
		});
        }
       
        
    }]);  
    
})(angular, lifeCircleApp);
