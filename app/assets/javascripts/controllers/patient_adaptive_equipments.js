(function(angular, app) {
    "usestrict";
    app.controller('PatientAdaptiveEquipmentsController',["$scope" ,"lifeCircleService", "resources", function($scope,lifeCircleService, resources) {
    	$scope.success = false
        $scope.warning = false
        $scope.equipments =[]
        $scope.loadAdaptiveEquipments = function(id){
            //alert(id)
            $scope.enrollment_id = id
            lifeCircleService.getPatientEquipments($scope.enrollment_id)
		.then(function(result){
		    $scope.equipments = result.data
                    console.log($scope.equipments)
		});           
        }

        $scope.save = function(){
            //alert($scope.enrollment_id)
            lifeCircleService.savePatientEquipments($scope.enrollment_id, $scope.equipments)
		.then(function(result){
		    $scope.equipments = result.data
		    $scope.success = true
                    //alert(JSON.stringify(result.data))
                    console.log($scope.equipments)
                    $scope.loadAdaptiveEquipments($scope.enrollment_id)
		}, function(reason) {
			$scope.warning = true
                    // handle failure
                });
        }

        $scope.update = function(equipment){
            //alert(JSON.stringify(abilities))
            lifeCircleService.updatePatientEquipments($scope.enrollment_id, equipment)
		.then(function(result){
                    equipment.edit = false
                   // alert(result.data)
		    $scope.updateabilities = result.data
		});
        }
       
        
    }]);  
    
})(angular, lifeCircleApp);
