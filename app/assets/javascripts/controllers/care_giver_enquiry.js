(function(angular, app) {
    "use strict";
    app.controller('CareGiverEnquiryController',["$scope", "lifeCircleService", "$location", "$http", "$state", "localStorageService", "$rootScope", function($scope, lifeCircleService, $location, $state, localStorageService, $rootScope , $http) {
             
	
        
        $scope.submit_all = function(){
            $scope.enquiry.patient_address = address_auto_complete()
            $scope.address_final = [];
	    var a = $scope.enquiry.patient_address
	    for (var key in a) {
		$scope.address_final.push(a[key]) ;
	    }
	    
            if($scope.enquiry.address_attributes  == 0){
	    }
	    else{
	        lifeCircleService.check_city_support($scope.enquiry.patient_address.city)
		    .then(function(responce){
		        var isCityExists = responce.data.isCityExists
		    	
                        if(typeof isCityExists != 'undefined' && isCityExists === true){
                            //window.location.href = '/care_giver_masters/care_giver_enquiry?city='+add
                            lifeCircleService.save_cg_enquiry($scope.enquiry)
                            	.then(function(result){
                                    window.location.href = '/care_giver_enquiries/'+result.data.id
                                });
                        }else{
                           
                            window.location.href = '/care_giver_enquiries/new_city_request?city='+$scope.enquiry.patient_address.city+'&name='+$scope.enquiry.name+'&email='+$scope.enquiry.email+'&phone='+$scope.enquiry.phone_no
                            
		        }
                        
                    })
            }
        }
	
        

	
	var get_all_locations_from_auto_complete = function() {
	    var add = [];
	    if($scope.enquiry.place_details_str1.length > 0){
		if($("#place_details").val()){
		    var gmapAddress = JSON.parse($("#place_details_object").val())
		    add.push(gmapAddress);}
		else
		{
		    alert('Enter Valid Location')
		    return 0;
		} 
		
	    }
	   		    
	    
	    var address = []
            var line1 = ""
            var line2 = ""
	    for(var i=0; i < add.length ; i++)
	    {
		if(typeof add[i].route != 'undefined' && add[i].route.length > 1){
                    line1 += (add[i].route + ", ")
		}
		if(typeof add[i].sublocality_level_3 != 'undefined' && add[i].sublocality_level_3.length > 1){
                    line1 += add[i].sublocality_level_3
		}
		if(typeof add[i].sublocality_level_2 != 'undefined' && add[i].sublocality_level_2.length > 1){
                    line2 += (add[i].sublocality_level_2 + ", ")
		}
		if(typeof add[i].sublocality_level_1 != 'undefined' && add[i].sublocalit1y_level_1.length > 1){
                    line2 += add[i].sublocality_level_1
		}
		if(line1.length > 1){
                    address.line1 = line1
		}
		if(line2.length > 1){
                    address.line2 = line2
		}
		
		if(typeof add[i].locality != 'undefined' && add[i].locality.length > 1){
                    address.city = add[i].locality
		}
		
		if(typeof add[i].country != 'undefined' && add[i].country.length > 1){
                    address.country = add[i].country
		}
		
		if(typeof add[i].postal_code != 'undefined' && add[i].postal_code.length > 1){
                    address.postal_code = add[i].postal_code
		}
		
		address.push(address.city);
	    
	    }
	    return address
	}

       
        var address_auto_complete = function(){
	    var gmapAddress = JSON.parse($("#place_details_object").val())
            
            var address = {}
            var line1 = ""
            var line2 = ""
            if(typeof gmapAddress.route != 'undefined' && gmapAddress.route.length > 1){
                line1 += (gmapAddress.route + ", ")
            }
            if(typeof gmapAddress.sublocality_level_3 != 'undefined' && gmapAddress.sublocality_level_3.length > 1){
                line1 += gmapAddress.sublocality_level_3
            }
            if(typeof gmapAddress.sublocality_level_2 != 'undefined' && gmapAddress.sublocality_level_2.length > 1){
                line2 += (gmapAddress.sublocality_level_2 + ", ")
            }
            if(typeof gmapAddress.sublocality_level_1 != 'undefined' && gmapAddress.sublocality_level_1.length > 1){
                line2 += gmapAddress.sublocality_level_1
            }
            if(line1.length > 1){
                address.line1 = line1
            }
            if(line2.length > 1){
                address.line2 = line2
            }

            if(typeof gmapAddress.locality != 'undefined' && gmapAddress.locality.length > 1){
                address.city = gmapAddress.locality
            }

            if(typeof gmapAddress.country != 'undefined' && gmapAddress.country.length > 1){
                address.country = gmapAddress.country
            }

            if(typeof gmapAddress.postal_code != 'undefined' && gmapAddress.postal_code.length > 1){
                address.postal_code = gmapAddress.postal_code
            }
            return address;
        }

        $scope.applyAccordian = function(){
            accordian.invoke()
        }

        
    }]);  
    
})(angular, lifeCircleApp);
