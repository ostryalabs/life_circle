(function(angular, app) {
    "use strict";
    app.controller('jobRunsController',["$scope", "lifeCircleService", "$window",function($scope, lifeCircleService, $window){
        $scope.loadJobPayments = function(page, jobRunId){
            $scope.jobRunId = jobRunId
            $scope.getJobPayments(page)
        }

        $scope.getJobPayments = function(page){        
            lifeCircleService.getJobPayments(page, $scope.jobRunId)
                .then(function(response){
                    $scope.payment_masters = response.data.payment_masters
                    $scope.from_index = response.data.from_index
                    $scope.to_index = response.data.to_index
                    $scope.total_entries = response.data.total_entries;
                    $scope.current_page = parseInt(response.data.current_page)
                })
        }

        $scope.mailSelectedPayments = function(){
            lifeCircleService.mailSelectedPayments($scope.jobRunId, getCheckedPaymentIds())
                .then(function(response){
                    if(response.data.status == true){
                        markPaymentsAsMailSent(getCheckedPayments())
                    }
                })

        }

        $scope.mailAllPayments = function(){
            lifeCircleService.mailAllPayments($scope.jobRunId)
                .then(function(response){
                    if(response.data.status == true){
                        markPaymentsAsMailSent($scope.payment_masters)
                    }
                })

        }

        $scope.nullifySelectedPayments = function(){
            lifeCircleService.nullifySelectedPayments($scope.jobRunId, getCheckedPaymentIds())
                .then(function(response){
                    if(response.data.status == true){
                        markPaymentsAsNullified(getCheckedPayments())
                    }

                })

        }

        var markPaymentsAsMailSent = function(payment_masters){
            angular.forEach(payment_masters, function(payment){
                payment.mail_sent = true;
            });
        }

        var markPaymentsAsNullified = function(payment_masters){
            angular.forEach(payment_masters, function(payment){
                payment.nullified = true;
            });
        }


        var getCheckedPaymentIds = function(){
            var checkedPayments = new Array()
            angular.forEach($scope.payment_masters, function(payment){
               if(typeof payment.isChecked != 'undefined' && payment.isChecked == true){
                   checkedPayments.push(payment.id)
               } 
            });
            return checkedPayments;
        }

        var getCheckedPayments = function(){
            var checkedPayments = []
            angular.forEach($scope.payment_masters, function(payment){
               if(typeof payment.isChecked != 'undefined' && payment.isChecked == true){
                   checkedPayments.push(payment)
               } 
            });
            return checkedPayments;
        }

        
    }]);  
})(angular, lifeCircleApp);


