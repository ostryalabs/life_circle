(function(angular, app) {
    "usestrict";
    app.controller('TicketcategController',["$scope", "resources", "$window",function($scope, resources, $window) {
    
        $scope.type_tags = [{value: 'Patient', key: "Patient"}, { value: 'Caregiver', key:"Caregiver"}]

        $scope.loadTicketcategs = function(){
            $scope.ticketcategs = resources.Ticketcateg.query();
        }
        
        $scope.newTicketcateg = function(){
            $scope.ticketcateg = new resources.Ticketcateg({ticketcateg: null})
            $scope.ticketcateg.isNew = true
        }

        $scope.edit = function(ticketcateg){
            ticketcateg.edit = true;
        }

        $scope.update = function(ticketcateg){
            alert(JSON.stringify(ticketcateg))
            ticketcateg.$update()
                .then(function(value) {
                    ticketcateg.edit = false;
                    $scope.loadTicketcategs();
                }, function(reason) {
                    console.log(reason)
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.ticketcateg.$save()
                .then(function(value) {
                    $scope.loadTicketcategs();
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(ticketcateg){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                ticketcateg.$delete()
                    .then(function(response){
                        $scope.ticketcategs.splice($scope.ticketcategs.indexOf(ticketcateg), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
