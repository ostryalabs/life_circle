(function(angular, app) {
    "use strict";
    app.controller('CareGiverEducationController',["$scope", "$http", "params", "FileUploader","$state", function($scope, $http, params, FileUploader, $state){
        $('input[data-toggle="tooltip"]').tooltip();
        $scope.careGiverMasterId = params.careGiverMasterId
        var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_educations/education_for_registration.json";
        var saveUrl = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_educations.json";
        $http.get(url)
            .then(function(response){
                console.log(response)
                $scope.degrees = response.data.degrees
                //alert(JSON.stringify($scope.degrees))
                $scope.uploader = new FileUploader({
                    url: saveUrl,
                    autoUpload: false,
                    removeAfterUpload: true,
                    alias: "attachment",
                    headers: {
                        "X-CSRF-Token" : $('meta[name="csrf-token"]').attr('content')
                    }
                }
                );
            })
        
	$scope.get_date = function(from_date ,to_date){
	     var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_educations/cg_dob.json";
	    $http.get(url)
		.then(function(response){
		    $scope.dob = response.data.status
		    var date1 = from_date; //original_date
		    var temp = date1.split('/'); //splitting the date
		    var format = temp[1] + '/' + temp[0] + '/' + temp[2]; //changing the date format to mm/dd/yyyy
		    
		    $scope.firstDate = temp[2];
		    var date2 = to_date; //original_date
		    var temp2 = date2.split('/'); //splitting the date
		    var format2 = temp2[1] + '/' + temp2[0] + '/' + temp2[2]; //changing the date format to mm/dd/yyyy
		    $scope.secondDate = temp2[2];
		    
		    
		    
		    var dod = $scope.dob
		    var temp3 = dod.split('/');
		    var format3 = temp3[1] + '/' + temp3[0] + '/' + temp3[2];
		    $scope.d = temp3[2];
		    if($scope.d < $scope.firstDate && $scope.d < $scope.secondDate){
			alert('From Date and To date Should be greater than the Date of Birth')
		    }
		    if($scope.firstDate > $scope.secondDate )
		    {
			alert('From Date should be Less than To date')
		    } 
		})
	}
        $scope.addMore = function(){            
            var newDegree = {}
            angular.forEach($scope.degrees[0], function(val, key){
                newDegree[key] = "";
            })
            newDegree.care_giver_master_id = $scope.careGiverMasterId
            $scope.degrees.push(newDegree)
        }

        $scope.save = function(){
            //alert(JSON.stringify($scope.degrees))
	    var degrees_without_files = []
            var degrees_with_files = []
            angular.forEach($scope.degrees, function(degree, index){
                if(typeof degree.institute != 'undefined' && degree.institute != null && degree.institute.length > 1){
                    var isFileAttached = $("#file_field_"+index).val()
                    if(typeof isFileAttached != 'undefined' && isFileAttached.length > 1){
                        degrees_with_files.push(degree)
                    }else{
                        degrees_without_files.push(degree)
                    }
                }
            })
            if(degrees_with_files.length > 0){
                angular.forEach($scope.uploader.queue, function(val, key){                
                    angular.forEach(degrees_with_files[key], function(val, keyInner){               
                        if(val == null || typeof val == 'undefined'){
                            degrees_with_files[key][keyInner] = ""
                        }
                    });
                    val.formData.push(degrees_with_files[key])
                    val.upload();
                });
            }
            
            var saveUrl = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_educations/save_education.json";
            if(degrees_without_files.length > 0){
                $http.post(saveUrl, {care_giver_educations: degrees_without_files})
                    .then(function(response){
                        console.log(response)                       
                    })

            }
          $state.go('careGiverEmployer', {careGiverMasterId: $scope.careGiverMasterId})
        }

        $scope.approve = function(education){
            var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_educations/"+education.id+"/approve.json/";
            $http.put(url)
                .then(function(response){
                    education.approved = response.data.status
                });
        }

    }]);  
    
})(angular, lifeCircleApp);
