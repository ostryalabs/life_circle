(function(angular, app) {
    "use strict";
    app.controller('CareGiverStayController',["$scope", "$http", "params", "$state", function($scope, $http, params, $state){
        $scope.careGiverMasterId = params.careGiverMasterId
        $scope.job_types = ["LIVE_IN", "LIVE_OUT", "BOTH"];
        var url = "/care_giver_masters/"+$scope.careGiverMasterId+".json";
        $http.get(url)
            .then(function(response){
                $scope.care_giver_master = response.data
                $scope.jobType = $scope.care_giver_master.job_type
            })

        $scope.updateCGStay = function(){
            $scope.updateStatusMsg = ""
            $scope.updateStatus = false;
            var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/update_cg_stay.json";
            $http.put(url, {job_type: $scope.jobType})
                .then(function(response){
                    $scope.updateStatus = response.data.status;
                    $scope.updateStatusMsg = response.data.msg;
                })
            
        }
    }]);  
    
})(angular, lifeCircleApp);
