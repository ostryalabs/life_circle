(function(angular, app) {
    "usestrict";
    app.controller('PatientDisabilitiesController',["$scope" ,"lifeCircleService", "resources", function($scope,lifeCircleService, resources) {
        $scope.success = false
        $scope.warning = false
        $scope.disabilities =[]
        $scope.loadDisabilities = function(id){
            $scope.enrollment_id = id
            lifeCircleService.getPatientDisabilities($scope.enrollment_id)
		.then(function(result){
		    $scope.disabilities  = result.data
                    console.log($scope.disabilities)
		});           
        }

        $scope.save = function(){
            //alert(JSON.stringify($scope.disabilities))
            lifeCircleService.savePatientDisabilities($scope.enrollment_id, $scope.disabilities)
		.then(function(result){                    
		    $scope.disabilities = result.data
		    $scope.success = true
                    $scope.loadDisabilities($scope.enrollment_id)
                    //alert(JSON.stringify(result.data))
                    console.log($scope.disabilities)
		}, function(reason) {
			$scope.warning = true
                    // handle failure
                });
        }

        // $scope.update = function(equipment){
        //     //alert(JSON.stringify(abilities))
        //     lifeCircleService.updatePatientDisabilities($scope.enrollment_id, $scope.disabilities)
	// 	.then(function(result){
        //             equipment.edit = false
        //            // alert(result.data)
	// 	    $scope.updateDisabilitie = result.data
	// 	});
        // }
       
        
    }]);  
    
})(angular, lifeCircleApp);
