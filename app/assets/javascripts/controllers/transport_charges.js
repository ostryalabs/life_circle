(function(angular, app) {
    "use strict";
    app.controller('TransportChargeController',["$scope", "resources", "$window",function($scope, resources, $window) {
        
        $scope.loadTransportCharges = function(){
            $scope.transport_charges = resources.TransportCharge.query();
        }
        
        $scope.newTransportCharge = function(){
            $scope.transport_charge = new resources.TransportCharge({from_distance: null, to_distance: null, charge: null})
            $scope.transport_charge.isNew = true
        }

        $scope.edit = function(transport_charge){
            transport_charge.edit = true;
        }

        $scope.update = function(transport_charge){
            transport_charge.$update()
                .then(function(value) {
                    transport_charge.edit = false;
                    $scope.loadTransportCharges();
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.transport_charge.$save()
                .then(function(value) {
                    $scope.loadTransportCharges();
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(transport_charge){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                transport_charge.$delete()
                    .then(function(responce){
                        $scope.transport_charges.splice($scope.transport_charges.indexOf(transport_charge), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
