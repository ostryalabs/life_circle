(function(angular, app) {
    "use strict";
    app.controller('EnrollmentClosingPaymentController',["$scope", "lifeCircleService", "$window",function($scope, lifeCircleService, $window){
        
        $scope.loadPaymentDetails = function(enrollmentId, closingDate){
            $scope.enrollmentId = enrollmentId
            $scope.closingDate = closingDate
            getPaymentAttributes()
        }

        $scope.toggleAll = function(context){
            var scopedVariable = ""
            if(context == 'pending'){
                scopedVariable = $scope.checkAllpending
            }else if(context == 'issued'){
                scopedVariable = $scope.checkAllissued
            }else{
                scopedVariable = $scope.checkAllto_be_issued
            }
            
            angular.forEach($scope.getContextBasedCreditNotes(context), function(cr){
                cr.checked = scopedVariable
            });
            $scope.reflectTotalCreditNoteAmount();
        }

        $scope.reflectTotalCreditNoteAmount = function(){
            $scope.total_credit_amount = 0

            $scope.total_credit_amount_to_be_issued = 0
            $scope.total_credit_amount_issued = 0
            $scope.total_credit_amount_pending = 0

            angular.forEach($scope.payment_attributes.credit_notes_pending, function(cr){
                    $scope.total_credit_amount_pending += cr.value_of_credit_note
            });
            angular.forEach($scope.payment_attributes.credit_notes_issued, function(cr){
                    $scope.total_credit_amount_issued += cr.value_of_credit_note
            });
            $scope.total_credit_amount = ($scope.total_credit_amount_pending+ $scope.total_credit_amount_issued);
        }

        var getPaymentAttributes = function(){
            lifeCircleService.getClosingEnrollmentPaymentAttributes($scope.enrollmentId, $scope.closingDate)
                .then(function(responce){
                    $scope.total_credit_amount = 0
                    $scope.payment_attributes =  responce.data
                    angular.forEach($scope.payment_attributes.credit_notes, function(cr){
                        $scope.total_credit_amount += cr.value_of_credit_note
                    });
                    $scope.reflectTotalCreditNoteAmount()
                    console.log($scope.payment_attributes)
                });
        }

        $scope.getContextBasedCreditNotes = function(context){
            if(typeof $scope.payment_attributes != 'undefined'){ 
                if(context == 'issued'){
                    return $scope.payment_attributes.credit_notes_issued
                }else if(context == 'to_be_issued'){
                    return $scope.payment_attributes.credit_notes_to_be_issued
                }else{
                    return $scope.payment_attributes.credit_notes_pending
                }
            }
        }

        $scope.issue_action_on_credit_note = function(credit_note){
            credit_note.status = "ISSUED"
            $scope.payment_attributes.credit_notes_issued.push(credit_note);
            var index = $scope.payment_attributes.credit_notes_pending.indexOf(credit_note)
            $scope.payment_attributes.credit_notes_pending.splice(index, 1);
            $scope.reflectTotalCreditNoteAmount();
        }

        $scope.issueActionOnMultipleCreditNotes = function(){
            var selectedCreditNotes = []
            angular.forEach($scope.payment_attributes.credit_notes_pending, function(cr){
                if(cr.checked){
                    $scope.issue_action_on_credit_note(cr)
                }
            });
        }

        $scope.revokeActionOnMultipleCreditNotes = function(){
            angular.forEach($scope.payment_attributes.credit_notes_issued, function(cr){
                if(cr.checked){
                    $scope.revoke_action_on_credit_note(cr)
                }
            });
        }

        $scope.revoke_action_on_credit_note = function(credit_note){
            credit_note.status = "PENDING"
            $scope.payment_attributes.credit_notes_pending.push(credit_note);
            var index = $scope.payment_attributes.credit_notes_issued.indexOf(credit_note)
            $scope.payment_attributes.credit_notes_issued.splice(index, 1);
            $scope.reflectTotalCreditNoteAmount();
        }

        $scope.saveClosingPayment = function(){
            var credit_notes = $scope.payment_attributes.credit_notes_issued.concat($scope.payment_attributes.credit_notes_pending)
            var payment_master = "";
            if(typeof $scope.payment_attributes.payment_master != 'udefined'){
                payment_master = $scope.payment_attributes.payment_master;
            } 
            lifeCircleService.saveClosingPayment($scope.enrollmentId, credit_notes, payment_master)
                .then(function(response){
                    $window.location.href = "/enrollments/"+$scope.enrollmentId+"/payment_masters/"+response.data.payment_master.id+"/credit_notes"
                });

        }


        $scope.destroy_credit_note = function(credit_note, reload){
            lifeCircleService.destroyCreditNote($scope.enrollmentId, credit_note.payment_master_id, credit_note.id)
                .then(function(responce){
                    if(reload){

                    }
                    return true;
                });
        }

        $scope.update_credit_note_status = function(credit_note, status, reload){
            lifeCircleService.updateCreditNoteStatus($scope.enrollmentId, credit_note.payment_master_id, credit_note.id, status)
                .then(function(responce){
                    var credit_notes_having_status = $scope.getContextBasedCreditNotes(status.toLowerCase())
                    var index = credit_notes_having_status.indexOf(credit_note)
                    credit_notes_having_status.splice(index, 1);
                    $scope.payment_attributes.credit_notes_to_be_issued.push(responce.data.credit_note)
                    return true;
                });
        }

        $scope.create_credit_note = function(credit_note, reload){
            lifeCircleService.createCreditNote($scope.enrollmentId, credit_note.payment_master_id, credit_note)
                .then(function(responce){
                    move_credit_note_to_next_level('pending', responce.data.credit_note)
                    return true;
                });
        }

    }]);  
})(angular, lifeCircleApp);


