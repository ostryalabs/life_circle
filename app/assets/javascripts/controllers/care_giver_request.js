(function(angular, app) {
    "use strict";
    app.controller('CareGiverRequestController',["$scope", "lifeCircleService", "$location", "$state", "localStorageService", "$rootScope", function($scope, lifeCircleService, $location, $state, localStorageService, $rootScope) {
        $scope.enquiry = {}
        
	$scope.submit_city_and_area = function(){
	    
	    $scope.enquiry.address_attributes = get_address_details_from_auto_complete()
	    //alert(JSON.stringify($scope.enquiry.address_attributes))
	    $scope.address_final = [];
	    var a = $scope.enquiry.address_attributes
	    for (var key in a) {
		$scope.address_final.push(a[key]) ;
	    }
	    
	    if($scope.enquiry.address_attributes  == 0){
	    }
	    else{
		localStorageService.set("enquiry", JSON.stringify($scope.enquiry))
		localStorageService.set("currentState", "check_city_support")
		window.location.href = "/enquiry"
	    }
	}

			
	window.onbeforeunload = function () {
            localStorageService.set("enquiry", JSON.stringify($scope.enquiry))
        };
	
	var get_address_details_from_auto_complete = function(){
	    if($("#place_details_object").val()){
		
		var gmapAddress = JSON.parse($("#place_details_object").val())
	    
		var address = {}
		var line1 = ""
		var line2 = ""
		if(typeof gmapAddress.route != 'undefined' && gmapAddress.route.length > 1){
                    line1 += (gmapAddress.route + ", ")
		}
		if(typeof gmapAddress.sublocality_level_3 != 'undefined' && gmapAddress.sublocality_level_3.length > 1){
                    line1 += gmapAddress.sublocality_level_3
		}
		if(typeof gmapAddress.sublocality_level_2 != 'undefined' && gmapAddress.sublocality_level_2.length > 1){
                    line2 += (gmapAddress.sublocality_level_2 + ", ")
		}
		if(typeof gmapAddress.sublocality_level_1 != 'undefined' && gmapAddress.sublocality_level_1.length > 1){
                    line2 += gmapAddress.sublocality_level_1
		}
		if(line1.length > 1){
                    address.line1 = line1
		}
		if(line2.length > 1){
                    address.line2 = line2
		}

		if(typeof gmapAddress.locality != 'undefined' && gmapAddress.locality.length > 1){
                    address.city = gmapAddress.locality
		}

		if(typeof gmapAddress.country != 'undefined' && gmapAddress.country.length > 1){
                    address.country = gmapAddress.country
		}

		if(typeof gmapAddress.postal_code != 'undefined' && gmapAddress.postal_code.length > 1){
                    address.postal_code = gmapAddress.postal_code
		}
		return address;
	    }
	    else
	    {
		alert('Enter Valid Location')
		return 0;
	    }
        }
    }]);  
    
})(angular, lifeCircleApp);
