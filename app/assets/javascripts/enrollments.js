$(document).ready(function(){
    $("#paymentDiscountType").change(function(e){
        calculateTotalAmount();
    })

    $("#paymentDiscount").on("blur", function(e){
        calculateTotalAmount();
    })

});

function reflectDistanceChange(index){
    var cgTravelDistance = parseInt($("#cgTravelDistance-"+index).val())
    $.get( "/transport_charge_masters/charge_for_distance.json?distance="+cgTravelDistance, function( data ) {
        console.log(data)
        $("#cgTravelCharge-"+index).val(data.charge)
        calculateTotal(index)
    });
}

function calculateTotal(index){
    var cgCharge = parseInt($("#cgCharge-"+index).val())
    var cgTravelCharge = parseInt($("#cgTravelCharge-"+index).val())
    $("#cgTotal-"+index).val(cgCharge+cgTravelCharge)
    calculateGrandTotal();
}

function calculateGrandTotal(){
    var total = 0
    $("input[rel='totalConsiderable']").each( function(){
        total += parseInt($(this).val())
    });
    $("#cgGrandTotal").val(total)    
    var noOfDays = parseInt($("#noOfDays").val())
    $("#paymentAmountInRupees").val(Math.round(total*noOfDays))    
    calculateTotalAmount();
}

function calculateTotalAmount(){
    var amount = parseInt($("#paymentAmountInRupees").val())
    var discount = parseInt($("#paymentDiscount").val())
    var  enrollmentFee= parseInt($("#paymentEnrollmentFee").val())
    if(typeof amount != 'undefined' && amount > 0 && typeof discount != 'undefined' && discount > 0){
        var discountType = $("#paymentDiscountType").val()
        var total = amount;
        if(discountType == 'PERCENTAGE'){
            total = Math.round((amount - (amount*(discount/100))))
        }else{
            total = Math.round(amount - discount)
        }
        if(typeof enrollmentFee != 'undefined' && enrollmentFee > 0){
            total = total+enrollmentFee
        }
        
        console.log(total)
        $("#paymentTotal").val(total)
    }else{
        $("#paymentTotal").val((amount+enrollmentFee))
    }
}
