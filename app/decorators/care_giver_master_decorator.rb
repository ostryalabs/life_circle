class CareGiverMasterDecorator < Draper::Decorator
  delegate_all
  attr_accessor :total_charge
  
  def image_url
    profile_pic.thumb.url
  end

  def image_xs_url
    profile_pic.xs.url
  end


  def total_charge_per_day
    if approximate_travel_distance.present?
      charge = TransportChargeMaster.having_distance(approximate_travel_distance).first.try(:charge).to_i
      self.model.total_charge + charge
    else
      self.model.total_charge
    end
  end

  

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

  def as_json(options)
    if(options.empty?)
      super(:methods => [:approximate_travel_distance, :image_url, :name, :services, :languages, :expected_sal_per_day, :education_details_str, :professional_qualification_str, :age, :total_charge])
    else
      super(options)
    end
  end


end
