class AddStatusToCreditNotes < ActiveRecord::Migration
  def change
    add_column :credit_notes, :status, :string
    add_column :credit_notes, :credit_note_date, :date
    add_column :credit_notes, :enrollment_id, :integer
  end
end
