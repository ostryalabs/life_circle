class CreateEnquiryCareGiverHistories < ActiveRecord::Migration
  def change
    create_table :enquiry_care_giver_histories do |t|
      t.integer :enquiry_history_id
      t.integer :care_giver_master_id
      t.integer :caregiver_charge
      t.integer :transport_charge
      t.integer :approx_travel_distance
    end
  end
end
