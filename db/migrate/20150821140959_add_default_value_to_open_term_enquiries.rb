class AddDefaultValueToOpenTermEnquiries < ActiveRecord::Migration
  def change
    change_column :enquiries, :open_term, :boolean, :default => false
  end
end
