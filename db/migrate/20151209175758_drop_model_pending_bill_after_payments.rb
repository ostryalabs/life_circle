class DropModelPendingBillAfterPayments < ActiveRecord::Migration
  def change
    drop_table :pending_bill_after_payments
  end
end
