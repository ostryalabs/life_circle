class AddStatusFieldToCareGiverMiscDetails < ActiveRecord::Migration
  def change
    add_column :care_giver_relatives, :approved, :boolean, :default => false
    add_column :care_giver_relatives, :approved_by, :integer
    add_column :care_giver_educations, :approved, :boolean, :default => false
    add_column :care_giver_educations, :approved_by, :integer
    add_column :care_giver_employers, :approved, :boolean, :default => false
    add_column :care_giver_employers, :approved_by, :integer
    add_column :care_giver_services, :approved, :boolean, :default => false
    add_column :care_giver_services, :approved_by, :integer
    add_column :care_giver_charges, :approved, :boolean, :default => false
    add_column :care_giver_charges, :approved_by, :integer
  end
end
