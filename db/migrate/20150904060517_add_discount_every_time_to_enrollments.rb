class AddDiscountEveryTimeToEnrollments < ActiveRecord::Migration
  def change
    add_column :enrollments, :discount_every_time, :boolean
  end
end
