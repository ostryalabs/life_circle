class CreateMenuBars < ActiveRecord::Migration
  def change
    create_table :menu_bars do |t|
      t.string :name
      t.integer :parent_id
      t.string :url
      t.string :mark_as_parent

      t.timestamps null: false
    end
  end
end
