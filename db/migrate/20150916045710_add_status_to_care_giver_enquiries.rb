class AddStatusToCareGiverEnquiries < ActiveRecord::Migration
  def change
    add_column :care_giver_enquiries, :status, :string
    add_column :care_giver_enquiries, :reason_for_closing, :string
  end
end
