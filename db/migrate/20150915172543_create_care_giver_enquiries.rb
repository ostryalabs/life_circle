class CreateCareGiverEnquiries < ActiveRecord::Migration
  def change
    create_table :care_giver_enquiries do |t|
      t.string :name
      t.string :location
      t.date :dob
      t.string :phone
      t.string :email
      t.date :apt_date
      t.time :apt_time

      t.timestamps null: false
    end
  end
end
