class RemoveUnwantedFieldsFromDrugMastersAndAddNewFields < ActiveRecord::Migration
  def change
    remove_column :drug_masters, :route
    remove_column :drug_masters, :dose
        
    add_column :drug_masters, :route_master_id, :integer
    add_column :drug_masters, :form_master_id, :integer
    add_column :drug_masters, :unit_master_id, :integer
  end
end
