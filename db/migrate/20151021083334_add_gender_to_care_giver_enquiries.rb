class AddGenderToCareGiverEnquiries < ActiveRecord::Migration
  def change
    add_column :care_giver_enquiries, :gender, :string
    add_column :care_giver_enquiries, :city, :string
  end
end
