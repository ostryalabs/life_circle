class AddSuperCareGiverIdToEnrollments < ActiveRecord::Migration
  def change
    add_column :enrollments, :super_care_giver_id, :integer
  end
end
