class AddEnrollmentIdToPcpServices < ActiveRecord::Migration
  def change
    add_column :pcp_services, :enrollment_id, :integer
  end
end
