class RemoveUnwantedFieldsFromPatientRoutineAndAddNewField < ActiveRecord::Migration
  def change
    remove_column :patient_routines, :wakeup_at
    remove_column :patient_routines, :breakfast
    remove_column :patient_routines, :pre_lunch_snack
    remove_column :patient_routines, :lunch
    remove_column :patient_routines, :evening_tea
    remove_column :patient_routines, :dinner
    remove_column :patient_routines, :bed_time
    add_column :patient_routines, :routine_master_id, :integer
  end
end
