class AddCloseRequestDateAndNotificationDateToEnrollments < ActiveRecord::Migration
  def change
    add_column :enrollments, :close_request_date, :date
    add_column :enrollments, :notification_given_date, :date
    add_column :enrollments, :comments_on_notification, :text
    add_column :enrollments, :closing_comments, :text
    add_column :enrollments, :reason_for_closing, :string
    add_column :enrollments, :closed_date, :date
  end
end
