class CreateCareGiverLeaves < ActiveRecord::Migration
  def change
    create_table :care_giver_leaves do |t|
      t.integer :care_giver_master_id
      t.date :leave_date
      t.date :applied_at
      t.text :description
      t.string :status
    end
  end
end
