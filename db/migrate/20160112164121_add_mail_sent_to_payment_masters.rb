class AddMailSentToPaymentMasters < ActiveRecord::Migration
  def change
    add_column :payment_masters, :mail_sent, :boolean, :default => false
  end
end
