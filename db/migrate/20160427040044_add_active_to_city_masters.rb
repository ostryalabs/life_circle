class AddActiveToCityMasters < ActiveRecord::Migration
  def change
    add_column :city_masters, :active, :boolean, default: true
  end
end
