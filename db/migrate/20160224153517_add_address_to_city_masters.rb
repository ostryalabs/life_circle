class AddAddressToCityMasters < ActiveRecord::Migration
  def change
    add_column :city_masters, :address, :text
  end
end
