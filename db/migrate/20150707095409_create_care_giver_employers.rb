class CreateCareGiverEmployers < ActiveRecord::Migration
  def change
    create_table :care_giver_employers do |t|
      t.string :employer
      t.date :period_from
      t.string :period_to
      t.string :nature_of_work
      t.string :contact_no

      t.timestamps null: false
    end
  end
end
