class CreateRoleMenuMappings < ActiveRecord::Migration
  def change
    create_table :role_menu_mappings do |t|
      t.integer :role_id
      t.integer :menu_bar_id

      t.timestamps null: false
    end
  end
end
