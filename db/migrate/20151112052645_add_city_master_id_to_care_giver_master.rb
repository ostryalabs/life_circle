class AddCityMasterIdToCareGiverMaster < ActiveRecord::Migration
  def change
    add_column :care_giver_masters, :city_master_id, :integer
  end
end
