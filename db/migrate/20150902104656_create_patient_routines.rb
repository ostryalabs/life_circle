class CreatePatientRoutines < ActiveRecord::Migration
  def change
    create_table :patient_routines do |t|
      t.string :wakeup_at
      t.string :breakfast
      t.string :pre_lunch_snack
      t.string :lunch
      t.string :evening_tea
      t.string :dinner
      t.string :bed_time
      t.integer :enrollment_id
      t.integer :created_by
      t.timestamps null: false
    end
  end
end
