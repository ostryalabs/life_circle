class CreateCareGiverDailyBudgetMasters < ActiveRecord::Migration
  def change
    create_table :care_giver_daily_budget_masters do |t|
      t.integer :upper_limit
      t.integer :lower_limit
      t.string :symbol

      t.timestamps null: false
    end
  end
end
