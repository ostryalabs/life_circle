class AddReasonForComingIdToEnrollment < ActiveRecord::Migration
  def change
    add_column :enrollments, :reason_for_coming_id, :integer
  end
end
