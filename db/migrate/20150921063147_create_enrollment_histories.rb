class CreateEnrollmentHistories < ActiveRecord::Migration
  def change
    create_table :enrollment_histories do |t|
      t.integer :enrollment_id
      t.string :note
      t.timestamps null: false
    end
  end
end
