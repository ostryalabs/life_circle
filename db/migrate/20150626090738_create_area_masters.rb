class CreateAreaMasters < ActiveRecord::Migration
  def change
    create_table :area_masters do |t|
      t.string :name
      t.float :latitude
      t.float :longitude
      t.integer :city_master_id

      t.timestamps null: false
    end
  end
end
