class AddFormFilledLocationToEnquiries < ActiveRecord::Migration
  def change
    add_column :enquiries, :form_filled_location, :string
  end
end
