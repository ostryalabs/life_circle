class CreateRoutineMasters < ActiveRecord::Migration
  def change
    create_table :routine_masters do |t|
      t.string :name
      t.integer :created_by
      t.timestamps null: false
    end
  end
end
