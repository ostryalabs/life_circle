class AddNewFieldsToCareGiverMasters < ActiveRecord::Migration
  def change
    add_column :care_giver_masters, :reason_for_status_change, :string
    add_column :care_giver_masters, :status_changed_by, :integer
    add_column :care_giver_masters, :status_changed_at, :date
  end
end
