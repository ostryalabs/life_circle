class AddCareGiverMasterIdToCaregiverRelatives < ActiveRecord::Migration
  def change
    add_column :care_giver_relatives, :care_giver_master_id, :integer
  end
end
