class AddTypeOfEnquiryToEnquiry < ActiveRecord::Migration
  def change
    add_column :enquiries, :type_of_enquiry, :string
  end
end
