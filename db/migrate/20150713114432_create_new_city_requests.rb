class CreateNewCityRequests < ActiveRecord::Migration
  def change
    create_table :new_city_requests do |t|
      t.string :city
      t.string :area
      t.string :email
      t.string :mobile_no

      t.timestamps null: false
    end
  end
end
