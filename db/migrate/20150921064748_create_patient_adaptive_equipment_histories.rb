class CreatePatientAdaptiveEquipmentHistories < ActiveRecord::Migration
  def change
    create_table :patient_adaptive_equipment_histories do |t|
      t.integer :enrollment_history_id
      t.integer :patient_adaptive_equipment_id
      t.timestamps null: false
    end
  end
end
