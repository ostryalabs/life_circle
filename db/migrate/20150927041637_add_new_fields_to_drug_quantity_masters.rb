class AddNewFieldsToDrugQuantityMasters < ActiveRecord::Migration
  def change
    add_column :drug_quantity_masters, :sos, :string
    add_column :drug_quantity_masters, :dose, :integer
    add_column :drug_quantity_masters, :review_date, :date
    add_column :drug_quantity_masters, :refill_date, :date
  end
end
