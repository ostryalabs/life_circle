class AddNameToNewCityRequests < ActiveRecord::Migration
  def change
    add_column :new_city_requests, :name, :string
  end
end
