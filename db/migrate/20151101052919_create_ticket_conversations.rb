class CreateTicketConversations < ActiveRecord::Migration
  def change
    create_table :ticket_conversations do |t|
      t.text :description
      t.integer :ticket_id
      t.integer :entered_by
      t.datetime :entered_at
      t.string :tag
      t.timestamps null: false
    end
  end
end
