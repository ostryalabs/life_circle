class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :house_no
      t.string :apartment_name
      t.string :lane_name
      t.string :plot_no
      t.string :street
      t.integer :area_master_id
      t.integer :city_master_id
      t.string :state
      t.string :country
      t.string :pin_code

      t.timestamps null: false
    end
  end
end
