class CreatePatientDisabilities < ActiveRecord::Migration
  def change
    create_table :patient_disabilities do |t|
      t.integer :patient_id
      t.integer :disability_master_id
      t.integer :enrollment_id

      t.timestamps null: false
    end
  end
end
