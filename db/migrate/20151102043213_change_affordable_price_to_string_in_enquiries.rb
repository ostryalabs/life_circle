class ChangeAffordablePriceToStringInEnquiries < ActiveRecord::Migration
  def change
    change_column :enquiries, :affordable_price, :string
  end
end
