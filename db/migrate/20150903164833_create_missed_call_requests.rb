class CreateMissedCallRequests < ActiveRecord::Migration
  def change
    create_table :missed_call_requests do |t|
      t.string :mobile_no
      t.datetime :requested_at
      t.string :status
      t.string :processed_by

      t.timestamps null: false
    end
  end
end
