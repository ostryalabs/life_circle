class CreateEnrollmentAttendances < ActiveRecord::Migration
  def change
    create_table :enrollment_attendances do |t|
      t.integer :enrollment_id
      t.datetime :login_time
      t.datetime :logout_time
      t.text :notes
      t.integer :entered_by

      t.timestamps null: false
    end
  end
end
