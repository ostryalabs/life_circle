class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :role
      t.integer :patient_id
      t.integer :caregiver_id
      t.text :description
      t.string :raised_by
      t.integer :ticket_category_master_id
      t.string :status      
      t.date :raised_date
      t.date :opened_date
      t.date :closed_date
      t.string :current_tag
      t.timestamps null: false
    end
  end
end
