class AddMobileNoAndEmailToEnquiries < ActiveRecord::Migration
  def change
    add_column :enquiries, :mobile_no, :string
    add_column :enquiries, :email, :string
  end
end
