class CreateTransportChargeMasters < ActiveRecord::Migration
  def change
    create_table :transport_charge_masters do |t|
      t.integer :from_distance
      t.integer :to_distance
      t.integer :charge
      t.timestamps null: false
    end
  end
end
