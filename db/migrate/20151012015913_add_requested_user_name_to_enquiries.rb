class AddRequestedUserNameToEnquiries < ActiveRecord::Migration
  def change
    add_column :enquiries, :requested_user_name, :string
  end
end
