class AddExpectedSalPerDayToCareGiverMasters < ActiveRecord::Migration
  def change
    add_column :care_giver_masters, :expected_sal_per_day, :integer
  end
end
