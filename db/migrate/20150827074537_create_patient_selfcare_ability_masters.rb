class CreatePatientSelfcareAbilityMasters < ActiveRecord::Migration
  def change
    create_table :patient_selfcare_ability_masters do |t|
      t.string :selfcare_ability      
      t.timestamps null: false
    end
  end
end
