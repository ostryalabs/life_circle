class CreateServiceMasters < ActiveRecord::Migration
  def change
    create_table :service_masters do |t|
      t.string :service

      t.timestamps null: false
    end
  end
end
