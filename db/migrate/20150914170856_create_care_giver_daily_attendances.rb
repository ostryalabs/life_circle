class CreateCareGiverDailyAttendances < ActiveRecord::Migration
  def change
    create_table :care_giver_daily_attendances do |t|
      t.integer :care_giver_master_id
      t.integer :patient_id
      t.date :date
      t.time :from_time
      t.time :to_time
      t.time :reached_from_time
      t.time :left_to_time
      t.string :actual_latitude
      t.string :actual_longitude
      t.string :latitude
      t.string :longitude
      t.string :status

      t.timestamps null: false
    end
  end
end
