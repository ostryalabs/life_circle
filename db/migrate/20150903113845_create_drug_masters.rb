class CreateDrugMasters < ActiveRecord::Migration
  def change
    create_table :drug_masters do |t|
      t.string :name
      t.string :route
      t.string :dose
      t.timestamps null: false
    end
  end
end
