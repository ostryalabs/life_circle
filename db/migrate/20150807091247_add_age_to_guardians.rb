class AddAgeToGuardians < ActiveRecord::Migration
  def change
    add_column :guardians, :age, :integer
    add_column :guardians, :gender, :string
  end
end
