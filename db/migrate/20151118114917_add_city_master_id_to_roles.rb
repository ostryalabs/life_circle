class AddCityMasterIdToRoles < ActiveRecord::Migration
  def change
    add_column :roles, :city_master_id, :integer
    add_column :roles, :missed_call, :boolean
  end
end
