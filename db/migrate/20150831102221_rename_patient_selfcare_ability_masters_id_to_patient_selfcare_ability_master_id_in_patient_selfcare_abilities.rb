class RenamePatientSelfcareAbilityMastersIdToPatientSelfcareAbilityMasterIdInPatientSelfcareAbilities < ActiveRecord::Migration
  def change
     rename_column :patient_selfcare_abilities, :patient_selfcare_ability_masters_id, :patient_selfcare_ability_master_id
  end
end
