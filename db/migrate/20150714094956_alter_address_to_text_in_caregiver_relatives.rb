class AlterAddressToTextInCaregiverRelatives < ActiveRecord::Migration
  def change
    change_column :care_giver_relatives, :address, :text
  end
end
