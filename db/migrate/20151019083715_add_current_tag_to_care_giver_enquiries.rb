class AddCurrentTagToCareGiverEnquiries < ActiveRecord::Migration
  def change
    add_column :care_giver_enquiries, :current_tag, :string
  end
end
