class CreateEnquiryConversations < ActiveRecord::Migration
  def change
    create_table :enquiry_conversations do |t|
      t.text :description
      t.integer :conversation_master_id
      t.integer :entered_by
      t.datetime :entered_at
      t.string :conversation_master_type
      t.timestamps null: false
    end
  end
end
