class CreateEnquiryHistories < ActiveRecord::Migration
  def change
    create_table :enquiry_histories do |t|
      t.integer :enquiry_id
      t.date :service_required_from
      t.date :service_required_to
      t.integer :no_of_care_givers
      t.string :duty_station
      t.string :caregiver_stay
      t.date :migration_date
    end
  end
end
