class RemoveLanguagesKnownFromCareGiverMasters < ActiveRecord::Migration
  def change
    remove_column :care_giver_masters, :languages_known
  end
end
