class CreateCareGiverLanguages < ActiveRecord::Migration
  def change
    create_table :care_giver_languages do |t|
      t.integer :care_giver_master_id
      t.integer :language_master_id

      t.timestamps null: false
    end
  end
end
