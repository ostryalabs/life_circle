class CreateCareGiverMasters < ActiveRecord::Migration
  def change
    create_table :care_giver_masters do |t|
      t.string :first_name
      t.string :last_name
      t.string :recruitment_mode
      t.string :gender
      t.string :father_name
      t.string :mother_name
      t.string :place_of_birth
      t.date :dob
      t.integer :address_id
      t.string :telephone_no
      t.string :maritual_status
      t.string :id_proof_no
      t.string :id_proof_doc
      t.boolean :id_proof_verified
      t.integer :id_proof_verified_by
      t.string :address_proof_id
      t.string :address_proof_doc
      t.boolean :address_proof_verified
      t.integer :address_proof_verified_by
      t.string :status
      t.string :languages_known
      t.string :job_type
      t.string :mentioned_registration_type
      t.integer :user_id 

      t.timestamps null: false
    end
  end
end
