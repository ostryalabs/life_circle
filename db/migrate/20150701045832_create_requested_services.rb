class CreateRequestedServices < ActiveRecord::Migration
  def change
    create_table :requested_services do |t|
      t.integer :patient_id
      t.integer :enquiry_id
      t.integer :service_master_id

      t.timestamps null: false
    end
  end
end
