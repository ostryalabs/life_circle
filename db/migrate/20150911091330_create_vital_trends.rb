class CreateVitalTrends < ActiveRecord::Migration
  def change
    create_table :vital_trends do |t|      
      t.integer :key_vitals_master_id
      t.string :value
      t.string :date
      t.integer :enrollment_id
      t.integer :created_by
      t.timestamps null: false
    end
  end
end
