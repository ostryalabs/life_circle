class CreateGuardians < ActiveRecord::Migration
  def change
    create_table :guardians do |t|
      t.string :first_name
      t.string :last_name
      t.string :mobile_no
      t.string :relation
      t.boolean :address_same_as_patient
      t.integer :address_id

      t.timestamps null: false
    end
  end
end
