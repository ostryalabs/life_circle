class CreateCareGiverDocuments < ActiveRecord::Migration
  def change
    create_table :care_giver_documents do |t|
      t.integer :care_giver_master_id
      t.string :document_name
      t.string :attachment
      t.date :date_of_creation
      t.boolean :approved
      t.integer :approved_by
      t.timestamps null: false
    end
  end
end
