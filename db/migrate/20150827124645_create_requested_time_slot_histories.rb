class CreateRequestedTimeSlotHistories < ActiveRecord::Migration
  def change
    create_table :requested_time_slot_histories do |t|
      t.integer :enquiry_history_id
      t.datetime :time_from
      t.datetime :time_to
      t.datetime :booked_at
      t.integer :care_giver_master_id
    end
  end
end
