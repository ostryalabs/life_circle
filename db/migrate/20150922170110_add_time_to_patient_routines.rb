class AddTimeToPatientRoutines < ActiveRecord::Migration
  def change
    add_column :patient_routines, :time, :string
  end
end
