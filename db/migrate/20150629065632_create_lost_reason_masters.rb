class CreateLostReasonMasters < ActiveRecord::Migration
  def change
    create_table :lost_reason_masters do |t|
      t.string :reason

      t.timestamps null: false
    end
  end
end
