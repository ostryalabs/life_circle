class RemovePaymentBillFieldsFromPaymentMasters < ActiveRecord::Migration
  def change
    remove_column :payment_masters, :description_1, :string
    remove_column :payment_masters, :description_2, :string
    remove_column :payment_masters, :payment_done_by, :string
    remove_column :payment_masters, :payment_mode, :string
    remove_column :payment_masters, :payment_date, :string
  end
end
