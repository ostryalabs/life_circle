class RemoveAndAddNewFieldsToCareGiverLeaves < ActiveRecord::Migration
  def change
    remove_column :care_giver_leaves, :leave_date
    
    add_column :care_giver_leaves, :from_date, :date
    add_column :care_giver_leaves, :to_date, :date
  end
end
