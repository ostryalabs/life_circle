class CreateTemplateTexts < ActiveRecord::Migration
  def change
    create_table :template_texts do |t|
      t.string :text1
      t.string :text2
      t.string :text3
      t.string :text4
      t.string :text5
      t.string :text6
      t.string :sms1
      t.string :sms2
      t.string :sms3
      t.string :sms4
      t.string :sms5
      t.string :sms6

      t.timestamps null: false
    end
  end
end
