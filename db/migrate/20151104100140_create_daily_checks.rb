class CreateDailyChecks < ActiveRecord::Migration
  def change
    create_table :daily_checks do |t|
      t.text :description
      t.integer :patient_id
      t.integer :care_giver_master_id
      t.integer :entered_by
      t.datetime :entered_at
      t.string :tag
      t.timestamps null: false
    end
  end
end
