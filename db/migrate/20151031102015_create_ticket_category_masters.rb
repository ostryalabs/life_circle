class CreateTicketCategoryMasters < ActiveRecord::Migration
  def change
    create_table :ticket_category_masters do |t|
      t.string :name
      t.string :description
      t.string :category_type
      t.timestamps null: false
    end
  end
end
