class AddTotalChargeToCareGiverCharges < ActiveRecord::Migration
  def change
    add_column :care_giver_charges, :total_charge, :integer
  end
end
