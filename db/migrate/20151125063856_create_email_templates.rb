class CreateEmailTemplates < ActiveRecord::Migration
  def change
    create_table :email_templates do |t|
      t.string :text1
      t.string :text2
      t.string :name

      t.timestamps null: false
    end
  end
end
