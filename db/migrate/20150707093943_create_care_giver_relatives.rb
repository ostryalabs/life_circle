class CreateCareGiverRelatives < ActiveRecord::Migration
  def change
    create_table :care_giver_relatives do |t|
      t.string :name
      t.integer :age
      t.string :occupation
      t.string :contact_no
      t.string :relation
      t.string :address
      t.boolean :emergency_contact
    end
  end
end
