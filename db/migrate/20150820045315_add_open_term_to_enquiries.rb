class AddOpenTermToEnquiries < ActiveRecord::Migration
  def change
    add_column :enquiries, :open_term, :boolean
  end
end
