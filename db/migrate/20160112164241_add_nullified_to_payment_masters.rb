class AddNullifiedToPaymentMasters < ActiveRecord::Migration
  def change
    add_column :payment_masters, :nullified, :boolean, :default => false
  end
end
