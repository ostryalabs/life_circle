class CreateEnquiryReferenceMasters < ActiveRecord::Migration
  def change
    create_table :enquiry_reference_masters do |t|
      t.string :reference

      t.timestamps null: false
    end
  end
end
