class CreateCareGiverChargeBuckets < ActiveRecord::Migration
  def change
    create_table :care_giver_charge_buckets do |t|
      t.integer :hrs_from
      t.integer :hrs_to
      t.float :charge_percentage

      t.timestamps null: false
    end
  end
end
