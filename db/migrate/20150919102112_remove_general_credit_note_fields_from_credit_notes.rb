class RemoveGeneralCreditNoteFieldsFromCreditNotes < ActiveRecord::Migration
  def change
    remove_column :credit_notes, :days_of_service_provided, :integer
    remove_column :credit_notes, :days_of_service_invoiced, :integer
    remove_column :credit_notes, :daily_service_rate, :integer
    remove_column :credit_notes, :value_details, :string
    remove_column :credit_notes, :no_days_service_as_per_client_register, :integer
    remove_column :credit_notes, :no_days_service_as_per_cg_register, :integer
    remove_column :credit_notes, :is_notice_period_amount_adjusted, :boolean
    remove_column :credit_notes, :is_payment_made_on_client_or_patient_name, :boolean
    remove_column :credit_notes, :authorization_token_from_client, :text
  end
end
