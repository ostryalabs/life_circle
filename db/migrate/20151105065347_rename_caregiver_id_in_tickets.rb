class RenameCaregiverIdInTickets < ActiveRecord::Migration
  def change
    rename_column :tickets, :caregiver_id, :care_giver_master_id
  end
end
