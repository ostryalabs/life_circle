class AddAdjustmentAmountToEnquiryCareGiverHistories < ActiveRecord::Migration
  def change
    add_column :enquiry_care_giver_histories, :adjustment_amount, :integer
  end
end
