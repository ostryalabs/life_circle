class CreateEnquiries < ActiveRecord::Migration
  def change
    create_table :enquiries do |t|
      t.string :registered_name
      t.string :registered_by
      t.string :mentioned_registration_type
      t.integer :user_id
      t.integer :area_master_id
      t.string :care_giver_gender
      t.date :service_required_from
      t.date :service_required_to
      t.integer :no_of_care_givers
      t.boolean :require_heavy_patient_lifting
      t.string :duty_station
      t.string :caregiver_stay
      t.string :language_profficiency

      t.timestamps null: false
    end
  end
end
