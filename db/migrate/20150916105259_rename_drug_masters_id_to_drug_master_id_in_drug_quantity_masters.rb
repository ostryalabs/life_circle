class RenameDrugMastersIdToDrugMasterIdInDrugQuantityMasters < ActiveRecord::Migration
  def change
     rename_column :drug_quantity_masters, :drug_masters_id, :drug_master_id
  end
end
