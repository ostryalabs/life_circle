class RenameUserIdToPatientIdInEnquiries < ActiveRecord::Migration
  def change
    rename_column :enquiries, :user_id, :patient_id
  end
end
