class AddClosingCommentsToEnquiries < ActiveRecord::Migration
  def change
    add_column :enquiries, :closing_comments, :text
    add_column :enquiries, :closed_at, :date
    add_column :enquiries, :closed_by, :integer
  end
end
