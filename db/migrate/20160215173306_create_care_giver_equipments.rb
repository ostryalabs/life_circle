class CreateCareGiverEquipments < ActiveRecord::Migration
  def change
    create_table :care_giver_equipments do |t|
      t.string :name
      t.string :status

      t.timestamps null: false
    end
  end
end
