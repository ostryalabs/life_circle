class AddRatePerDayToPaymentMastersAndRemoveTotalAmountFromPaymentMasters < ActiveRecord::Migration
  def change
    add_column :payment_masters, :rate_per_day, :integer
    remove_column :payment_masters, :total_amount_in_rupees, :integer
  end
end
