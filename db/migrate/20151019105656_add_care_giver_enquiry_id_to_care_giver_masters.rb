class AddCareGiverEnquiryIdToCareGiverMasters < ActiveRecord::Migration
  def change
    add_column :care_giver_masters, :care_giver_enquiry_id, :integer
  end
end
