class CreateEnquiryLanguageHistories < ActiveRecord::Migration
  def change
    create_table :enquiry_language_histories do |t|
      t.integer :enquiry_history_id
      t.integer :language_master_id
    end
  end
end
