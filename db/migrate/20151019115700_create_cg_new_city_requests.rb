class CreateCgNewCityRequests < ActiveRecord::Migration
  def change
    create_table :cg_new_city_requests do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :city
      t.string :area

      t.timestamps null: false
    end
  end
end
