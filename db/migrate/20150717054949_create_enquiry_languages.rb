class CreateEnquiryLanguages < ActiveRecord::Migration
  def change
    create_table :enquiry_languages do |t|
      t.integer :enquiry_id
      t.integer :language_master_id

      t.timestamps null: false
    end
  end
end
