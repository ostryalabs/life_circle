class CreateEnrollmentSafetyViolations < ActiveRecord::Migration
  def change
    create_table :enrollment_safety_violations do |t|
      t.integer :enrollment_id
      t.integer :enrolled_by
      t.text :comments

      t.timestamps null: false
    end
  end
end
