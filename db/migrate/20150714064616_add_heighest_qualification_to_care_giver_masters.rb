class AddHeighestQualificationToCareGiverMasters < ActiveRecord::Migration
  def change
    add_column :care_giver_masters, :heighest_qualification, :string
    add_column :care_giver_masters, :profile_pic, :string
  end
end
